<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KpiUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kpi_users')->insert([
            'user_id' => '1',
            'milestone_id' => '17',
            'kpi_id' => '7',
            'state' => 'Complete',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
