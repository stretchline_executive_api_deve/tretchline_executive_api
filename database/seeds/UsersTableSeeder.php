<?php

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // -------------------------------------------

        $row                   = [];
        $row['email']          = 'Chamath@gmail.com';
        $row['password']       = '1234';
        $row['phone']          = '0712345689';
        $row['name']           = 'Chamath';
        $row['is_admin']       = '0';
        $row['grade_id']       = '2';
        $row['epf_no']         = '127';
        $row['designation_id'] = '2';
        $row['department_id']  = '1';
        $row['profile_image']  = '';

        App\User::create($row);

    }
}
