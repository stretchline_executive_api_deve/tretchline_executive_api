<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MilestoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('milestones')->insert([
            'kpi_id' => '7',
            'name' => 'Milestone 16',
            'description' => 'Milestone description 16',
            'weight' => '16',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
