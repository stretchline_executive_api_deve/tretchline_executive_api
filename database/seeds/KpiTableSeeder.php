<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KpiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('kpis')->insert([
            'title' => 'kpi title 8',
            'description' => 'kpi description 8',
            'target_end' => '2017-12-19',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
