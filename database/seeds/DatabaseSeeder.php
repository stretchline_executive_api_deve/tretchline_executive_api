<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name'           => 'Danushka',
            'email'          => 'admin@gmail.com',
            'password'       => bcrypt('1234'),
            'phone'          => '0713739839',
            'is_admin'       => '1',
            'grade_id'       => '01',
            'epf_no'         => '29',
            'designation_id' => '1',
            'department_id'  => '2',
            'profile_image'  => ''

        ]);


    }
}
