<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserLinkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$subUserArray = array(3, 4, 5);
    	$arrayS = serialize($subUserArray);
         DB::table('user_links')->insert([
            'user_id' => '1',
            'superuser_id' => '2',
            'subuser_id' => $arrayS,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
