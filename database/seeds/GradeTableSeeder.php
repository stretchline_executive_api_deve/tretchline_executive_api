<?php

use Illuminate\Database\Seeder;

class GradeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           DB::table('grades')->insert([
            'name' => 'grade name 2',
            'description' => 'grade description 2'
        ]);
    }
}
