<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('profile_image')->default('profile_img/user_default.jpg');
            $table->enum('is_admin', ['0', '1']);
            $table->string('phone');
            $table->integer('grade_id');
            $table->integer('epf_no')->unique();
            $table->integer('designation_id');
            $table->integer('department_id');
            $table->string('email')->unique();
            $table->string('gmail')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
