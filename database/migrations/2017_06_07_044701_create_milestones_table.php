<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milestones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kpi_id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->string('weight');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });

        Schema::table('milestones', function (Blueprint $table) {
            $table->foreign('kpi_id')
                    ->references('id')->on('kpis')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milestones');
    }
}
