<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpi_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('milestone_id')->unsigned();
            $table->integer('kpi_id')->unsigned();
            $table->enum('state', ['Complete', 'Incomplete']);
            $table->timestamps();
        });

        Schema::table('kpi_users', function (Blueprint $table) {
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->foreign('milestone_id')
                    ->references('id')->on('milestones')
                    ->onDelete('cascade');
            $table->foreign('kpi_id')
                    ->references('id')->on('kpis')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpi_users');
    }
}
