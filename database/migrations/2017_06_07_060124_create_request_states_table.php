<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_states', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->unsigned();
            $table->integer('receiver_id')->unsigned();
            $table->enum('state', ['Pending', 'Confirmed', 'Rejected']);
            $table->timestamps();
        });

        Schema::table('request_states', function (Blueprint $table) {
            $table->foreign('request_id')
                    ->references('id')->on('request_meetings')
                    ->onDelete('cascade');
            $table->foreign('receiver_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_states');
    }
}
