-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 02:35 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stretchline`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `description`, `user_type`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'test announcement 1', 'Sapien voluptatibus lobortis adipiscing quisque ultrices laborum molestias, vitae optio ped', '1', '2017-09-12', '2017-09-23', '2017-09-13 03:54:24', '2017-09-13 04:40:00'),
(2, 'dgfhdgfhfg', 'hdgfhfgh', '0', '2017-09-12', '2017-09-22', '2017-09-13 06:03:14', '2017-09-13 06:03:14'),
(3, 'ttyetrte', 'rteyry', '0', '2017-09-11', '2017-09-15', '2017-09-13 06:03:30', '2017-09-13 06:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `announce_users`
--

CREATE TABLE `announce_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `announce_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `announce_users`
--

INSERT INTO `announce_users` (`id`, `announce_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-09-13 03:54:30', '2017-09-13 03:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'dip name 1', 'dip description 44', '2017-06-22 18:30:00', '2017-06-16 01:17:53'),
(2, 'dip name 2', 'dip description 2', '2017-06-21 18:30:00', '2017-06-23 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'desig name 1', 'designations description 1', '2017-06-28 18:30:00', '2017-06-09 18:30:00'),
(2, 'desig name 2', 'designations description 2', '2017-06-27 18:30:00', '2017-06-29 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `fcm_datas`
--

CREATE TABLE `fcm_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fcm_datas`
--

INSERT INTO `fcm_datas` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 2, 'sdgfdgst43t5gry6hfh56hyyhnhg656u5j', '2017-07-12 03:31:23', '2017-07-12 03:31:23'),
(2, 1, 'sdgfdgst43t5gry6hfh56hyyhnhg656u5j', '2017-09-06 01:25:16', '2017-09-13 06:04:46'),
(3, 8, 'sdgfdgst43t5gry6hfh56hyyhnhg656u5j', '2017-09-08 00:05:39', '2017-09-14 22:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'grade name 1', 'grade description 1', '2017-06-29 18:30:00', '2017-06-29 18:30:00'),
(2, 'grade 2', '', '2017-09-11 06:39:50', '2017-09-11 06:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendAnnounceEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendAnnounceEmail\",\"command\":\"O:26:\\\"App\\\\Jobs\\\\SendAnnounceEmail\\\":8:{s:5:\\\"title\\\";s:19:\\\"test announcement 1\\\";s:11:\\\"description\\\";s:91:\\\"Sapien voluptatibus lobortis adipiscing quisque ultrices laborum molestias, vitae optio ped\\\";s:7:\\\"user_id\\\";s:18:\\\"a:1:{i:0;s:1:\\\"2\\\";}\\\";s:4:\\\"type\\\";s:1:\\\"1\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:5:\\\"delay\\\";N;}\"}}', 0, NULL, 1505294670, 1505294670),
(2, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendAnnounceEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendAnnounceEmail\",\"command\":\"O:26:\\\"App\\\\Jobs\\\\SendAnnounceEmail\\\":8:{s:5:\\\"title\\\";s:10:\\\"dgfhdgfhfg\\\";s:11:\\\"description\\\";s:8:\\\"hdgfhfgh\\\";s:7:\\\"user_id\\\";s:2:\\\"N;\\\";s:4:\\\"type\\\";s:1:\\\"0\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:5:\\\"delay\\\";N;}\"}}', 0, NULL, 1505302395, 1505302395),
(3, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendAnnounceEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendAnnounceEmail\",\"command\":\"O:26:\\\"App\\\\Jobs\\\\SendAnnounceEmail\\\":8:{s:5:\\\"title\\\";s:8:\\\"ttyetrte\\\";s:11:\\\"description\\\";s:6:\\\"rteyry\\\";s:7:\\\"user_id\\\";s:2:\\\"N;\\\";s:4:\\\"type\\\";s:1:\\\"0\\\";s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:5:\\\"delay\\\";N;}\"}}', 0, NULL, 1505302411, 1505302411);

-- --------------------------------------------------------

--
-- Table structure for table `kpis`
--

CREATE TABLE `kpis` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kpis`
--

INSERT INTO `kpis` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'thjkghk', 'ryurytuy', '2017-09-12 22:46:14', '2017-09-13 05:15:59'),
(2, 'hgfjhjghfj', 'ghfjfghjghj', '2017-09-13 02:37:55', '2017-09-13 02:37:55'),
(3, 'hgfjhjghfj', 'ghfjfghjghj', '2017-09-13 02:38:30', '2017-09-13 02:38:30'),
(4, 'xczvxcv', 'vzxcvxc', '2017-09-13 02:48:48', '2017-09-13 02:48:48'),
(5, 'xcvbxcvbxc', 'cvxbxcbcv', '2017-09-13 03:04:37', '2017-09-13 03:04:37'),
(6, 'xcvbxcvbxc', 'cvxbxcbcv', '2017-09-13 03:06:30', '2017-09-13 03:06:30'),
(7, 'thjkghk', 'ryurytuy', '2017-09-13 07:12:36', '2017-09-13 07:12:36'),
(8, 'thjkghk', 'ryurytuy', '2017-09-13 07:15:48', '2017-09-13 07:15:48'),
(9, 'thjkghk1', 'ryurytuy', '2017-09-13 07:27:57', '2017-09-13 07:27:57'),
(10, 'thjkghk', 'ryurytuy', '2017-09-15 00:38:08', '2017-09-15 00:38:08'),
(11, 'thjkghk', 'ryurytuy', '2017-09-15 00:38:48', '2017-09-15 00:38:48'),
(12, 'thjkghk', 'ryurytuy', '2017-09-15 00:39:14', '2017-09-15 00:39:14'),
(13, 'thjkghk', 'ryurytuy', '2017-09-15 00:39:37', '2017-09-15 00:39:37'),
(14, 'thjkghk', 'ryurytuy', '2017-09-15 00:45:30', '2017-09-15 00:45:30'),
(15, 'thjkghk', 'ryurytuy', '2017-09-15 00:56:02', '2017-09-15 00:56:02'),
(16, 'thjkghk', 'ryurytuy', '2017-09-15 01:18:17', '2017-09-15 01:18:17'),
(17, 'xczvxcv', 'vzxcvxc', '2017-09-15 01:20:51', '2017-09-15 01:20:51'),
(18, 'xcvbxcvbxc', 'cvxbxcbcv', '2017-09-15 01:25:42', '2017-09-15 01:25:42'),
(19, 'xcvbxcvbxc', 'cvxbxcbcv', '2017-09-15 01:38:10', '2017-09-15 01:38:10'),
(20, 'xcvbxcvbxc', 'cvxbxcbcv', '2017-09-15 01:38:48', '2017-09-15 01:38:48'),
(21, 'thjkghk', 'ryurytuy', '2017-09-15 01:56:31', '2017-09-15 01:56:31'),
(22, 'thjkghk', 'ryurytuy', '2017-09-15 02:02:50', '2017-09-15 02:02:50'),
(23, 'thjkghk', 'ryurytuy', '2017-09-15 02:08:57', '2017-09-15 02:08:57'),
(24, 'thjkghk', 'ryurytuy', '2017-09-15 02:53:48', '2017-09-15 02:53:48'),
(25, 'thjkghk', 'ryurytuy', '2017-09-15 02:54:17', '2017-09-15 02:54:17'),
(26, 'thjkghk', 'ryurytuy', '2017-09-15 02:56:21', '2017-09-15 02:56:21'),
(27, 'thjkghk', 'ryurytuy', '2017-09-15 02:59:32', '2017-09-15 02:59:32'),
(28, 'thjkghk', 'ryurytuy', '2017-09-15 03:00:08', '2017-09-15 03:00:08'),
(29, 'thjkghk', 'ryurytuy', '2017-09-15 03:00:48', '2017-09-15 03:00:48'),
(30, 'thjkghk', 'ryurytuy', '2017-09-15 03:01:10', '2017-09-15 03:01:10'),
(31, 'thjkghk', 'ryurytuy', '2017-09-15 03:01:52', '2017-09-15 03:01:52'),
(32, 'thjkghk', 'ryurytuy', '2017-09-15 03:03:14', '2017-09-15 03:03:14'),
(33, 'thjkghk', 'ryurytuy', '2017-09-15 03:29:54', '2017-09-15 03:29:54'),
(34, 'thjkghk', 'ryurytuy', '2017-09-15 03:32:20', '2017-09-15 03:32:20'),
(35, 'thjkghk', 'ryurytuy', '2017-09-15 03:36:11', '2017-09-15 03:36:11'),
(36, 'thjkghk', 'ryurytuy', '2017-09-15 03:36:47', '2017-09-15 03:36:47'),
(37, 'thjkghk', 'ryurytuy', '2017-09-15 03:37:00', '2017-09-15 03:37:00'),
(38, 'thjkghk', 'ryurytuy', '2017-09-15 03:37:21', '2017-09-15 03:37:21'),
(39, 'thjkghk', 'ryurytuy', '2017-09-15 03:37:34', '2017-09-15 03:37:34'),
(40, 'thjkghk', 'ryurytuy', '2017-09-15 03:43:11', '2017-09-15 03:43:11'),
(41, 'thjkghk', 'ryurytuy', '2017-09-15 03:43:44', '2017-09-15 03:43:44');

-- --------------------------------------------------------

--
-- Table structure for table `kpi_users`
--

CREATE TABLE `kpi_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `milestone_id` int(10) UNSIGNED DEFAULT NULL,
  `kpi_id` int(10) UNSIGNED NOT NULL,
  `state` enum('Complete','Submitted','Incomplete') COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kpi_users`
--

INSERT INTO `kpi_users` (`id`, `user_id`, `milestone_id`, `kpi_id`, `state`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 1, 'Incomplete', '2017-09-13 02:01:47', '2017-09-13 05:15:59'),
(3, 8, NULL, 2, NULL, '2017-09-13 02:37:55', '2017-09-13 02:37:55'),
(4, 8, NULL, 3, NULL, '2017-09-13 02:38:30', '2017-09-13 02:38:30'),
(5, 8, NULL, 4, NULL, '2017-09-13 02:48:48', '2017-09-13 02:48:48'),
(6, 9, NULL, 5, NULL, '2017-09-13 03:04:38', '2017-09-13 03:04:38'),
(7, 9, NULL, 6, NULL, '2017-09-13 03:06:31', '2017-09-13 03:06:31'),
(9, 2, NULL, 8, NULL, '2017-09-13 07:15:48', '2017-09-13 07:15:48'),
(10, 2, NULL, 9, NULL, '2017-09-13 07:27:57', '2017-09-13 07:27:57'),
(11, 2, 2, 7, 'Incomplete', '2017-09-15 00:16:35', '2017-09-15 00:16:35'),
(12, 2, 3, 7, 'Incomplete', '2017-09-15 00:16:35', '2017-09-15 00:16:35'),
(13, 8, NULL, 10, NULL, '2017-09-15 00:38:08', '2017-09-15 00:38:08'),
(14, 8, NULL, 11, NULL, '2017-09-15 00:38:48', '2017-09-15 00:38:48'),
(15, 8, NULL, 12, NULL, '2017-09-15 00:39:14', '2017-09-15 00:39:14'),
(16, 8, NULL, 13, NULL, '2017-09-15 00:39:37', '2017-09-15 00:39:37'),
(17, 8, NULL, 14, NULL, '2017-09-15 00:45:30', '2017-09-15 00:45:30'),
(18, 8, NULL, 15, NULL, '2017-09-15 00:56:02', '2017-09-15 00:56:02'),
(19, 8, NULL, 16, NULL, '2017-09-15 01:18:18', '2017-09-15 01:18:18'),
(20, 9, NULL, 17, NULL, '2017-09-15 01:20:52', '2017-09-15 01:20:52'),
(21, 2, NULL, 18, NULL, '2017-09-15 01:25:42', '2017-09-15 01:25:42'),
(22, 9, NULL, 19, NULL, '2017-09-15 01:38:11', '2017-09-15 01:38:11'),
(23, 9, NULL, 20, NULL, '2017-09-15 01:38:48', '2017-09-15 01:38:48'),
(24, 8, NULL, 21, NULL, '2017-09-15 01:56:31', '2017-09-15 01:56:31'),
(25, 9, NULL, 22, NULL, '2017-09-15 02:02:50', '2017-09-15 02:02:50'),
(26, 2, NULL, 23, NULL, '2017-09-15 02:08:57', '2017-09-15 02:08:57'),
(27, 2, NULL, 24, NULL, '2017-09-15 02:53:48', '2017-09-15 02:53:48'),
(28, 2, NULL, 25, NULL, '2017-09-15 02:54:17', '2017-09-15 02:54:17'),
(29, 2, NULL, 26, NULL, '2017-09-15 02:56:21', '2017-09-15 02:56:21'),
(30, 2, NULL, 27, NULL, '2017-09-15 02:59:32', '2017-09-15 02:59:32'),
(31, 2, NULL, 28, NULL, '2017-09-15 03:00:08', '2017-09-15 03:00:08'),
(32, 2, NULL, 29, NULL, '2017-09-15 03:00:48', '2017-09-15 03:00:48'),
(33, 2, NULL, 30, NULL, '2017-09-15 03:01:10', '2017-09-15 03:01:10'),
(34, 2, NULL, 31, NULL, '2017-09-15 03:01:52', '2017-09-15 03:01:52'),
(35, 2, NULL, 32, NULL, '2017-09-15 03:03:14', '2017-09-15 03:03:14'),
(36, 8, NULL, 33, NULL, '2017-09-15 03:29:54', '2017-09-15 03:29:54'),
(37, 8, NULL, 34, NULL, '2017-09-15 03:32:20', '2017-09-15 03:32:20'),
(38, 8, NULL, 35, NULL, '2017-09-15 03:36:11', '2017-09-15 03:36:11'),
(39, 8, NULL, 36, NULL, '2017-09-15 03:36:47', '2017-09-15 03:36:47'),
(40, 8, NULL, 37, NULL, '2017-09-15 03:37:00', '2017-09-15 03:37:00'),
(41, 1, NULL, 38, NULL, '2017-09-15 03:37:21', '2017-09-15 03:37:21'),
(42, 1, NULL, 39, NULL, '2017-09-15 03:37:34', '2017-09-15 03:37:34'),
(44, 1, 5, 40, 'Incomplete', '2017-09-15 03:43:12', '2017-09-15 03:43:12'),
(46, 8, 6, 41, 'Incomplete', '2017-09-15 03:43:45', '2017-09-15 03:43:45'),
(47, 8, 7, 41, 'Incomplete', '2017-09-15 03:43:45', '2017-09-15 03:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_06_07_042238_create_departments_table', 1),
(4, '2017_06_07_042938_create_grades_table', 1),
(5, '2017_06_07_043433_create_designations_table', 1),
(6, '2017_06_07_043800_create_kpis_table', 1),
(7, '2017_06_07_044701_create_milestones_table', 1),
(8, '2017_06_07_045419_create_notes_table', 1),
(9, '2017_06_07_053328_create_request_meetings_table', 1),
(10, '2017_06_07_060124_create_request_states_table', 1),
(11, '2017_06_07_062209_create_user_links_table', 1),
(12, '2017_06_07_062739_create_kpi_users_table', 1),
(13, '2017_06_07_063921_create_announcements_table', 1),
(14, '2017_06_07_064846_create_announce_users_table', 1),
(15, '2017_06_12_114141_create_personal_notes_table', 2),
(17, '2017_07_11_065733_create_fcm_datas_table', 3),
(18, '2017_07_20_094243_create_jobs_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `milestones`
--

CREATE TABLE `milestones` (
  `id` int(10) UNSIGNED NOT NULL,
  `kpi_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `weight` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `milestones`
--

INSERT INTO `milestones` (`id`, `kpi_id`, `name`, `description`, `weight`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 'dgsfg', 'dfsgdsfg', '43529', '2017-09-18', '2017-09-22', '2017-09-13 02:01:45', '2017-09-13 02:01:45'),
(2, 7, 'ghjghjg', 'jjgjghj', '54', '2017-09-16', '2017-09-29', '2017-09-15 00:16:31', '2017-09-15 00:16:31'),
(3, 7, 'dddddghdh', 'dgfhfghfgh', '54', '2017-09-14', '2017-09-21', '2017-09-15 00:16:35', '2017-09-15 00:16:35'),
(4, 39, 'dgsfg', 'dfsgdsfg', '43529', '2017-09-18', '2017-09-22', '2017-09-15 03:38:50', '2017-09-15 03:38:50'),
(5, 40, 'dgsfg', 'dfsgdsfg', '43529', '2017-09-18', '2017-09-22', '2017-09-15 03:43:11', '2017-09-15 03:43:11'),
(6, 41, 'ghjghjg', 'jjgjghj', '54', '2017-09-16', '2017-09-29', '2017-09-15 03:43:44', '2017-09-15 03:43:44'),
(7, 41, 'dddddghdh', 'dgfhfghfgh', '54', '2017-09-14', '2017-09-21', '2017-09-15 03:43:45', '2017-09-15 03:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `kpi_id` int(10) UNSIGNED NOT NULL,
  `milestone_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `next_review` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_notes`
--

CREATE TABLE `personal_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_meetings`
--

CREATE TABLE `request_meetings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `milestone_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `meeting_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_states`
--

CREATE TABLE `request_states` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `state` enum('Pending','Confirmed','Rejected') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'profile_img/user_default.jpg',
  `is_admin` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grade_id` int(11) NOT NULL,
  `epf_no` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `profile_image`, `is_admin`, `phone`, `grade_id`, `epf_no`, `designation_id`, `department_id`, `email`, `gmail`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Danushka', 'profile_img/97941.jpg', '1', '0713739839', 1, 29, 1, 2, 'admin@gmail.com', '', '$2y$10$LKw3jro0.YLH9ASMhF.hOuYxBg80OXkg3U2Pn3knDlYSRLnP0YT1a', 'y3hsVKgtX3S4PBn2dJn9AJ3HXBTk3jDMi39mcazph8RQpiQXYLJLsuGy8lcR', '2017-06-06 21:30:00', '2017-06-19 11:06:34'),
(2, 'Sandharuwan', 'profile_img/67602.png', '0', '0719124141', 2, 124, 1, 2, 'sandharu1@gmail.com', '', '$2y$10$/CKYuoUx0.1m8vlO24M/ZOT.oWhR8B0HXYwLy.S2SHZqlEcJgzIP.', 'TjbiiPM9KKAzucEY7a0rOlWN44DVekDX4923H8if6GglIXMUYBzxBCCQqWD8', '2017-06-13 23:19:12', '2017-06-13 23:19:12'),
(8, 'demo-stretchline', 'profile_img/32129.jpg', '0', '+94719124141', 1, 123123, 1, 2, 'wad.mobile.app@gmail.com', 'wad.mobile.app@gmail.com', '$2y$10$dvaSY1QlG5l9QM3hmWDQa.UiquFTXl4JRe/kuvuVocM3mKSpUmTKG', NULL, '2017-09-06 05:16:47', '2017-09-06 05:16:47'),
(9, 'Stretchline-A', 'profile_img/29709.jpg', '1', '+94712721566', 1, 1231234, 1, 1, 'wadinternationalmobile@gmail.com', 'wadinternationalmobile@gmail.com', '$2y$10$LKw3jro0.YLH9ASMhF.hOuYxBg80OXkg3U2Pn3knDlYSRLnP0YT1a', '0T7KYVvWD4GP6gosqO9zfrE4iQQHMzF4B8lIJH7PvfhsDa1gG4YtkqbBZ65r', '2017-09-11 01:45:49', '2017-09-11 01:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_links`
--

CREATE TABLE `user_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `superuser_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subuser_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_links`
--

INSERT INTO `user_links` (`id`, `user_id`, `superuser_id`, `subuser_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"9\";}', '2017-06-14 00:20:25', '2017-09-15 04:37:32'),
(3, 2, NULL, 'a:1:{i:0;s:1:\"9\";}', '2017-06-20 09:40:53', '2017-09-14 05:01:10'),
(9, 8, '1', NULL, '2017-09-06 05:16:47', '2017-09-15 04:37:32'),
(10, 9, '1', NULL, '2017-09-11 01:45:49', '2017-09-15 04:37:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announce_users`
--
ALTER TABLE `announce_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announce_users_user_id_foreign` (`user_id`),
  ADD KEY `announce_users_announce_id_foreign` (`announce_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fcm_datas`
--
ALTER TABLE `fcm_datas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fcm_datas_user_id_foreign` (`user_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `kpis`
--
ALTER TABLE `kpis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kpi_users`
--
ALTER TABLE `kpi_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kpi_users_user_id_foreign` (`user_id`),
  ADD KEY `kpi_users_milestone_id_foreign` (`milestone_id`),
  ADD KEY `kpi_users_kpi_id_foreign` (`kpi_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `milestones`
--
ALTER TABLE `milestones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `milestones_kpi_id_foreign` (`kpi_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notes_user_id_foreign` (`user_id`),
  ADD KEY `notes_kpi_id_foreign` (`kpi_id`),
  ADD KEY `notes_milestone_id_foreign` (`milestone_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `personal_notes`
--
ALTER TABLE `personal_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personal_notes_user_id_foreign` (`user_id`);

--
-- Indexes for table `request_meetings`
--
ALTER TABLE `request_meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_meetings_user_id_foreign` (`user_id`),
  ADD KEY `request_meetings_milestone_id_foreign` (`milestone_id`);

--
-- Indexes for table `request_states`
--
ALTER TABLE `request_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_states_request_id_foreign` (`request_id`),
  ADD KEY `request_states_receiver_id_foreign` (`receiver_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_epf_no_unique` (`epf_no`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_links`
--
ALTER TABLE `user_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_links_user_id_unique` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `announce_users`
--
ALTER TABLE `announce_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fcm_datas`
--
ALTER TABLE `fcm_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kpis`
--
ALTER TABLE `kpis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `kpi_users`
--
ALTER TABLE `kpi_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `milestones`
--
ALTER TABLE `milestones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personal_notes`
--
ALTER TABLE `personal_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_meetings`
--
ALTER TABLE `request_meetings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_states`
--
ALTER TABLE `request_states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_links`
--
ALTER TABLE `user_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `announce_users`
--
ALTER TABLE `announce_users`
  ADD CONSTRAINT `announce_users_announce_id_foreign` FOREIGN KEY (`announce_id`) REFERENCES `announcements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `announce_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fcm_datas`
--
ALTER TABLE `fcm_datas`
  ADD CONSTRAINT `fcm_datas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kpi_users`
--
ALTER TABLE `kpi_users`
  ADD CONSTRAINT `kpi_users_kpi_id_foreign` FOREIGN KEY (`kpi_id`) REFERENCES `kpis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kpi_users_milestone_id_foreign` FOREIGN KEY (`milestone_id`) REFERENCES `milestones` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kpi_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `milestones`
--
ALTER TABLE `milestones`
  ADD CONSTRAINT `milestones_kpi_id_foreign` FOREIGN KEY (`kpi_id`) REFERENCES `kpis` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_kpi_id_foreign` FOREIGN KEY (`kpi_id`) REFERENCES `kpis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notes_milestone_id_foreign` FOREIGN KEY (`milestone_id`) REFERENCES `milestones` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `personal_notes`
--
ALTER TABLE `personal_notes`
  ADD CONSTRAINT `personal_notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `request_meetings`
--
ALTER TABLE `request_meetings`
  ADD CONSTRAINT `request_meetings_milestone_id_foreign` FOREIGN KEY (`milestone_id`) REFERENCES `milestones` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_meetings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `request_states`
--
ALTER TABLE `request_states`
  ADD CONSTRAINT `request_states_receiver_id_foreign` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_states_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `request_meetings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_links`
--
ALTER TABLE `user_links`
  ADD CONSTRAINT `user_links_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
