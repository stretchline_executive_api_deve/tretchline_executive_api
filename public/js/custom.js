// Data tables
jQuery(document).ready(function() {
    jQuery('#departmentTable').DataTable( {
    responsive: true,
    dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Departments Data - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Departments Data - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Departments Data - ©Stretchline'
            },
            'colvis'
        ]
});
    jQuery('#gradeTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Grades Data - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Grades Data - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Grades Data - ©Stretchline'
            },
            'colvis'
        ]
});
    jQuery('#designationTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Designations Data - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Designations Data - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'Designations Data - ©Stretchline'
            },
            'colvis'
        ]
});
    // =========Cascade down kpi table==========
/* Formatting function for row details - modify as you need */
// function format ( value ) {
//   var arr = value.split('|');
//   var edit = "{{ route('kpi.edit','++') }}";
//   alert(arr[0]);
//     // `d` is the original data object for the row
//     return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
//         '<tr>'+
//             '<td>Description:</td>'+
//             '<td>'+arr[0]+'</td>'+
//         '</tr>'+
//         '<tr>'+
//             '<td>Extension number:</td>'+
//             '<td>'+arr[1]+'</td>'+
//         '</tr>'+
//         '<tr>'+
//             '<td>Extra info:</td>'+
//             '<td><div class="row">'+
//           '<div class="col-md-4">'+
//             '<a class="btn btn-warning btn-xs" href="{{ route("kpi.edit",'+arr[1]+') }}" style="width: 50px;">EDIT</a>'+
//           '</div>'+
//           '<div class="col-md-4">'+
//             '<a class="btn btn-primary btn-xs" href="{{ route("kpi.edit",'+arr[1]+') }}?copy" style="width: 50px;">COPY</a>'+
//           '</div>'+
//           '<div class="col-md-4">'+
//             '<form method="POST" action="{{ route("kpi.destroy", '+arr[1]+') }}" accept-charset="UTF-8">'+
//               '{{ csrf_field() }}'+
//               '{{ method_field("DELETE") }}'+
//               '<button type="submit" class="btn btn-danger btn-xs" data-toggle="confirmation" data-placement="left" data-singleton="true" style="width: 50px;">DELETE</button>'+
//             '</form>'+
//           '</div>'+
    
//           '</div></td>'+
//         '</tr>'+
//     '</table>'+;
// }

//     var table = jQuery('#kpiTable').DataTable( {
//     responsive: true,
//      dom: 'Bfrtip',
//         buttons: [
//         {
//                extend: 'excelHtml5',
//                 exportOptions: {
//                     columns: [ 0, 1, 2, 3, 4, 5]
//                 },
//                 title:'KPI Data - ©Stretchline'
//             },
//             {
//                 extend: 'csvHtml5',
//                 exportOptions: {
//                     columns: [ 0, 1, 2, 3, 4, 5]
//                 },
//                 title:'KPI Data - ©Stretchline'
//             },
//             {
//                 extend: 'pdfHtml5',
//                 exportOptions: {
//                     columns: [ 0, 1, 2, 3, 4, 5]
//                 },
//                 title:'KPI Data - ©Stretchline'
//             },
//             'colvis'
//         ]
// }); 

    $(document).ready(function() {
    $('#kpiTable').DataTable( {
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   [0]
        },
        { responsivePriority: 1, targets: 4 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 1, targets: 2 }

         ],
          dom: 'Bfrtip',
        buttons: [
        {
               extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
                title:'KPI Data - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
                title:'KPI Data - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
                title:'KPI Data - ©Stretchline'
            },
            'colvis'
        ]

    } );
} );

// Add event listener for opening and closing details
    // $('#kpiTable tbody').on('click', 'td.details-control', function () {
       
    //       var tr = $(this).closest('tr');
    //       var row = table.row(tr);

    //       if (row.child.isShown()) {
    //           // This row is already open - close it
    //           row.child.hide();
    //           tr.removeClass('shown');
    //       } else {
    //           // Open this row
    //           row.child(format(tr.data('child-value'))).show();
    //           tr.addClass('shown');
    //       }
    // } );
    // ============ end cascade down Kpi================
    jQuery('#kpiUserTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
        {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
                title:'Milestone by user - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
                title:'Milestone by user - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
                title:'Milestone by user - ©Stretchline'
            },
            'colvis'
        ]
}); 
    jQuery('#announceTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
        {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Announcements - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Announcements - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Announcements - ©Stretchline'
            },
            'colvis'
        ]
});
    jQuery('#usersTable').DataTable(
        {
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: -1 }
    ],
     dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6, 7, 8]
                },
                title:'Users - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6, 7, 8]
                },
                title:'Users - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6, 7, 8]
                },
                title:'Users - ©Stretchline'
            },
            'colvis'
        ]
});
    jQuery('#requestMeetingTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
             {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6]
                },
                title:'Meeting Requests - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6]
                },
                title:'Meeting Requests - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6]
                },
                title:'Meeting Requests - ©Stretchline'
            },
            'colvis'
        ]
});
    jQuery('#noteTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
             {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6]
                },
                title:'Notes - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6]
                },
                title:'Notes - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 3, 4, 5, 6]
                },
                title:'Notes - ©Stretchline'
            },
            'colvis'
        ]
});
    jQuery('#pvtannounceTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ]
});
    jQuery('#pubannounceTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'colvis'
        ]
});

        jQuery('#userMeetingRequestsTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
        {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Meeting Requests - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Meeting Requests - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Meeting Requests - ©Stretchline'
            },
            'colvis'
        ]
});

            jQuery('#myKpiListTable').DataTable( {
    responsive: true,
     dom: 'Bfrtip',
        buttons: [
        {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'My KPI List - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'My KPI List - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                },
                title:'My KPI List - ©Stretchline'
            },
            'colvis'
        ]
});

      jQuery('#subuserKpiMilesTable').DataTable( {
       responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   [0]
        },
        { responsivePriority: 1, targets: 5 },
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 1, targets: 3 },
        { responsivePriority: 1, targets: 6 }
         ],
     dom: 'Bfrtip',
        buttons: [
        {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Subuser KPI/Milestone - ©Stretchline'
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Subuser KPI/Milestone - ©Stretchline'
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6]
                },
                title:'Subuser KPI/Milestone - ©Stretchline'
            },
            'colvis'
        ]
});


} );

$('[data-toggle=confirmation]').confirmation({
  rootSelector: '[data-toggle=confirmation]',
  // other options
});



// ---------milestone add--------------
//define template
var template = $('#sections .section:first').clone();

//define counter
var sectionsCount = 1;

//add new section
$('body').on('click', '.addsection', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

    }).end()

    //inject new section
    .appendTo('#sections');
    $(".remove:last").show();
    return false;
});

//remove section
$('#sections').on('click', '.remove', function() {
    //fade out section
    $(this).parent().fadeOut(300, function(){
        //remove parent element (main section)
        $(this).parent().empty();
        return false;
    });
    return false;
});

//date pickes
  // $( function() {
  //   $( "#date-basic-addon13" ).datepicker(
  //           {
  //           dateFormat: "yy-mm-dd",
  //           numberOfMonths: 2,
  //           onSelect: function (selected) {
  //           var dt = new Date(selected);
  //           dt.setDate(dt.getDate() + 1);
  //           $("#date-basic-addon14").datepicker("option", "minDate", dt);
  //           }
  //       });
  //   $( "#date-basic-addon14" ).datepicker(
  //           {
  //           dateFormat: "yy-mm-dd",
  //           numberOfMonths: 2,
  //           onSelect: function (selected) {
  //           var dt = new Date(selected);
  //           dt.setDate(dt.getDate() - 1);
  //           $("#date-basic-addon13").datepicker("option", "maxDate", dt);
  //           }
  //       });
  // } );
  //   $( function() {
  //   $( "#date-basic-addon15" ).datepicker(
  //           {
  //           dateFormat: "yy-mm-dd",
  //           numberOfMonths: 2,
  //           onSelect: function (selected) {
  //           var dt = new Date(selected);
  //           dt.setDate(dt.getDate() + 1);
  //           $("#date-basic-addon16").datepicker("option", "minDate", dt);
  //           }
  //       });
  //   $( "#date-basic-addon16" ).datepicker(
  //           {
  //           dateFormat: "yy-mm-dd",
  //           numberOfMonths: 2,
  //           onSelect: function (selected) {
  //           var dt = new Date(selected);
  //           dt.setDate(dt.getDate() - 1);
  //           $("#date-basic-addon15").datepicker("option", "maxDate", dt);
  //           }

  //       });
  // } );


$('body').on('focus',".date-basic-addon13", ".date-basic-addon14", function(){
    $(".date-basic-addon13").datepicker(
 {
            dateFormat: "yy-mm-dd",
            numberOfMonths: 2,
            onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".date-basic-addon14").datepicker("option", "minDate", dt);
            }
        }
        );
    $(".date-basic-addon14").datepicker(
 {
            dateFormat: "yy-mm-dd",
            numberOfMonths: 2,
            onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $(".date-basic-addon13").datepicker("option", "maxDate", dt);
            }
        }
        );
});

$('body').on('focus',".date-basic-addon15", ".date-basic-addon16", function(){
    $(".date-basic-addon15").datepicker(
 {
            dateFormat: "yy-mm-dd",
            numberOfMonths: 2,
            onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".date-basic-addon16").datepicker("option", "minDate", dt);
            }
        }
        );
    $(".date-basic-addon16").datepicker(
 {
            dateFormat: "yy-mm-dd",
            numberOfMonths: 2,
            onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $(".date-basic-addon15").datepicker("option", "maxDate", dt);
            }
        }
        );
});

// ====================sub user kpi -ajax call ==========
    $(document).ready(function() {
        $('select[name="subuser_id"]').on('change', function() {
            var subuserID = $(this).val();
            var mainurl = location.protocol + "//" + document.domain + "/" + location.pathname.split('/')[1] + "/";
            if(subuserID) {
                $.ajax({
                    url: mainurl+'public/user/milestone/kpibyuser/'+subuserID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="kpi_id"]').empty();
                        $.each(data, function(key, value) {
                          $.each(value, function(key, value) {
                          // alert("key:"+key+"  |  value: "+value);
                            $('select[name="kpi_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                          });
                        });
                        $('.chosen-select2').trigger('chosen:updated');
                        $('.chosen-select2').chosen();
                        $('.chosen-select-deselect2').chosen({ allow_single_deselect: true });

                    }
                });
            }else{
                $('select[name="kpi_id"]').empty();
            }
        });
    });
// ====================user kpi -ajax call ==========
     $(document).ready(function() {
        $('select[name="user_id"]').on('change', function() {
            
            var subuserID = $(this).val();
            // alert(mainurl);
            var mainurl = location.protocol + "//" + document.domain + "/" + location.pathname.split('/')[1] + "/";
            // alert(mainurl);
            if(subuserID) {
                $.ajax({
                    url: mainurl+'public/admin/milestone/kpibyuser/'+subuserID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="kpi_id"]').empty();
                        $.each(data, function(key, value) {
                          $.each(value, function(key, value) {
                          // alert("key:"+key+"  |  value: "+value);
                            $('select[name="kpi_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                          });
                        });
                        $('.chosen-select4').trigger('chosen:updated');
                        $('.chosen-select4').chosen();
                        $('.chosen-select-deselect4').chosen({ allow_single_deselect: true });

                    }
                });
            }else{
                $('select[name="kpi_id"]').empty();
            }
        });
    });

    //user chosen
$(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
$(function() {
        $('.chosen-select2').chosen();
        $('.chosen-select-deselect2').chosen({ allow_single_deselect: true });
      });
$(function() {
        $('.chosen-select3').chosen();
        $('.chosen-select-deselect3').chosen({ allow_single_deselect: true });
      });
$(function() {
        $('.chosen-select4').chosen();
        $('.chosen-select-deselect4').chosen({ allow_single_deselect: true });
      });
//overall dashboard filter choosen
$(function() {
        $('.chosen-select5').chosen();
        $('.chosen-select-deselect5').chosen({ allow_single_deselect: true });
      });
// =======Dashboad new ui info box click========
jQuery(function(){
         jQuery('#showalll').click(function(){
               jQuery('.targetDiv').show(500);
        });
        jQuery('.showSingle').click(function(){
              jQuery('.targetDiv').hide(500);
              jQuery('#div'+$(this).attr('target')).show(500);
        });
});


// ===========milestone div stick==========
function sticky_relocate1() {
        var window_top = $(window).scrollTop();
        
        var div_top = $('#sticky-anchor1').offset().top;
        if (window_top > div_top) {
            $('#sticky1').addClass('stick');
            $('#sticky-anchor1').height($('#sticky1').outerHeight());
        } else {
            $('#sticky1').removeClass('stick');
            $('#sticky-anchor1').height(0);
        }
     }

$(function() {
    $(window).scroll(sticky_relocate1);
    // alert(window_top);
    sticky_relocate1();
  });
// -------
function sticky_relocate2() {
        var window_top = $(window).scrollTop();
        
        var div_top = $('#sticky-anchor2').offset().top;
        if (window_top > div_top) {
            $('#sticky2').addClass('stick');
            $('#sticky-anchor2').height($('#sticky2').outerHeight());
        } else {
            $('#sticky2').removeClass('stick');
            $('#sticky-anchor2').height(0);
        }
     }

$(function() {
    $(window).scroll(sticky_relocate2);
    // alert(window_top);
    sticky_relocate2();
  });

// -------
function sticky_relocate3() {
        var window_top = $(window).scrollTop();
        
        var div_top = $('#sticky-anchor3').offset().top;
        if (window_top > div_top) {
            $('#sticky3').addClass('stick');
            $('#sticky-anchor3').height($('#sticky3').outerHeight());
        } else {
            $('#sticky3').removeClass('stick');
            $('#sticky-anchor3').height(0);
        }
     }

$(function() {
    $(window).scroll(sticky_relocate1);
    // alert(window_top);
    sticky_relocate1();
  });

///numeric validation - prevent negetive
// Select your input element.
var number = document.getElementById('number');

// Listen for input event on numInput.
number.onkeydown = function(e) {
    if(!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58) 
      || e.keyCode == 8)) {
        return false;
    }
}