(function(){
	"use strict";

	var app = angular.module('app',
		[
			'app.controllers',
			'app.filters',
			'app.services',
			'app.directives',
			'app.routes',
			'app.config'
		]);

	angular.module('app.routes', ['ui.router']);
	angular.module('app.controllers', ['ngMaterial', 'ui.router', 'restangular']);
	angular.module('app.filters', []);
	angular.module('app.services', []);
	angular.module('app.directives', []);
	angular.module('app.config', ['ngMaterial']);

})();
(function(){
	"use strict";

	angular.module('app.routes').config( ["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider ) {

		var getView = function( viewName ){
			return '/views/app/' + viewName + '/' + viewName + '.html';
		};

		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('landing', {
				url: '/',
				views: {
					main: {
						templateUrl: getView('landing')
					}
				}
			}).state('login', {
			url: '/login',
			views: {
				main: {
					templateUrl: getView('login')
				},
				footer: {
					templateUrl: getView('footer')
				}
			}
		});

	}] );
})();
(function(){
	"use strict";

	angular.module('app.config').config( ["$mdThemingProvider", function($mdThemingProvider) {
		/* For more info, visit https://material.angularjs.org/#/Theming/01_introduction */
		$mdThemingProvider.theme('default')
			.primaryPalette('teal')
			.accentPalette('cyan')
			.warnPalette('red');
	}]);

})();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9tYWluLmpzIiwiYXBwL3JvdXRlcy5qcyIsImNvbmZpZy90aGVtZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxDQUFBLFVBQUE7Q0FDQTs7Q0FFQSxJQUFBLE1BQUEsUUFBQSxPQUFBO0VBQ0E7R0FDQTtHQUNBO0dBQ0E7R0FDQTtHQUNBO0dBQ0E7OztDQUdBLFFBQUEsT0FBQSxjQUFBLENBQUE7Q0FDQSxRQUFBLE9BQUEsbUJBQUEsQ0FBQSxjQUFBLGFBQUE7Q0FDQSxRQUFBLE9BQUEsZUFBQTtDQUNBLFFBQUEsT0FBQSxnQkFBQTtDQUNBLFFBQUEsT0FBQSxrQkFBQTtDQUNBLFFBQUEsT0FBQSxjQUFBLENBQUE7OztBQ2xCQSxDQUFBLFVBQUE7Q0FDQTs7Q0FFQSxRQUFBLE9BQUEsY0FBQSxpREFBQSxTQUFBLGdCQUFBLHFCQUFBOztFQUVBLElBQUEsVUFBQSxVQUFBLFVBQUE7R0FDQSxPQUFBLGdCQUFBLFdBQUEsTUFBQSxXQUFBOzs7RUFHQSxtQkFBQSxVQUFBOztFQUVBO0lBQ0EsTUFBQSxXQUFBO0lBQ0EsS0FBQTtJQUNBLE9BQUE7S0FDQSxNQUFBO01BQ0EsYUFBQSxRQUFBOzs7TUFHQSxNQUFBLFNBQUE7R0FDQSxLQUFBO0dBQ0EsT0FBQTtJQUNBLE1BQUE7S0FDQSxhQUFBLFFBQUE7O0lBRUEsUUFBQTtLQUNBLGFBQUEsUUFBQTs7Ozs7OztBQzFCQSxDQUFBLFVBQUE7Q0FDQTs7Q0FFQSxRQUFBLE9BQUEsY0FBQSwrQkFBQSxTQUFBLG9CQUFBOztFQUVBLG1CQUFBLE1BQUE7SUFDQSxlQUFBO0lBQ0EsY0FBQTtJQUNBLFlBQUE7OztLQUdBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe1xuXHRcInVzZSBzdHJpY3RcIjtcblxuXHR2YXIgYXBwID0gYW5ndWxhci5tb2R1bGUoJ2FwcCcsXG5cdFx0W1xuXHRcdFx0J2FwcC5jb250cm9sbGVycycsXG5cdFx0XHQnYXBwLmZpbHRlcnMnLFxuXHRcdFx0J2FwcC5zZXJ2aWNlcycsXG5cdFx0XHQnYXBwLmRpcmVjdGl2ZXMnLFxuXHRcdFx0J2FwcC5yb3V0ZXMnLFxuXHRcdFx0J2FwcC5jb25maWcnXG5cdFx0XSk7XG5cblx0YW5ndWxhci5tb2R1bGUoJ2FwcC5yb3V0ZXMnLCBbJ3VpLnJvdXRlciddKTtcblx0YW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycsIFsnbmdNYXRlcmlhbCcsICd1aS5yb3V0ZXInLCAncmVzdGFuZ3VsYXInXSk7XG5cdGFuZ3VsYXIubW9kdWxlKCdhcHAuZmlsdGVycycsIFtdKTtcblx0YW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycsIFtdKTtcblx0YW5ndWxhci5tb2R1bGUoJ2FwcC5kaXJlY3RpdmVzJywgW10pO1xuXHRhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbmZpZycsIFsnbmdNYXRlcmlhbCddKTtcblxufSkoKTsiLCIoZnVuY3Rpb24oKXtcblx0XCJ1c2Ugc3RyaWN0XCI7XG5cblx0YW5ndWxhci5tb2R1bGUoJ2FwcC5yb3V0ZXMnKS5jb25maWcoIGZ1bmN0aW9uKCRzdGF0ZVByb3ZpZGVyLCAkdXJsUm91dGVyUHJvdmlkZXIgKSB7XG5cblx0XHR2YXIgZ2V0VmlldyA9IGZ1bmN0aW9uKCB2aWV3TmFtZSApe1xuXHRcdFx0cmV0dXJuICcvdmlld3MvYXBwLycgKyB2aWV3TmFtZSArICcvJyArIHZpZXdOYW1lICsgJy5odG1sJztcblx0XHR9O1xuXG5cdFx0JHVybFJvdXRlclByb3ZpZGVyLm90aGVyd2lzZSgnLycpO1xuXG5cdFx0JHN0YXRlUHJvdmlkZXJcblx0XHRcdC5zdGF0ZSgnbGFuZGluZycsIHtcblx0XHRcdFx0dXJsOiAnLycsXG5cdFx0XHRcdHZpZXdzOiB7XG5cdFx0XHRcdFx0bWFpbjoge1xuXHRcdFx0XHRcdFx0dGVtcGxhdGVVcmw6IGdldFZpZXcoJ2xhbmRpbmcnKVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSkuc3RhdGUoJ2xvZ2luJywge1xuXHRcdFx0dXJsOiAnL2xvZ2luJyxcblx0XHRcdHZpZXdzOiB7XG5cdFx0XHRcdG1haW46IHtcblx0XHRcdFx0XHR0ZW1wbGF0ZVVybDogZ2V0VmlldygnbG9naW4nKVxuXHRcdFx0XHR9LFxuXHRcdFx0XHRmb290ZXI6IHtcblx0XHRcdFx0XHR0ZW1wbGF0ZVVybDogZ2V0VmlldygnZm9vdGVyJylcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXG5cdH0gKTtcbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG5cdFwidXNlIHN0cmljdFwiO1xuXG5cdGFuZ3VsYXIubW9kdWxlKCdhcHAuY29uZmlnJykuY29uZmlnKCBmdW5jdGlvbigkbWRUaGVtaW5nUHJvdmlkZXIpIHtcblx0XHQvKiBGb3IgbW9yZSBpbmZvLCB2aXNpdCBodHRwczovL21hdGVyaWFsLmFuZ3VsYXJqcy5vcmcvIy9UaGVtaW5nLzAxX2ludHJvZHVjdGlvbiAqL1xuXHRcdCRtZFRoZW1pbmdQcm92aWRlci50aGVtZSgnZGVmYXVsdCcpXG5cdFx0XHQucHJpbWFyeVBhbGV0dGUoJ3RlYWwnKVxuXHRcdFx0LmFjY2VudFBhbGV0dGUoJ2N5YW4nKVxuXHRcdFx0Lndhcm5QYWxldHRlKCdyZWQnKTtcblx0fSk7XG5cbn0pKCk7Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
