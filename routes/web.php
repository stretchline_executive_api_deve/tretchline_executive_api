<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('reset_password/{token}', ['as' => 'password.reset', function($token)
{
    // implement your reset password route here!
}]);

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('/login', 'Auth\LoginController@postLogin');

Route::group(['prefix' => 'admin','middleware' => ['admin']], function () {
Route::get('/home', 'HomeController@index');
Route::get('/logout', 'HomeController@getSignOut');
Route::get('/getMilestones', 'HomeController@getMilestones');
Route::resource('/department', 'Admin\DepartmentController');
Route::resource('/grade', 'Admin\GradeController');
Route::resource('/designation', 'Admin\DesignationController');
Route::resource('/kpi', 'Admin\KpiController');
Route::resource('/kpiuser', 'Admin\KpiUserController');
Route::resource('/announce', 'Admin\AnnouncementController');
Route::resource('/users', 'Admin\UsersController');
Route::resource('/userlink', 'Admin\UserLinkController');
Route::resource('/milestone', 'Admin\MilestoneController');
Route::post('/copy_kpi_milestone', 'Admin\MilestoneController@copy_kpi_milestone');
Route::resource('/request_meeting', 'Admin\RequestMeetingController');
Route::resource('/notes', 'Admin\NotesController');
Route::get('/milestone/kpibyuser/{id}', 'Admin\MilestoneController@kpibyuser');
});



Route::group(['prefix' => 'user','middleware' => ['user']], function () {
Route::get('/home', 'User\HomeController@index');
Route::get('/my_overall', 'User\MyOverallController@index');
Route::get('/logout', 'User\HomeController@getSignOut');
Route::get('/getMilestones', 'User\HomeController@getMilestones');

Route::get('/kpi/create/{id}', 'User\KpiUsersController@kpiCreate');
Route::post('/kpi/store/', 'User\KpiUsersController@kpiStore');
Route::get('/milestone/create', 'User\KpiUsersController@milestoneCreate');
Route::get('/milestone/kpibyuser/{id}', 'User\KpiUsersController@kpibyuser');
Route::post('/milestone/store', 'User\KpiUsersController@milestoneStore');
Route::get('/meeting-request/{id}', 'User\MeetingRequestingController@index');//Ajax call
Route::get('/meeting-request/show/{id}', 'User\MeetingRequestingController@showAll');
Route::post('/meeting-request/state/{id}', 'User\MeetingRequestingController@updateRequest');
Route::get('/milestone/{id}', 'User\KpiUsersController@showMilestone');

Route::get('/milestone/edit/{id}', 'User\KpiUsersController@editMilestone');
Route::get('/milestone/copy/{id}', 'User\KpiUsersController@copyMilestone');
Route::post('/milestone/update/{id}', 'User\KpiUsersController@milestoneUpdate');
Route::post('/milestone/copyupdate', 'User\KpiUsersController@milestoneCopyUpdate');
Route::post('/milestone/stateupdate/{mid}/{kid}', 'User\KpiUsersController@stateupdate');

Route::get('/super_user/{id}', 'User\KpiUsersController@showSuperusers');
Route::get('/sub_users/{id}', 'User\KpiUsersController@index');
Route::get('/user_kpis/{id}', 'User\KpiUsersController@userKpiList');
Route::get('/admin_kpis/{id}', 'User\KpiUsersController@adminKpiList');
Route::post('/kpi/delete/{id}', 'User\KpiUsersController@deleteKpi');
Route::get('/milestone/delete/{id}', 'User\KpiUsersController@deleteMilestone');
Route::get('/announcement/{id}', 'User\AnnouncementController@index');
});

///Crons
Route::get('/m_deadline_inform', 'CronController@m_deadline_inform');
