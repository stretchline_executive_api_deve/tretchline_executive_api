<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted!'
            ]);

        });
        //get user informations for profile
        $api->post('user', 'App\Api\V1\Controllers\GetUserController@getAuthUser');
        $api->post('sub_super_list', 'App\Api\V1\Controllers\GetUserController@getSubSuperList');

        $api->post('overall', 'App\Api\V1\Controllers\KpiController@overallperf');
        $api->post('performance', 'App\Api\V1\Controllers\KpiController@singleperf');
        $api->post('subuserPerformance', 'App\Api\V1\Controllers\KpiController@sub_singleperf');

        $api->post('addNote', 'App\Api\V1\Controllers\NoteController@addNote');
        $api->post('addPnote', 'App\Api\V1\Controllers\NoteController@addPnote');
        $api->post('deletePnote', 'App\Api\V1\Controllers\NoteController@deletePnote');
        $api->post('announcePnote', 'App\Api\V1\Controllers\NoteController@announcePnote');

        $api->post('usersReport', 'App\Api\V1\Controllers\KpiController@milestoneReport');
        
        $api->post('requestMeeting', 'App\Api\V1\Controllers\CalendarController@requestMeeting');
        $api->post('userStateUpdate', 'App\Api\V1\Controllers\KpiController@stateUpdate');
        $api->post('subUserStateUpdate', 'App\Api\V1\Controllers\KpiController@subStateUpdate');

        $api->post('fcmToken', 'App\Api\V1\Controllers\FcmController@addFcm');




        
        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, refresh access token at each request.'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
