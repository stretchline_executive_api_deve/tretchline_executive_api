<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestMeeting extends Model
{
    protected $table = 'request_meetings';
    protected $fillable = array('user_id', 'milestone_id', 'title', 'message', 'meeting_date');


    public function requestState(){
    	return $this->hasMany('App\RequestState', 'request_id', 'id');
    }


       public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}