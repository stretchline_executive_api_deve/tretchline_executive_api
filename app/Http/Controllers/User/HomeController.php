<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\KpiUser;
use App\Milestone;
use Carbon\Carbon;
use App\UserLink;
use App\User;
use App\Department;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {   
       
        // =====Results Filter By User====
            $subUserArray = UserLink::where('user_id', '=', Auth::user()->id)->first();
            if ($subUserArray->subuser_id != null) {
                $subUserIdArray = unserialize($subUserArray->subuser_id);
            }else{
                $subUserIdArray = array();
            }
            // dd($subUserArray);
            $userList = User::whereIn('id', $subUserIdArray)->get(['id', 'name', 'department_id']);
        // ===================end filter User========================

         // =====Results Filter By Department====
            $dip_ids = $userList->pluck('department_id')->toArray();
            $departmentList = Department::whereIn('id', $dip_ids)->get(['id', 'name']);
        // ===================end filter depatment========================
          
            // dd($request->department_id);
        if ($request->department_id != null && isset($request->dipartment_filter)) {
            $intDipId = (int)$request->department_id;
            $totUsers = User::where('department_id', '=', $intDipId)
                            ->whereIn('id', $subUserIdArray)
                            ->count();
            $totUsersList = User::where('department_id', '=', $intDipId)
                                    ->whereIn('id', $subUserIdArray)
                                    ->get()
                                    ->pluck('id')->toArray();
        }elseif($request->user_id != null && isset($request->individual_filter)){
            $totUsers = 1;
        }else{
            $totUsers = User::whereIn('id', $subUserIdArray)->count();
        }
        // ===============Overall performance - All users==============
        
        
        if ($request->department_id != null && isset($request->dipartment_filter)) {
            $kpis = KpiUser::whereNotNull('milestone_id')
                                ->whereIn('user_id', $totUsersList)->get()->unique('kpi_id');
        }elseif($request->user_id != null && isset($request->individual_filter)){
            $kpis = KpiUser::where('user_id', '=', $request->user_id)->whereNotNull('milestone_id')->get()->unique('kpi_id');

        }elseif($request->start_date != null && $request->end_date != null && isset($request->time_filter)){
            //filter date range search
            $kpis = KpiUser::whereDate('created_at', '>=', $request->start_date)
                            ->whereDate('created_at', '<=', $request->end_date)
                            ->whereIn('user_id', $subUserIdArray)
                            ->whereNotNull('milestone_id')
                            ->get()
                            ->unique('kpi_id');
            // dd($kpis);
        }else{
        	//list down all sub users

            $kpis = KpiUser::whereIn('user_id', $subUserIdArray)
            				->whereNotNull('milestone_id')
            				->get()
            				->unique('kpi_id');
        }
        $Totalkpis = $kpis->count();  //Total numbers of KPI's
        //Overall next review date from all users
        //next review date
        //filter date range search
            if ($request->start_date != null && $request->end_date != null && isset($request->time_filter)) {
                $overall_next_review_d = Milestone::where('end_date', '>=', $request->end_date)
                                    ->min('end_date');
            }else{
                $overall_next_review_d = Milestone::where('end_date', '>', Carbon::yesterday())
                                    ->min('end_date');
            }
            

                if ($overall_next_review_d == null) {
                    $overall_next_review = '----/--/--';
                }else {
                    $overall_next_review = $overall_next_review_d;
                }

        //Response array
        $byKpi = array();
        $Completedkpis = 0;   //Completed KPI's
        $userTotMilestone = 0;
        $userTotCompMiles = 0;
        $totOverDueKpis = 0;
        $totOngingKpis = 0;
        foreach ($kpis as $key => $value) {

            $kpiInfo = KpiUser::with('kpi', 'user')
           ->where('kpi_id', '=', $value->kpi_id)
           ->first();

            //Overall performance
           $kpiMilestones = KpiUser::where('kpi_id', '=', $value->kpi_id)
           ->get(); 
            $totalMilestones = $kpiMilestones->count(); //Total number of milestones for single kpi
            $userTotMilestone = $userTotMilestone + $totalMilestones;
            $completedMilestones = $kpiMilestones->where('state', '=', 'Complete')
                                    ->count(); //Number of completed milestones for single kpi
            $userTotCompMiles = $userTotCompMiles + $completedMilestones;

            //filter date range search
            if ($request->start_date != null && $request->end_date != null && isset($request->time_filter)) {
                $re_end_date = $request->end_date;
                $milestoneInfo = KpiUser::with(['milestone' => function($query) use ($re_end_date){
                                    $query->where("end_date", "<", $re_end_date);
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->whereNotNull("milestone_id")
                                    ->where('state', '=', 'Incomplete')
                                    ->get();
            }else{
                 $milestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", "<", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->whereNotNull("milestone_id")
                                    ->where('state', '=', 'Incomplete')
                                    ->get();
            }
           
            $numOfDueMilestones = $milestoneInfo->where("milestone", "!=", null)->unique('kpi_id')->count();
            
            
            //ongoin milestone info
            //filter date range search
            if ($request->start_date != null && $request->end_date != null && isset($request->time_filter)) {
                $ree_end_date = $request->end_date;
                $ongoingMilestoneInfo = KpiUser::with(['milestone' => function($query) use ($ree_end_date){
                                    $query->where("end_date", ">", $ree_end_date);
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->whereNotNull('milestone_id')
                                    ->where('state', '!=', 'Complete')
                                    ->get();
            }else{
                $ongoingMilestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", ">", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->whereNotNull('milestone_id')
                                    ->where('state', '!=', 'Complete')
                                    ->get();
                                    // dd($ongoingMilestoneInfo);
                                    // print_r($value->kpi_id);
            }
            
             $numOfOngMilestones = $ongoingMilestoneInfo->unique('kpi_id')->count();
             // dd($numOfOngMilestones);
             if ($numOfDueMilestones != 0) {
                // $totOverDueKpis++;
                $kpiCurrentState = 'OVERDUE';
                // print_r($totOverDueKpis);
            }else if ($numOfOngMilestones != 0) {
                // $totOngingKpis++;
                $kpiCurrentState = 'ONGOING';
                // print_r($totOngingKpis);
            }
                //target end date
                        $target_end_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->max('end_date');
                                if ($target_end_d == null) {
                                    $target_end = '----/--/--';
                                }else {
                                    $target_end = $target_end_d;
                                }
                                if ($totalMilestones == $completedMilestones) {
                                     $kpiState = 100;
                                     $Completedkpis = $Completedkpis + 1;
                                     $next_review = '----/--/--';
                                     $kpiCurrentState = 'COMPLETE';
                                 }else{
                                    $completedWeight = 0;
                                    $totalWeight = 0;
                //next review date
                                    //filter date range search
                                    if ($request->start_date != null && $request->end_date != null && isset($request->time_filter)) {
                                        $next_review_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                                        ->where('end_date', '>=', $request->end_date)
                                                        ->min('end_date');
                                    }else{
                                        $next_review_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                                        ->where('end_date', '>', Carbon::yesterday())
                                                        ->min('end_date');
                                    }
                                    

                                    if ($next_review_d == null) {
                                        $next_review = '----/--/--';
                                    }else {
                                        $next_review = $next_review_d;
                                    }

                //loop each milestone for single kpi
                                    foreach ($kpiMilestones as $keyy => $valuee) {
                                        $milestoneInfo = Milestone::where('id', '=', $valuee->milestone_id)
                                        ->first();
                                        if ($valuee->state == 'Complete') {
                                            $completedWeight = $completedWeight + $milestoneInfo->weight;
                                        }
                                        if ($milestoneInfo != null) {
                                            $totalWeight = $totalWeight + $milestoneInfo->weight;
                                        }
                                        
                                    }
                //Calculate kpi state for incomplete kpi
                                    if ($completedWeight == 0 && $totalWeight == 0) {
                                        $kpiState = 0;
                                    }else {
                                        $kpiState = ($completedWeight / $totalWeight) *100; 
                                    }
                                    

                                }
                                if ($kpiCurrentState == 'OVERDUE') {
                                    $totOverDueKpis++;
                                }elseif ($kpiCurrentState == 'ONGOING') {
                                    $totOngingKpis++;
                                }
                                
                                $byKpi [] = array('kpi_id' => $value->kpi_id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'target_end' => $target_end, 'next_review' => $next_review, 'kpi_state' => round($kpiState), 'kpi_user_id' => $kpiInfo->user->id, 'kpi_user_name' => $kpiInfo->user->name, 'kpi_user_img' => $kpiInfo->user->profile_image, 'kpiCurrentState' => $kpiCurrentState);
                            }   
                            /// calculate presantage

                            if ($Totalkpis != 0) {
                                $completeKpiPre = ($Completedkpis/$Totalkpis)*100;
                                $ongoinKpiPre = ($totOngingKpis/$Totalkpis)*100;
                                $overdueKpiPre = ($totOverDueKpis/$Totalkpis)*100;
                            } else {
                                $completeKpiPre = 0;
                                $ongoinKpiPre = 0;
                                $overdueKpiPre = 0;
                            }

               //Return data 
               //
                            $overallPerf = array('completed_kpis' => $Completedkpis , 'total_kpis' => $Totalkpis,'totOverDueKpis' => $totOverDueKpis, 'totOngingKpis' => $totOngingKpis, 'totUsers' => $totUsers, 'overall_next_review' => $overall_next_review, 'completeKpiPre' => (int) round($completeKpiPre,0), 'ongoinKpiPre' => (int) round($ongoinKpiPre,0), 'overdueKpiPre' => (int) round($overdueKpiPre,0));

        // ==============end overall performace===============
        // dd($overallPerf);
        return view('pages.user.home.index', compact('byKpi', 'overallPerf', 'departmentList', 'userList'));
    }

     public function getSignOut()
    {
        // Session::flush();
        Auth::logout();
        return redirect('/');
        return Redirect::route('/');

    }

 
    public function getMilestones(Request $request){
        // $user = JWTAuth::toUser($request->token);

                            $kpiInfo = KpiUser::with('kpi')
                            ->where('user_id', '=', $request->user_id)
                            ->where('kpi_id', '=', $request->kpi_id)
                            ->first();
        //Retrive kpi milestones
                            $kpiMilestones = KpiUser::where('kpi_id', '=', $request->kpi_id)
                            ->where('user_id', '=', $request->user_id)
                            ->get();

        //next review date
                            $next_review = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->where('end_date', '>', Carbon::now())
                            ->min('end_date'); 
        //target end date
                            $kpi_targetend = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->max('end_date'); 

        //Array
                            $milestoneList = array();
        //Total Milestone presantage
                            $milestoneTotWeight = Milestone::where('kpi_id', '=', $request->kpi_id)
                                                    ->sum('weight');

                            // $milestoneTotWeight = 0;
                            foreach ($kpiMilestones as $key => $value) {
            //milestone complete or not
                            $milestoneStatus = $value->state;

            //Basic infomation of milestone
                            $milestoneInfo = Milestone::where('id', '=', $value->milestone_id)
                                ->first();
            // $milestoneTotWeight = $milestoneTotWeight + $milestoneInfo->weight ; //Total milestone weight
                             $milestonePercentage = round($milestoneInfo->weight*100/$milestoneTotWeight);

            //Add data to array
            $milestoneList [] = array('milestone_id' => $milestoneInfo->id, 'milestone_state' => $milestoneStatus, 'milestone_percentage' => $milestonePercentage, 'end_date' => $milestoneInfo->end_date, 'milestone_name' => $milestoneInfo->name, 'milestone_description' => $milestoneInfo->description);
        }

        $kpiOverall = array('kpi_id'=> $kpiInfo->kpi->id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'next_review' => $next_review, 'kpi_targetend' =>$kpi_targetend);

            //Response
        return response()->json(['kpiOverall' => $kpiOverall, 'milestone_data' => $milestoneList, 'totalWeight' => $milestoneTotWeight]);
    }

}
