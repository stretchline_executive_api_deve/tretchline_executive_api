<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\KpiCreateRequest;
use App\Http\Requests\MilestoneCreateRequest;
use App\Http\Requests\CopyKPIRequest;
use App\Http\Requests\SubuserMilestoneCreateRequest;
use App\Kpi;
use App\KpiUser;
use App\Milestone;
use App\User;
use App\UserLink;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Carbon\Carbon;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class KpiUsersController extends Controller
{
        //

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $sub_users_link = UserLink::where('user_id', $id)->with('user')->first();
        if ($sub_users_link->subuser_id != null) {
            $unserial       = unserialize($sub_users_link->subuser_id);
            $sub_users      = User::whereIn('id', $unserial)->get();
        }else{
            $sub_users = null;
        }
        

        return view('pages.user.subusers.index', compact('sub_users'));
    }
    public function showSuperusers($id)
    {
        $super_users_link = UserLink::where('user_id', $id)->with('user')->first();
        if ($super_users_link->superuser_id != null) {
            $super_users      = User::where('id', $super_users_link->superuser_id)->first();
        }else{
            $super_users = null;
        }
        
        return view('pages.user.superusers.index', compact('super_users', 'super_users_link'));
    }


    public function userKpiList($id)
    {
        $kpis       = KpiUser::with('kpi')->where('user_id', $id)->with('kpi')->get();
        $users_kpi = $kpis->unique('kpi_id');
        // dd($users_kpi);
        $users_kpis = array(); 
        foreach ($users_kpi as $value) {
            $submitCount = KpiUser::where('kpi_id', '=', $value->kpi_id)
                                    ->where('state', '=', 'Submitted')
                                    ->count();
            $kpiInfo = Kpi::where('id', '=', $value->kpi_id)->first();
            $milestone_nullcount = KpiUser::where('kpi_id', '=', $value->kpi_id)
                                    ->where('milestone_id', '=', null)
                                    ->count();
            ///Check incomplete overdue count
            $milestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", "<", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->where('state', '=', 'Incomplete')
                                    ->get();
            $numOfDueMilestones = $milestoneInfo->where("milestone", "!=", null)->count();

            // ================================Overall - KPI======================
                   $kpiMilestones = KpiUser::where('kpi_id', '=', $value->kpi_id)->whereNotNull('milestone_id')->get();
                    $totalMilestones = $kpiMilestones->count(); //Total number of milestones for single kpi
                    $completedMilestones = $kpiMilestones->where('state', '=', 'Complete')
                                                ->count(); //Number of completed milestones for single kpi
                    if ($totalMilestones == $completedMilestones) {
                        $kpiState = 100;
                    }

                       //loop each milestone for single kpi
                    $completedWeight = 0;
                    $totalWeight = 0;
                    //loop each milestone
                        foreach ($kpiMilestones as $valuee) {
                            $milestoneInfo = Milestone::where('id', '=', $valuee->milestone_id)
                            ->first();
                            if ($valuee->state == 'Complete') {
                                $completedWeight = $completedWeight + $milestoneInfo->weight;
                            }
                            if ($milestoneInfo != null) {
                                // dd((int)$milestoneInfo->weight);
                                $totalWeight = $totalWeight + (int)$milestoneInfo->weight;
                            }
                            
                        }
                        //get user info
                        // $kpiuserDetails = User::where('id', $kpiuserInfo->user_id)->first();
                        // if($kpiuserDetails != null){
                        //     $kpi_user = $kpiuserDetails->name;
                        // }else{
                        //     $kpi_user = '--';
                        // }

                        // dd($value->user_id);
                        if ($completedWeight == 0 && $totalWeight == 0) {
                            $kpiState = 0;
                        }else {
                            $kpiState = ($completedWeight / $totalWeight) *100; 
                        }
            // ====================End Overall - KPI ======================
            $users_kpis [] = array('kpi_id'=> $value->kpi_id, 'title' => $kpiInfo->title, 'description' => $kpiInfo->description, 'created_at' => $value->created_at, 'submitted' => $submitCount, 'user_id' => $value->user_id, 'milestone_nullcount'=> $milestone_nullcount, 'numOfDueMilestones'=> $numOfDueMilestones, 'kpiState' => $kpiState); 
        }
        //userInfor
        $userInfo = User::with('designation')->where('id', $id)->first();

        return view('pages.user.kpi_user.index', compact('users_kpis', 'userInfo'));
    }


    public function adminKpiList($id)
    {
        $kpis       = KpiUser::where('user_id', $id)->with('kpi')->get();
        $users_kpis = $kpis->unique('kpi_id');

        return view('pages.user.kpi_admin.index', compact('users_kpis'));
    }

    public function kpiCreate($id)
    {
        // $users_kpis = KpiUser::where('user_id',$id)->with('kpi')->get();
        $sub_users_link = UserLink::where('user_id', $id)->with('user')->first();
        if ($sub_users_link->subuser_id != null) {
            $unserial       = unserialize($sub_users_link->subuser_id);
            $userList       = User::whereIn('id', $unserial)->get();
        }else{
            $userList = null;
        }
        
        return view('pages.user.kpi_user.create', compact('users_kpis', 'userList'));
    }
    
    public function kpiStore(KpiCreateRequest $request)
    {
        DB::transaction(function () use ($request) {

            $input = $request->only(
                'title',
                'description'
            );

            $kpi = Kpi::create($input);

            $kpi_user               = new KpiUser();
            $kpi_user->kpi_id       = $kpi->id;
            $kpi_user->user_id      = $request->user_id;
            $kpi_user->milestone_id = null;
            $kpi_user->state        = null;
            $kpi_user->save();
        });

        // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $request->user_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | KPI's Info");
                // $notificationBuilder->setBody("You have new KPI., '".$request->title."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }

                if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | KPI's Info", 'body'  => "You have new KPI., '".$request->title."'.");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | KPI's Info",
                            'body'  => "You have new KPI., '".$request->title."'."
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================
            
        if (isset($request->add_milestone)) {
            return redirect('/user/milestone/create');
        }else {
            return redirect('/user/home');
        }

        

    }

    public function milestoneCreate()
    {
        $user = Auth::user();
        $alluser = User::all();
        $subuserarray = UserLink::where('user_id', '=', $user->id)->first();
        $userInfoArray = array();
        if ($subuserarray->subuser_id != null) {
            foreach (unserialize($subuserarray->subuser_id) as $value) {
            $userInfo = User::where('id', '=', $value)->first();
            // dd($userInfo);
            $userInfoArray [] = array('user_id' => $value, 'user_name' => $userInfo['name']);
        }
        }
        
        // dd($userInfoArray);
        return view('pages.user.milestone.create', compact('userInfoArray'));
    }
 
    public function kpibyuser($id){
        $userKpis = KpiUser::with('kpi')
                                ->where('user_id', '=', $id)
                                ->get();
         $userKpiList =  $userKpis->unique('kpi_id')->values();               
         // dd($userKpiList[0]->kpi->title);
         $kpiInfo = array();
         foreach ($userKpiList as $value) {
             
             $kpiInfo [] = array($value->kpi_id => $value->kpi->title);
         }

         // dd($kpiInfo);
         return json_encode($kpiInfo);
    }

    public function milestoneStore(SubuserMilestoneCreateRequest $request)
    {

        DB::transaction(function () use ($request) {
            $users_kpis = KpiUser::where('kpi_id', $request->kpi_id)->first();

            for ($i = 0; $i < count($request->name); $i++) {
                $milestone              = new Milestone();
                $milestone->kpi_id      = $request->kpi_id;
                $milestone->name        = $request->name[$i];
                $milestone->description = $request->description[$i];
                $milestone->weight      = $request->weight[$i];
                $milestone->start_date  = $request->start_date[$i];
                $milestone->end_date    = $request->end_date[$i];
                $milestone->save();

                $KpiUser               = new KpiUser();
                $KpiUser->kpi_id       = $request->kpi_id;
                $KpiUser->user_id      = $users_kpis->user_id;
                $KpiUser->milestone_id = $milestone->id;
                $KpiUser->state        = 'Incomplete';
                $KpiUser->save();

                // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $users_kpis->user_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Milestone Info");
                // $notificationBuilder->setBody("You have new Milestone named, '".$request->name[$i]."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }


                // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Milestone Info", 'body'  => "You have new Milestone named, '".$request->name[$i]."'.");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Milestone Info",
                            'body'  => "You have new Milestone named, '".$request->name[$i]."'." 
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }

            // ===========================================
            }
            $users_kpis->delete();
        });

        return redirect('user/milestone/create');
    }

    public function showMilestone($id)
    {

        //kpi_id = $id
        $kpi           = Kpi::find($id);
        $milestonelist = KpiUser::where('kpi_id', $id)->whereNotNull('milestone_id')->get();
        $sum0fweight = Milestone::where('kpi_id', $id)->sum('weight');
        $completedtask = Milestone::where('kpi_id', $id)->get();
          $sumoftask = 0;
        foreach ($milestonelist as $key => $value) {
        

           if($value->state == 'Complete'){
// dd();
            $sumoftask =$sumoftask  + $value->milestone->weight;
            // dd($sumoftask);
          }
          
        }
       // dd( $completedtask );
        return view('pages.user.milestone.index', compact('milestonelist', 'kpi','sum0fweight','sumoftask'));

    }


       public function editMilestone($id)
    {
// dd('asdasd');
        //kpi_id = $id
        $kpi           = Kpi::find($id);
        $milestonelist = KpiUser::where('kpi_id', $id)->whereNotNull('milestone_id')->get();
        $sum0fweight = Milestone::where('kpi_id', $id)->sum('weight');
        $completedtask = Milestone::where('kpi_id', $id)->get();
        
        foreach ($milestonelist as $key => $value) {
          $sumoftask = 0;

           if($value->state == 'Complete'){
// dd();
            $sumoftask =$sumoftask  + $value->milestone->weight;

          }
          
        }
       
        return view('pages.user.kpi_user.edit', compact('milestonelist', 'kpi','sum0fweight','sumoftask'));

    }
           public function copyMilestone($id)
    {
// dd('asdasd');
        //kpi_id = $id
        $kpi           = Kpi::find($id);
        $milestonelist = KpiUser::where('kpi_id', $id)->whereNotNull('milestone_id')->get();
        $sum0fweight = Milestone::where('kpi_id', $id)->sum('weight');
        $completedtask = Milestone::where('kpi_id', $id)->get();
        $subuserArray = UserLink::where('user_id', '=', Auth::user()->id)->first();
        $userList = User::whereIn('id', unserialize($subuserArray->subuser_id))->get(['id', 'name']);
        // dd(unserialize($subuserArray->subuser_id));
        foreach ($milestonelist as $key => $value) {
          $sumoftask = 0;

           if($value->state == 'Complete'){
// dd();
            $sumoftask =$sumoftask  + $value->milestone->weight;

          }
          
        }
       
        return view('pages.user.kpi_user.copy', compact('milestonelist', 'kpi','sum0fweight','sumoftask', 'userList'));

    }
public function milestoneUpdate(Request $request, $id)
    {

          $kpi    = Kpi::find($id);
          $kpi->update(['title'=>$request->title]);
            for ($i = 0; $i < count($request->name); $i++) {
                $milestone              = Milestone::find($request->milestone_id[$i]);             
                $milestone->name        = $request->name[$i];
                $milestone->description = $request->description[$i];
                $milestone->weight      = $request->weight[$i];
                $milestone->start_date  = $request->start_date[$i];
                $milestone->end_date    = $request->end_date[$i];
                $milestone->save();
                $kpiuser              = KpiUser::where('milestone_id', $request->milestone_id[$i])->where('kpi_id',$id)->first();
                $kpiuser->state       = $request->state[$i];
                $kpiuser->save();

            // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $kpiuser->user_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Milestone Info");
                // $notificationBuilder->setBody("You have new Milestone update for, '".$request->name[$i]."' and state updated to '".$request->state[$i]."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }
                // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Milestone Info", 'body'  => "You have new Milestone update for, '".$request->name[$i]."' and state updated to '".$request->state[$i]."'." );

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Milestone Info",
                            'body'  => "You have new Milestone update for, '".$request->name[$i]."' and state updated to '".$request->state[$i]."'." 
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================

            }
          
    

        return redirect('user/user_kpis/'.$kpi->kpiuser[0]->user->id);
    }

    ///Copy update store
    public function milestoneCopyUpdate(CopyKPIRequest $request)
    {
        // dd($request->all());
          $kpi    = Kpi::create(['title'=>$request->title, 'description'=> $request->kpidescription]);
          // dd($kpi->id);
            for ($i = 0; $i < count($request->name); $i++) {
                $milestone  = Milestone::create(['kpi_id' => $kpi->id, 'name' => $request->name[$i], 'description' => $request->description[$i], 'weight' => $request->weight[$i], 'start_date' => $request->start_date[$i], 'end_date' => $request->end_date[$i]]); 
                // dd($milestone->id);
                $kpiuser = KpiUser::create(['milestone_id' => $milestone->id, 'user_id' => $request->user_id, 'kpi_id' => $kpi->id, 'state' => $request->state[$i]]);

            // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $kpiuser->user_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Milestone Info");
                // $notificationBuilder->setBody("You have new Milestone update for, '".$request->name[$i]."' and state updated to '".$request->state[$i]."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }

                // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Milestone Info", 'body'  => "You have new Milestone update for, '".$request->name[$i]."' and state updated to '".$request->state[$i]."'.");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Milestone Info",
                            'body'  => "You have new Milestone update for, '".$request->name[$i]."' and state updated to '".$request->state[$i]."'."
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================

            }
          
    

        return redirect('user/user_kpis/'.$kpiuser->user_id);
    }



    public function deleteKpi(Request $request, $id)
    {

        DB::transaction(function () use ($request, $id) {
            $kpi       = Kpi::find($id);
            $kpiuser   = KpiUser::where('kpi_id', $id);
            $milestone = Milestone::where('kpi_id', $id);
            // dd($id);
            $kpi->delete();
            $kpiuser->delete();
            $milestone->delete();
        });
        return redirect('/user/user_kpis/' . $request->user_id);

    }
 public function deleteMilestone($id)
    {
    
        DB::transaction(function () use ($id) {
            $Milestone       = Milestone::find($id);
            $kpiuser   = KpiUser::where('milestone_id', $id);
            $kpi = Kpi::where('kpi_id', $Milestone->kpi_id);
            // dd($id);
            $kpi->delete();
            $kpiuser->delete();
            $milestone->delete();
        });
        return redirect('/user/user_kpis/' . $Milestone->kpiuser[0]->user_id);

    }

    ///own status update
    public function stateupdate(Request $request, $mid, $kid)
    {
        $user = Auth::user();
        // dd($request->state);
        $KpiUser = KpiUser::where('user_id', '=', $user->id)
                ->where('kpi_id', '=', $kid)
                ->where('milestone_id', '=', $mid)
                ->first();
        if ($KpiUser->update($request->all())) {
            // ===========FCM=====================
                $uperUserInfo = UserLink::where('user_id', '=', $user->id)->first();
                $pushyToken = FcmData::where('user_id', '=', $uperUserInfo->superuser_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Subuser Milestone Status");
                //     $notificationBuilder->setBody("Your subuser has updated status as follow,\nSubuser Name: ".$user->name."\nMilestone ID: ".$mid."\nKPI ID: ".$kid."\nStatus: ".$request->state."")
                //                         ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }
                    // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('message' => "Stretchline | Subuser Milestone Status", 'body'  => "Your subuser has updated status as follow,\nSubuser Name: ".$user->name."\nMilestone ID: ".$mid."\nKPI ID: ".$kid."\nStatus: ".$request->state."");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'message' => "Stretchline | Subuser Milestone Status",
                            'body'  => "Your subuser has updated status as follow,\nSubuser Name: ".$user->name."\nMilestone ID: ".$mid."\nKPI ID: ".$kid."\nStatus: ".$request->state.""
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================
            return back()->with('success', 'State update successfully');
        }else {
            return back()->with('error', 'State update fail.');
        }
    }



}
