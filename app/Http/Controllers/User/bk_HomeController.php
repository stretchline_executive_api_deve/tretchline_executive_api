<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\KpiUser;
use App\Milestone;
use Carbon\Carbon;
use App\UserLink;
use App\User;

class HomeController extends Controller
{
    //

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //User information
        $user = Auth::user();
        $userInfo = User::with('grade', 'designation', 'department')->where('id',$user->id)->first();
        // dd($userInfo);
        // =============================
        $kpis = KpiUser::where('user_id', '=', $user->id)->get()->unique('kpi_id');
        $Totalkpis = $kpis->count();  //Total numbers of KPI's
        //Response array
        $UserKpi = array();
        $Completedkpis = 0;   //Completed KPI's
        $userTotMilestone = 0;
        $userTotCompMiles = 0;
        $userTotOverDueMiles = 0;
        foreach ($kpis as $key => $value) {

            $kpiInfo = KpiUser::with('kpi')
           ->where('user_id', '=', $user->id)
           ->where('kpi_id', '=', $value->kpi_id)
           ->first();

            //Overall performance
           $kpiMilestones = KpiUser::where('kpi_id', '=', $value->kpi_id)
           ->where('user_id', '=', $user->id)
           ->get(); 
            $totalMilestones = $kpiMilestones->count(); //Total number of milestones for single kpi
            $userTotMilestone = $userTotMilestone + $totalMilestones;
            $completedMilestones = $kpiMilestones->where('state', '=', 'Complete')
                                    ->count(); //Number of completed milestones for single kpi
            $userTotCompMiles = $userTotCompMiles + $completedMilestones;

            $milestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", "<", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->where('state', '=', 'Incomplete')
                                    ->get();
            $numOfDueMilestones = $milestoneInfo->where("milestone", "!=", null)->count();
            $userTotOverDueMiles = $userTotOverDueMiles + $numOfDueMilestones;
                //target end date
                        $target_end_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->max('end_date');
                                if ($target_end_d == null) {
                                    $target_end = '----/--/--';
                                }else {
                                    $target_end = $target_end_d;
                                }
                                if ($totalMilestones == $completedMilestones) {
                                     $kpiState = 100;
                                     $Completedkpis = $Completedkpis + 1;
                                     $next_review = '----/--/--';
                                 }else{
                                    $completedWeight = 0;
                                    $totalWeight = 0;
                //next review date
                                    $next_review_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->where('end_date', '>', Carbon::yesterday())
                                    ->min('end_date');

                                    if ($next_review_d == null) {
                                        $next_review = '----/--/--';
                                    }else {
                                        $next_review = $next_review_d;
                                    }

                //loop each milestone for single kpi
                                    foreach ($kpiMilestones as $keyy => $valuee) {
                                        $milestoneInfo = Milestone::where('id', '=', $valuee->milestone_id)
                                        ->first();
                                        if ($valuee->state == 'Complete') {
                                            $completedWeight = $completedWeight + $milestoneInfo->weight;
                                        }
                                        if ($milestoneInfo != null) {
                                            $totalWeight = $totalWeight + $milestoneInfo->weight;
                                        }
                                        

                                    }
                //Calculate kpi state for incomplete kpi
                                    if ($completedWeight == 0 && $totalWeight == 0) {
                                        $kpiState = 0;
                                    }else {
                                        $kpiState = ($completedWeight / $totalWeight) *100; 
                                    }
                                    

                                }

                                $UserKpi [] = array('kpi_id' => $value->kpi_id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'target_end' => $target_end, 'next_review' => $next_review, 'kpi_state' => round($kpiState));
                            }


                                

               //Return data

                            $UserOverall = array('completed_kpis' => $Completedkpis , 'total_kpis' => $Totalkpis, 'userTotMilestone' => $userTotMilestone, 'userTotCompMiles' => $userTotCompMiles, 'userTotOverDueMiles' => $userTotOverDueMiles, 'designation'=> $userInfo->designation->name);

                            return view('pages.user.home.index', compact('UserOverall', 'UserKpi'));

        // =============================
        // return view('pages.user.home.index');
    }

     public function getSignOut()
    {
        // Session::flush();
        Auth::logout();
        return redirect('/');
        // return Redirect::route('/');

    }
}
