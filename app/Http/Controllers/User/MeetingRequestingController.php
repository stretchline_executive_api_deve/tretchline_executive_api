<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Http\Requests\MilestoneCreateRequest;
use Illuminate\Support\Facades\Redirect;
use App\Announcement;
use App\AnnounceUser;
use App\User;
use App\UserLink;
use App\Kpi;
use App\KpiUser;
use App\Milestone;
use App\RequestMeeting;
use App\RequestState;
use DB;
use App\Mail\RequestStates;
use Mail;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class MeetingRequestingController extends Controller
{
      //

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('user');
    }
    
    //
     public function index($id)
    {
    	$request_meeting = RequestState::where('receiver_id',$id)->where('state','Pending')->with('requestMeeting')->get();      
        // $request_meeting = RequestMeeting::where('user_id',$id)->first();      
        // $sub_users = User::whereIn('id',$unserial)->get();
         return response()->json($request_meeting);
    }

  public function showAll($id)
    {
    	
    	$request_meeting = RequestState::where('receiver_id',$id)->get();  
        
        // $sub_users = User::whereIn('id',$unserial)->get();
 // dd($request->);
         return view('pages.user.meeting_request.index', compact('request_meeting'));
    }
  public function updateRequest(Request $request , $id)
    {
  
       RequestState::where('receiver_id', $request->receiver_id)->where('request_id',$id)->update(['state' => $request->state]);
       $requestInfo = RequestMeeting::where('id', '=', $id)->first();
       $senderInfo = User::where('id', '=', $requestInfo->user_id)->first();
       $receiverInfo = User::where('id', '=', $request->receiver_id)->first();
        Mail::to($senderInfo->email)->send(new RequestStates($senderInfo->name, $requestInfo->milestone_id, $requestInfo->title, $requestInfo->message, $requestInfo->meeting_date, $receiverInfo->name, $request->state, $senderInfo->id));
        // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', $requestInfo->user_id)->first();
                    // if ($fcmToken != null) {
                        
                    
                    // $optionBuilder = new OptionsBuilder();
                    // $optionBuilder->setTimeToLive(60*20);

                    // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Request Meeting Status");
                    // $notificationBuilder->setBody("Your request status updated as follow,\nReceiver Name: ".$receiverInfo->name."\nMilestone ID: ".$requestInfo->milestone_id."\nRequest Title: ".$requestInfo->title."\nRequest Message: ".$requestInfo->message."\nMeeting Date: ".$requestInfo->meeting_date."\nStatus: ".$request->state."\n")
                    //                     ->setSound('default');
                                        
                    // $dataBuilder = new PayloadDataBuilder();
                    // $dataBuilder->addData(['a_data' => 'my_data']);

                    // $option = $optionBuilder->build();
                    // $notification = $notificationBuilder->build();
                    // $data = $dataBuilder->build();

                    // $token = $fcmToken->token;

                    // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                    // }

                    // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Request Meeting Status", 'body'  => "Your request status updated as follow,\nReceiver Name: ".$receiverInfo->name."\nMilestone ID: ".$requestInfo->milestone_id."\nRequest Title: ".$requestInfo->title."\nRequest Message: ".$requestInfo->message."\nMeeting Date: ".$requestInfo->meeting_date."\nStatus: ".$request->state."\n");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Request Meeting Status",
                            'body'  => "Your request status updated as follow,\nReceiver Name: ".$receiverInfo->name."\nMilestone ID: ".$requestInfo->milestone_id."\nRequest Title: ".$requestInfo->title."\nRequest Message: ".$requestInfo->message."\nMeeting Date: ".$requestInfo->meeting_date."\nStatus: ".$request->state."\n"
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }

            // ===========================================
        return redirect('/user/meeting-request/show/' .$request->receiver_id);

    }




}
