<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnnounceCreateRequest;
use App\Announcement;
use App\AnnounceUser;
use App\User;
use Carbon\Carbon;

class AnnouncementController extends Controller
{
        //

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('user');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
    public function index($id) 
    {
   
        $privateAnnounces = AnnounceUser::with('announcement')->where('user_id', '=', $id)->get();
        $publicAnnounces = Announcement::where('user_type', '=', '0')
                                            ->where('start_date', '<=', Carbon::today()->format('Y-m-d'))
                                            ->where('end_date', '>=', Carbon::today()->format('Y-m-d'))
                                            ->get();
        // dd($privateAnnounces);
        return view('pages.user.announcement.index', compact('publicAnnounces','privateAnnounces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     $userList = User::all();
    //     return view('pages.admin.announce.create', compact('userList'));
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(AnnounceCreateRequest $request)
    // {
    //     if ($createAnnounce = Announcement::create($request->only('title', 'description', 'user_type'))) {
    //         if ($request->user_type == '1') {
    //             // var_dump($request->user_id);
    //             foreach ($request->user_id as $value) {
    //                 $createUserby = AnnounceUser::create(['announce_id' => $createAnnounce->id, 'user_id'=>$value]);
    //             }
                
    //                 return redirect()->route('announce.create')
    //                                 ->with('success', 'Private announce created successfully');

                
    //         }else{
    //             return redirect()->route('announce.create')
    //             ->with('success', 'Public announce created successfully');
    //         }


            
    //     }else{
    //         return redirect()->route('announce.create')
    //             ->with('error', 'Announcement create fail');
    //     }
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     $announceInfo = Announcement::where('id', '=', $id)->first();
    //     $announceUserInfo = AnnounceUser::where('announce_id', '=', $id)->get(['user_id']);
    //     $userList = User::all();
    //     return view('pages.admin.announce.edit', compact('announceInfo', 'announceUserInfo', 'userList'));

    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}
