<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\KpiUser;
use App\Milestone;
use Carbon\Carbon;
use App\UserLink;
use App\User;

class MyOverallController extends Controller
{
    //

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //User information
        $user = Auth::user();
        $userInfo = User::with('grade', 'designation', 'department')->where('id',$user->id)->first();

        // ===============Overall performance - Current user==============
        // $totUsers = User::where('is_admin', '=', '0')->count();
        $kpis = KpiUser::whereNotNull('milestone_id')->where('user_id', '=', $user->id)->get()->unique('kpi_id');
        // dd($kpis);
        $Totalkpis = $kpis->count();  //Total numbers of KPI's
        //Overall next review date from all users
        //next review date
            $overall_next_review_d = Milestone::where('end_date', '>', Carbon::yesterday())
                                    ->min('end_date');

                if ($overall_next_review_d == null) {
                    $overall_next_review = '----/--/--';
                }else {
                    $overall_next_review = $overall_next_review_d;
                }

        //Response array
        $byKpi = array();
        $Completedkpis = 0;   //Completed KPI's
        $userTotMilestone = 0;
        $userTotCompMiles = 0;
        $totOverDueKpis = 0;
        $totOngingKpis = 0;
        foreach ($kpis as $key => $value) {

            $kpiInfo = KpiUser::with('kpi', 'user')
            ->where('user_id', '=', $user->id)
           ->where('kpi_id', '=', $value->kpi_id)
           ->first();
           // dd($value);
            //Overall performance
           $kpiMilestones = KpiUser::where('kpi_id', '=', $value->kpi_id)
                                    ->where('user_id', '=', $user->id)
                                    ->get(); 
            $totalMilestones = $kpiMilestones->count(); //Total number of milestones for single kpi
            $userTotMilestone = $userTotMilestone + $totalMilestones;
            $completedMilestones = $kpiMilestones->where('state', '=', 'Complete')
                                    ->count(); //Number of completed milestones for single kpi
            $userTotCompMiles = $userTotCompMiles + $completedMilestones;
            //over due mileston info
            $milestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", "<", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->where('state', '=', 'Incomplete')
                                    ->get();
                                    // dd($milestoneInfo);
            $numOfDueMilestones = $milestoneInfo->where("milestone", "!=", null)->unique('kpi_id')->count();
            
            
            //ongoin milestone info
            $ongoingMilestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", ">", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->whereNotNull('milestone_id')
                                    ->where('state', '!=', 'Complete')
                                    ->get();
                                    // dd($ongoingMilestoneInfo);
             $numOfOngMilestones = $ongoingMilestoneInfo->unique('kpi_id')->count();
             if ($numOfDueMilestones != 0) {
                // $totOverDueKpis = ++$totOverDueKpis;
                $kpiCurrentState = 'OVERDUE';
            }else if ($numOfOngMilestones != 0) { 
                // $totOngingKpis = ++$totOngingKpis;
                $kpiCurrentState = 'ONGOING';
            }
                //target end date
                        $target_end_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->max('end_date');
                                if ($target_end_d == null) {
                                    $target_end = '----/--/--';
                                }else {
                                    $target_end = $target_end_d;
                                }
                                if ($totalMilestones == $completedMilestones) {
                                     $kpiState = 100;
                                     $Completedkpis = $Completedkpis + 1;
                                     $next_review = '----/--/--';
                                     $kpiCurrentState = 'COMPLETE';
                                 }else{
                                    $completedWeight = 0;
                                    $totalWeight = 0;
                //next review date
                                    $next_review_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->where('end_date', '>', Carbon::yesterday())
                                    ->min('end_date');

                                    if ($next_review_d == null) {
                                        $next_review = '----/--/--';
                                    }else {
                                        $next_review = $next_review_d;
                                    }

                //loop each milestone for single kpi
                                    foreach ($kpiMilestones as $keyy => $valuee) {
                                        $milestoneInfo = Milestone::where('id', '=', $valuee->milestone_id)
                                        ->first();
                                        if ($valuee->state == 'Complete') {
                                            $completedWeight = $completedWeight + $milestoneInfo->weight;
                                        }
                                        if ($milestoneInfo != null) {
                                            $totalWeight = $totalWeight + $milestoneInfo->weight;
                                        }
                                        
                                    }
                //Calculate kpi state for incomplete kpi
                                    if ($completedWeight == 0 && $totalWeight == 0) {
                                        $kpiState = 0;
                                    }else {
                                        $kpiState = ($completedWeight / $totalWeight) *100; 
                                    }
                                    

                                }
                                if ($kpiCurrentState == 'OVERDUE') {
                                    $totOverDueKpis++;
                                }elseif ($kpiCurrentState == 'ONGOING') {
                                    $totOngingKpis++;
                                }
                                $byKpi [] = array('kpi_id' => $value->kpi_id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'target_end' => $target_end, 'next_review' => $next_review, 'kpi_state' => round($kpiState), 'kpi_user_id' => $kpiInfo->user->id, 'kpi_user_name' => $kpiInfo->user->name, 'kpi_user_img' => $kpiInfo->user->profile_image, 'kpiCurrentState' => $kpiCurrentState);
                            }   
                            /// calculate presantage
                            if ($Totalkpis != 0) {
                                $completeKpiPre = ($Completedkpis/$Totalkpis)*100;
                                $ongoinKpiPre = ($totOngingKpis/$Totalkpis)*100;
                                $overdueKpiPre = ($totOverDueKpis/$Totalkpis)*100;
                            } else {
                                $completeKpiPre = 0;
                                $ongoinKpiPre = 0;
                                $overdueKpiPre = 0;
                            }
                            

                            
               //Return data 
               //
                            $overallPerf = array('completed_kpis' => $Completedkpis , 'total_kpis' => $Totalkpis,'totOverDueKpis' => $totOverDueKpis, 'totOngingKpis' => $totOngingKpis, 'overall_next_review' => $overall_next_review, 'completeKpiPre' => (int) round($completeKpiPre,0), 'ongoinKpiPre' => (int) round($ongoinKpiPre,0), 'overdueKpiPre' => (int) round($overdueKpiPre,0));

        // ==============end overall performace===============
        // dd($byKpi);
        return view('pages.user.my_overall.index', compact('byKpi', 'overallPerf'));
    }

     public function getSignOut()
    {
        // Session::flush();
        Auth::logout();
        return redirect('/');
        // return Redirect::route('/');

    }

        public function getMilestones(Request $request){
        // $user = JWTAuth::toUser($request->token);

                            $kpiInfo = KpiUser::with('kpi')
                            ->where('user_id', '=', $request->user_id)
                            ->where('kpi_id', '=', $request->kpi_id)
                            ->first();
        //Retrive kpi milestones
                            $kpiMilestones = KpiUser::where('kpi_id', '=', $request->kpi_id)
                            ->where('user_id', '=', $request->user_id)
                            ->whereNotNull('milestone_id')
                            ->whereNotNull('state')
                            ->get();

        //next review date
                            $next_review = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->where('end_date', '>', Carbon::now())
                            ->min('end_date'); 
        //target end date
                            $kpi_targetend = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->max('end_date'); 

        //Array
                            $milestoneList = array();
        //Total Milestone presantage
                            $milestoneTotWeight = Milestone::where('kpi_id', '=', $request->kpi_id)
                                                    ->sum('weight');

                            // $milestoneTotWeight = 0;
                            foreach ($kpiMilestones as $key => $value) {
            //milestone complete or not
                            $milestoneStatus = $value->state;

            //Basic infomation of milestone
                            $milestoneInfo = Milestone::where('id', '=', $value->milestone_id)
                                ->first();
                                // dd($kpiMilestones);
            // $milestoneTotWeight = $milestoneTotWeight + $milestoneInfo->weight ; //Total milestone weight
                             $milestonePercentage = round($milestoneInfo->weight*100/$milestoneTotWeight);

            //Add data to array
            $milestoneList [] = array('milestone_id' => $milestoneInfo->id, 'milestone_state' => $milestoneStatus, 'milestone_percentage' => $milestonePercentage, 'end_date' => $milestoneInfo->end_date, 'milestone_name' => $milestoneInfo->name, 'milestone_description' => $milestoneInfo->description);
        }

        $kpiOverall = array('kpi_id'=> $kpiInfo->kpi->id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'next_review' => $next_review, 'kpi_targetend' =>$kpi_targetend);

            //Response
        return response()->json(['kpiOverall' => $kpiOverall, 'milestone_data' => $milestoneList, 'totalWeight' => $milestoneTotWeight]);
    }

}
