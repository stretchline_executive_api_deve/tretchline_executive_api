<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo;
    
    //= 'user/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        // $this->middleware('guest')->except('logout');
        $this->middleware('guest', ['except' => 'logout']);

    }

       public function postLogin(Request $request){
        
        $credentials = $request->only('email', 'password');
        $credentials = array('email'=> $request->email,'password'=> $request->password);
        //$credentials['userable_type'] = $this->model;

        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->isUser()){
                return redirect('user/home');
            }
            if(Auth::user()->isAdmin()){
                return redirect('admin/home');
            }
        }
        return redirect('/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                 'email' => 'These credentials do not match our records.',
            ]);
    }
} 
