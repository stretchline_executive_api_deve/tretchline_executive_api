<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\User;
use App\Milestone;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class CronController extends Controller
{
    public function m_deadline_inform(Request $request){

    	$milestoneInfo = Milestone::with(['kpiuser' => function($query){
                                    $query->where("state", "=", "Incomplete");
                                    }, 'milestoneuser'])
    								->whereDate('end_date', '=', Carbon::tomorrow())
    								->get();
// dd($milestoneInfo[0]->milestoneuser->user_id);
    	foreach ($milestoneInfo->where('kpiuser', '!=', null) as $value) {
    		
    	// dd($value->name);

    	  // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $value->milestoneuser->user_id)
                                    ->first();
                    // if ($fcmToken != null) {
                        
                    
                    // $optionBuilder = new OptionsBuilder();
                    // $optionBuilder->setTimeToLive(60*20);

                    // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Reminder");
                    // $notificationBuilder->setBody("Please note that the deadline for 'INCOMPLETE' milestone called '".$value->name."' expires tomorrow!")
                    //                     ->setSound('default');
                                        
                    // $dataBuilder = new PayloadDataBuilder();
                    // $dataBuilder->addData(['a_data' => 'my_data']);

                    // $option = $optionBuilder->build();
                    // $notification = $notificationBuilder->build();
                    // $data = $dataBuilder->build();

                    // $tokens = $fcmToken->token;

                    // $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

                    // }

                    // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Reminder", 'body'  => "Please note that the deadline for 'INCOMPLETE' milestone called '".$value->name."' expires tomorrow!");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Reminder",
                            'body'  => "Please note that the deadline for 'INCOMPLETE' milestone called '".$value->name."' expires tomorrow!" 
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }

            // ===========================================
                    }
    }
}
