<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Kpi;
use App\KpiUser;
use App\User;
use App\Milestone;
use App\Http\Requests\KpiUserCreateRequest;
use App\Http\Requests\KpiUserUpdateRequest;

class KpiUserController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kpiUserInfo = KpiUser::with('user', 'milestone', 'kpi')->whereNotNull('milestone_id')->get();
        // dd($kpiUserInfo[0]->kpi->title);
        return view('pages.admin.kpiuser.index', compact('kpiUserInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userList = User::all();
        $milestoneList = Milestone::all();
        $kpiList = Kpi::all();

        return view('pages.admin.kpiuser.create', compact('userList', 'milestoneList', 'kpiList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KpiUserCreateRequest $request)
    {
        if (KpiUser::create($request->only('user_id', 'milestone_id', 'kpi_id', 'state'))) {
            return redirect()->route('kpiuser.create')
                ->with('success', 'KPI User created successfully');
        }else{
            return redirect()->route('kpiuser.create')
                ->with('error', 'KPI User create fail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kpiUserInfo = KpiUser::where('id', '=', $id)->first();
        $userList = User::all();
        $milestoneList = Milestone::all();
        $kpiList = Kpi::all();
        return view('pages.admin.kpiuser.edit', compact('kpiUserInfo', 'userList', 'milestoneList', 'kpiList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kpiUserUpdate = KpiUser::where('id', '=', $id)->first();
      if ($kpiUserUpdate->update($request->all())) {
            return redirect()->route('kpiuser.index')
            ->with('success', 'KPI User Updated successfully');
      }else{
            return redirect()->route('kpiuser.index')
            ->with('error', 'KPI User Update fail');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kpiUserDelete = KpiUser::where('id', '=', $id)->first();
        if($kpiUserDelete->delete()){
            return redirect()->route('kpiuser.index')
            ->with('success', 'KPI User Deleted Successfully');
        }else{
            return redirect()->route('kpiuser.index')
            ->with('error', 'KPI User Delete Unsuccessful');
        }
    }
}
