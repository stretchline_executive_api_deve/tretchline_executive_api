<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Department;
use App\Http\Requests\DepartmentCreateRequest;
use App\Http\Requests\DepartmentUpdateRequest;

class DepartmentController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departmentInfo = Department::all();
        return view('pages.admin.department.index', compact('departmentInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentCreateRequest $request)
    {
        if (Department::create($request->only('name', 'description'))) {
            return redirect()->route('department.create')
                ->with('success', 'Department create successfully');
        }else{
            return redirect()->route('department.create')
                ->with('error', 'Department create fail');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departmentInfo = Department::where('id', '=', $id)->first();
        return view('pages.admin.department.edit', compact('departmentInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentUpdateRequest $request, $id)
    {
        
      $departmentUpdate = Department::where('id', '=', $id)->first();
      if ($departmentUpdate->update($request->all())) {
            return redirect()->route('department.index')
            ->with('success', 'Department Update successfully');
      }else{
            return redirect()->route('department.index')
            ->with('error', 'Department Update fail');
      }
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departmentDelete = Department::where('id', '=', $id)->first();
        if($departmentDelete->delete()){
            return redirect()->route('department.index')
            ->with('success', 'Department Delete Successfully');
        }else{
            return redirect()->route('department.index')
            ->with('error', 'Department Delete Unsuccessful');
        }
        
    }
}
