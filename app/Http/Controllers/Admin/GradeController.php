<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Grade;
use App\Http\Requests\GradeCreateRequest;
use App\Http\Requests\GradeUpdateRequest;

class GradeController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gradeInfo = Grade::all();
        return view('pages.admin.grade.index', compact('gradeInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.grade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeCreateRequest $request)
    {
        if (Grade::create($request->only('name', 'description'))) {
            return redirect()->route('grade.create')
                ->with('success', 'Grade create successfully');
        }else{
            return redirect()->route('grade.create')
                ->with('error', 'Grade create fail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gradeInfo = Grade::where('id', '=', $id)->first();
        return view('pages.admin.grade.edit', compact('gradeInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GradeUpdateRequest $request, $id)
    {
        $gradeUpdate = Grade::where('id', '=', $id)->first();
      if ($gradeUpdate->update($request->all())) {
            return redirect()->route('grade.index')
            ->with('success', 'Grade Update successfully');
      }else{
            return redirect()->route('grade.index')
            ->with('error', 'Grade Update fail');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gradeDelete = Grade::where('id', '=', $id)->first();
        if($gradeDelete->delete()){
            return redirect()->route('grade.index')
            ->with('success', 'Grade Delete Successfully');
        }else{
            return redirect()->route('grade.index')
            ->with('error', 'Grade Delete Unsuccessful');
        }
    }
}
