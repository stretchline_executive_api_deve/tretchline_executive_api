<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Mail\UserRegistered;

use App\User;
use App\Department;
use App\Grade;
use App\Designation;
use App\UserLink;
use Illuminate\Mail\Mailable;

use Mail;

class UsersController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $usersInfo = User::with('designation', 'grade', 'department')->get();
        $userOnly = User::with('designation', 'grade', 'department')->where('is_admin', '=', '0')->get();
        return view('pages.admin.users.index', compact('usersInfo', 'userOnly'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departmentList = Department::all();
        $designationList = Designation::all();
        $gradeList = Grade::all();

        return view('pages.admin.users.create', compact('departmentList', 'designationList', 'gradeList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        // var_dump($request->all());
        if ($user = User::create($request->all())) {
            if (UserLink::create(['user_id' => $user->id])) {
                $username  = $request->name;
                $userpw = $request->password;
                Mail::to($request->email)->send(new UserRegistered($username, $userpw));
                if (Mail::failures()) {
                        return redirect()->route('users.create')
                        ->with('error', 'Email Error: Your email was unable to send!. User created');
                }else{
                    return redirect()->route('users.create')
                        ->with('success', 'User created successfully');
                }
                
                
            }else{
                return redirect()->route('users.create')
                ->with('error', 'User link create fail');
            }
        }else{
            return redirect()->route('users.create')
                ->with('error', 'User create fail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departmentList = Department::all();
        $designationList = Designation::all();
        $gradeList = Grade::all();
        $userInfo = User::where('id', '=', $id)->first();

        return view('pages.admin.users.edit', compact('departmentList', 'designationList', 'gradeList', 'userInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
         $userUpdate = User::where('id', '=', $id)->first();
         if ($userUpdate->update($request->all())) {
                return redirect()->route('users.index')
                ->with('success', 'User updated successfully');
        }else{
            return redirect()->route('users.index')
                ->with('error', 'User update fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userDelete = User::where('id', '=', $id)->first();
        if($userDelete->delete()){
            return redirect()->route('users.index')
            ->with('success', 'User Deleted Successfully');
        }else{
            return redirect()->route('users.index')
            ->with('error', 'User Delete Unsuccessful');
        }
    }
}
