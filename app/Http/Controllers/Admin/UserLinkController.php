<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserLinkEditRequest;
use App\Http\Requests\UserLinkUpdateRequest;

use App\User;
use App\UserLink;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class UserLinkController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserLinkEditRequest $request, $id)
    {
         $userLinkInfo = UserLink::with('user', 'superuser')
                            ->where('user_id', '=', $request->user_id)->first();
        if ($userLinkInfo->subuser_id != null) {
            $subUserArray = unserialize($userLinkInfo->subuser_id);
        }else{
            $subUserArray = array();
        }
         
         $userList = User::all();
         // $superUserCollection = UserLink::whereNotNull('superuser_id')->get(['user_id']);
         // $superUserList = array_pluck($superUserCollection, 'user_id');
         // $validUserList = array_diff($userList->pluck('id')->toArray(), $superUserList);
         // =======new===
            $noSuperusersCollection = UserLink::with(['user' => function($query){
                                    $query->where("is_admin", "=", '0');
                                    }])->whereNull('superuser_id')->get(['user_id']);
            $noSuperusers = array_pluck($noSuperusersCollection->where('user', '!=', null), 'user_id');
            $validUserList = array_merge($noSuperusers, $subUserArray);
         // ==================                                                                                                    
         // dd($validUserList);
         return view('pages.admin.userlink.edit', compact('userLinkInfo', 'userList', 'subUserArray', 'validUserList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserLinkUpdateRequest $request, $id)
    {
        $updateUserLink = UserLink::where('id', '=', $id)->first();
        // dd($request->subuser_id);
        //removed sub users data update, 'super user id -> null'
        if($request->subuser_id != null && unserialize($updateUserLink->subuser_id) != null){
        $oldRemovedList = array_diff(unserialize($updateUserLink->subuser_id), $request->subuser_id);
        // dd($oldRemovedList);
        foreach ($oldRemovedList as $value) {
                UserLink::where('user_id', '=', $value)->update(['superuser_id' => null]);
        }
        }else{
                UserLink::where('superuser_id', '=', $updateUserLink->user_id)->update(['superuser_id' => null]);
        }
        // add new super user data to sub users
        if($request->subuser_id != null){
        foreach ($request->subuser_id as $value) {
                UserLink::where('user_id', '=', $value)->update(['superuser_id' => $updateUserLink->user_id]);
        }
        }
        if($request->subuser_id != null){
        $subuserString = serialize($request->subuser_id);
        }else{
        $subuserString = null; 
        }
        $superUserInfo = User::where('id', '=', $updateUserLink->user_id)->first();

        // dd($superUserInfo);
        if ($updateUserLink->update(['subuser_id' => $subuserString])) {
            // =========new
            
           
            // ========endnew


            // ===========FCM=====================
            // foreach ($request->subuser_id as $value) {
                // dd($value);
                if($request->subuser_id != null){


                $pushyToken = FcmData::whereIn('user_id', $request->subuser_id)
                                    ->pluck('token')->toArray();
                // dd($fcmToken);
                    // if ($fcmToken != null) {
                        
                    
                    // $optionBuilder = new OptionsBuilder();
                    // $optionBuilder->setTimeToLive(60*20);

                    // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Superuser Info");
                    // $notificationBuilder->setBody("You have new Superuser named, '".$superUserInfo->name."'." )
                    //                     ->setSound('default');
                                        
                    // $dataBuilder = new PayloadDataBuilder();
                    // $dataBuilder->addData(['a_data' => 'my_data']);

                    // $option = $optionBuilder->build();
                    // $notification = $notificationBuilder->build();
                    // $data = $dataBuilder->build();

                    // $tokens = $fcmToken;

                    // $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
                    //     }

                        // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Superuser Info", 'body'  => "You have new Superuser named, '".$superUserInfo->name."'." );

                    // The recipient device tokens
                    $to = $pushyToken;

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Superuser Info",
                            'body'  => "You have new Superuser named, '".$superUserInfo->name."'." 
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
                
                    }
                // }
            // ===========================================
            return redirect()->route('users.index')
                ->with('success', 'User Assign updated successfully');
        }else{
           return redirect()->route('users.index')
                ->with('error', 'User Assign update fail'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
