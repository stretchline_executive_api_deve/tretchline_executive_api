<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Kpi;
use App\User;
use App\KpiUser;
use App\FcmData;
use App\Milestone;

use App\Http\Requests\KpiCreateRequest;
use App\Http\Requests\KpiUpdateRequest;


use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;


class KpiController extends Controller
{
        /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kpiInfo = Kpi::all();
        //assign array
         $byKpi = array();

        foreach ($kpiInfo as $value) {
        $kpiuserInfo = KpiUser::where('kpi_id', '=', $value->id)->first();
        if ($kpiuserInfo != null) {

        $kpiMilestones = KpiUser::where('kpi_id', '=', $value->id)->whereNotNull('milestone_id')->get();
        $totalMilestones = $kpiMilestones->count(); //Total number of milestones for single kpi
        $completedMilestones = $kpiMilestones->where('state', '=', 'Complete')
                                    ->count(); //Number of completed milestones for single kpi
        if ($totalMilestones == $completedMilestones) {
            $kpiState = 100;
        }

           //loop each milestone for single kpi
        $completedWeight = 0;
        $totalWeight = 0;
        //loop each milestone
            foreach ($kpiMilestones as $valuee) {
                $milestoneInfo = Milestone::where('id', '=', $valuee->milestone_id)
                ->first();
                if ($valuee->state == 'Complete') {
                    $completedWeight = $completedWeight + $milestoneInfo->weight;
                }
                if ($milestoneInfo != null) {
                    // dd((int)$milestoneInfo->weight);
                    $totalWeight = $totalWeight + (int)$milestoneInfo->weight;
                }
                
            }
            //get user info
            $kpiuserDetails = User::where('id', $kpiuserInfo->user_id)->first();
            if($kpiuserDetails != null){
                $kpi_user = $kpiuserDetails->name;
            }else{
                $kpi_user = '--';
            }

            // dd($value->user_id);
            if ($completedWeight == 0 && $totalWeight == 0) {
                $kpiState = 0;
            }else {
                $kpiState = ($completedWeight / $totalWeight) *100; 
            }
            // dd($value->kpiuser[0]->user->name);
            $byKpi[] = (object) array('id' => $value->id, 'title' => $value->title, 'description' => $value->description, 'created_at' => $value->created_at, 'kpiState' => $kpiState, 'kpi_user' => $kpi_user);
        }
        }
        return view('pages.admin.kpi.index', compact('byKpi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $userList = User::all();
        return view('pages.admin.kpi.create', compact('userList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KpiCreateRequest $request)
    {
        $pushyToken = FcmData::where('user_id', '=', $request->user_id)->first();
        if ($kpiCreate = Kpi::create($request->only('title', 'description'))) {
            if (KpiUser::create(['kpi_id' => $kpiCreate->id, 'user_id' => $request->user_id])) {
                // ===========FCM=====================
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | KPI's Info");
                // $notificationBuilder->setBody("You have new KPI., '".$request->title."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }
                 // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | KPI's Info", 'body'  => "You have new KPI., '".$request->title."'.");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | KPI's Info",
                            'body'  => "You have new KPI., '".$request->title."'."
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================


                if (isset($request->add_milestone)) {
                    // dd($kpiCreate->title);
                    return redirect()->route('milestone.create')
                    ->with('success', 'Kpi created successfully')
                    ->with('kpi_id', $kpiCreate->id)
                    ->with('kpi_name', $kpiCreate->title)
                    ->with('user_id', $request->user_id);
                // }elseif(isset($request->copy_kpi)){
                //     //get all privious milestone ids
                //     $milestoneList = KpiUser::where('kpi_id', '=', $request->previous_kpi_id)
                //                             ->whereNotNull('milestone_id')
                //                             ->orderBy('id', 'DESC')
                //                             ->get();
                //     $milestoneIds = $milestoneList->pluck(['milestone_id']);
                //     // dd($milestoneIds); 
                //     $milestoneData = Milestone::whereIn('id', $milestoneIds)->get();
                //     $nameArray = $milestoneData->pluck('name')->toArray();
                //     $descriptionArray = $milestoneData->pluck('description')->toArray();
                //     $weightArray = $milestoneData->pluck('weight')->toArray();
                //     $start_dateArray = $milestoneData->pluck('start_date')->toArray();
                //     $end_date = $milestoneData->pluck('end_date')->toArray();
                //     $milesCopyData = array('kpi_id' => $kpiCreate->id, 'user_id' => $request->user_id, 'name' => $nameArray, 'description' => $descriptionArray, 'weight' => $weightArray, 'start_date' => $start_dateArray, 'end_date' => $end_date);

                //     return redirect('admin/copy_milestone')
                //     ->with('milesCopyData', $milesCopyData)
                //     ->with('success', 'Kpi created successfully');
                }else {
                    return redirect()->route('kpi.create')
                ->with('success', 'Kpi created successfully');
                }
                
            }else{

                return redirect()->route('kpi.create')
                ->with('error', 'KpiUser create fail');
            }

            
        }else{
            return redirect()->route('kpi.create')
                ->with('error', 'Kpi create fail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $kpiInfo = Kpi::where('id', '=', $id)->first();
        $kpiUser = KpiUser::where('kpi_id', '=', $id)->first();
        $milestoneByKpi = KpiUser::with('milestone')
                                  ->whereNotNull('milestone_id')
                                  ->where('kpi_id', '=', $id)
                                  ->get();
        $userList = User::all();
        if (isset($request->copy)) {
          // dd($milestoneByKpi);
            return view('pages.admin.kpi.copy', compact('kpiInfo', 'userList', 'kpiUser', 'milestoneByKpi'));
        }else{
            return view('pages.admin.kpi.edit', compact('kpiInfo', 'userList', 'kpiUser'));
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KpiUpdateRequest $request, $id)
    {
        $kpiUpdate = Kpi::where('id', '=', $id)->first();
      if ($kpiUpdate->update($request->only('title', 'description')) && KpiUser::where('kpi_id', '=', $id)->update(['user_id' => $request->user_id])) {
            return redirect()->route('kpi.index')
            ->with('success', 'Kpi Update successfully');
      }else{
            return redirect()->route('kpi.index')
            ->with('error', 'Kpi Update fail');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $kpiDelete = Kpi::where('id', '=', $id)->first();
        // $kpiUserDelete = KpiUser::where('kpi_id', '=', $id)->get();
        if(Kpi::where('id', '=', $id)->delete()){
            return redirect()->route('kpi.index')
            ->with('success', 'Kpi Delete Successfully');
        }else{
            return redirect()->route('kpi.index')
            ->with('error', 'Kpi Delete Unsuccessful');
        }
    }



}
