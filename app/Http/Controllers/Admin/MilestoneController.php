<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\MilestoneCreateRequest;
use App\Http\Requests\MilestoneUpdateRequest;
use App\Http\Requests\KpiMilestoneCopyRequest;

use App\Milestone;
use App\Kpi;
use App\KpiUser;
use App\User;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class MilestoneController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $milestoneInfo = Milestone::with('kpi', 'kpiuser', 'milestoneuser')->get();
        // dd($milestoneInfo[0]->milestoneuser->user->name);
        // dd($milestoneInfo[0]->milestoneuser->state);
        return view('pages.admin.milestone.index', compact('milestoneInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kpiList = Kpi::all();
        $userList = User::all();
        return view('pages.admin.milestone.create', compact('kpiList', 'userList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MilestoneCreateRequest $request)
    {
        // dd($request->all());
        // var_dump($request->all());
        $numOfMilestones = sizeof($request->name);
        // var_dump($numOfMilestones);
        // get user id from kpi user table
        $user_id = KpiUser::with('kpi')
        ->where('kpi_id', '=', $request->kpi_id)
        ->first();
        $i=0;
        for ($i=0; $i < $numOfMilestones; $i++) {
            //milestone data array
            $milestoneArray = array('kpi_id' => $request->kpi_id, 'name' => $request->name[$i], 'description' => $request->description[$i], 'weight' => $request->weight[$i], 'start_date' => $request->start_date[$i], 'end_date' => $request->end_date[$i]);
            //Insert milestone
            if ($milestoneCreate = Milestone::create($milestoneArray)) {

                // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $user_id->user_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Milestone Info");
                // $notificationBuilder->setBody("You have new Milestone, '".$request->name[$i]."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }
                 // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Milestone Info", 'body'  => "You have new Milestone, '".$request->name[$i]."'.");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Milestone Info",
                            'body'  => "You have new Milestone, '".$request->name[$i]."'." 
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================



                //kpi user data array
                $kpiuserArray = array('user_id' => $user_id->user_id, 'milestone_id' => $milestoneCreate->id, 'kpi_id' => $request->kpi_id, 'state' => 'Incomplete');
                //Insert kpi user by milestone
                if (KpiUser::create($kpiuserArray)) {

                    //Delete null record on kpi user from final loop
                    if ($i == $numOfMilestones - 1) {
                       $kpiUserDelete = KpiUser::with('kpi')
                       ->where('kpi_id', '=', $request->kpi_id)
                       ->where('milestone_id', '=', null)
                       ->delete();

                       return redirect()->route('milestone.create')
                       ->with('success', 'Milestone created successfully');
                   }

               }else{
                return redirect()->route('milestone.create')
                ->with('error', 'Kpi User create fail');
                }



            }else{
            return redirect()->route('milestone.create')
            ->with('error', 'Milestone create fail');
            }

        }
        // $insertData = Milestone::create()
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $milestoneInfo = Milestone::with('kpi', 'kpiuser', 'milestoneuser')
                        ->where('id', '=', $id)
                        ->first();
        $kpiList = Kpi::all();
        return view('pages.admin.milestone.edit', compact('milestoneInfo', 'kpiList'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MilestoneUpdateRequest $request, $id)
    {
        $updateMilestone = Milestone::where('id', '=', $id)->first();
        $updateKpiUser = KpiUser::where('milestone_id', '=', $id)->first();

        if ($updateMilestone->update($request->only('name', 'description', 'weight', 'start_date', 'end_date'))) 
        {
            return redirect()->route('milestone.index')
            ->with('success', 'Milestone updated successfully');
        }else{
            return redirect()->route('milestone.index')
            ->with('error', 'Milestone update fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteMilestone = Milestone::where('id', '=', $id)->first();
        if ($deleteMilestone->delete() ) {
            return redirect()->route('milestone.index')
            ->with('success', 'Milestone delete successfully');
        }else{
            return redirect()->route('milestone.index')
            ->with('error', 'Milestone delete fail');
        }
    }


    // Kpilist by user
        public function kpibyuser($id){
        $userKpis = KpiUser::with('kpi')
                                ->where('user_id', '=', $id)
                                ->get();
         $userKpiList =  $userKpis->unique('kpi_id')->values();               
         // dd($userKpiList[0]->kpi->title);
         $kpiInfo = array();
         foreach ($userKpiList as $value) {
             
             $kpiInfo [] = array($value->kpi_id => $value->kpi->title);
         }

         // dd($kpiInfo);
         return json_encode($kpiInfo);
    }


     ///New method for copy milestone
    public function copy_kpi_milestone(KpiMilestoneCopyRequest $request){
        // dd($request->all());
        // first copy kpi
        $pushyToken = FcmData::where('user_id', '=', $request->user_id)->first();
        if ($kpiCreate = Kpi::create(['title' => $request->title, 'description' => $request->kpidescription])) {
            if (KpiUser::create(['kpi_id' => $kpiCreate->id, 'user_id' => $request->user_id])) {
                // ===========FCM=====================
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | KPI's Info");
                // $notificationBuilder->setBody("You have new KPI., '".$request->title."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }
                 // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | KPI's Info", 'body'  => "You have new KPI., '".$request->title."'.");

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | KPI's Info",
                            'body'  => "You have new KPI., '".$request->title."'."  
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            }
        }

        // secondly copy all milestones
        $numOfMilestones = sizeof($request->name);
        $i=0;
        for ($i=0; $i < $numOfMilestones; $i++) {
            //milestone data array
            $milestoneArray = array('kpi_id' => $kpiCreate->id, 'name' => $request->name[$i], 'description' => $request->description[$i], 'weight' => $request->weight[$i], 'start_date' => $request->start_date[$i], 'end_date' => $request->end_date[$i]);
            //Insert milestone
            if ($milestoneCreate = Milestone::create($milestoneArray)) {

                // ===========FCM=====================
                $pushyToken = FcmData::where('user_id', '=', $request->user_id)->first();
                // if ($fcmToken != null) {
                    
                
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Milestone Info");
                // $notificationBuilder->setBody("You have new Milestone, '".$request->name[$i]."'." )
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = $fcmToken->token;

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                // }
                // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Milestone Info", 'body'  => "You have new Milestone, '".$request->name[$i]."'." );

                    // The recipient device tokens
                    $to = array($pushyToken->token);

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Milestone Info",
                            'body'  => "You have new Milestone, '".$request->name[$i]."'." 
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================



                //kpi user data array
                $kpiuserArray = array('user_id' => $request->user_id, 'milestone_id' => $milestoneCreate->id, 'kpi_id' => $kpiCreate->id, 'state' => 'Incomplete');
                //Insert kpi user by milestone
                if (KpiUser::create($kpiuserArray)) {

                    //Delete null record on kpi user from final loop
                    if ($i == $numOfMilestones - 1) {
                       $kpiUserDelete = KpiUser::with('kpi')
                       ->where('kpi_id', '=', $kpiCreate->id)
                       ->where('milestone_id', '=', null)
                       ->delete();

                       return redirect()->route('kpi.index')
                       ->with('success', 'KPI & Milestones copied successfully');
                   }

               }else{
                return redirect()->route('milestone.create')
                ->with('error', 'Kpi User create fail');
                }



            }else{
            return redirect()->route('milestone.create')
            ->with('error', 'Milestone create fail');
            }

        }
    }


}
