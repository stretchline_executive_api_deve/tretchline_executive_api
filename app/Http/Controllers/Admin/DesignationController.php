<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Designation;
use App\Http\Requests\DesignationCreateRequest;
use App\Http\Requests\DesignationUpdateRequest;

class DesignationController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designationInfo = Designation::all();
        return view('pages.admin.designation.index', compact('designationInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.designation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DesignationCreateRequest $request)
    {
        if (Designation::create($request->only('name', 'description'))) {
            return redirect()->route('designation.create')
                ->with('success', 'Designation create successfully');
        }else{
            return redirect()->route('designation.create')
                ->with('error', 'Designation create fail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $designationInfo = Designation::where('id', '=', $id)->first();
        return view('pages.admin.designation.edit', compact('designationInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DesignationUpdateRequest $request, $id)
    {
        $designationUpdate = Designation::where('id', '=', $id)->first();
      if ($designationUpdate->update($request->all())) {
            return redirect()->route('designation.index')
            ->with('success', 'Designation Update successfully');
      }else{
            return redirect()->route('designation.index')
            ->with('error', 'Designation Update fail');
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $designationDelete = Designation::where('id', '=', $id)->first();
        if($designationDelete->delete()){
            return redirect()->route('designation.index')
            ->with('success', 'Designation Delete Successfully');
        }else{
            return redirect()->route('designation.index')
            ->with('error', 'Designation Delete Unsuccessful');
        }
    }
}
