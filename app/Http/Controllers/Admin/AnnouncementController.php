<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\AnnounceCreateRequest;
use App\Http\Requests\AnnounceUpdateRequest;

use App\Announcement;
use App\AnnounceUser;
use App\User;
use App\Jobs\SendAnnounceEmail;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class AnnouncementController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announceInfo = Announcement::all();
        return view('pages.admin.announce.index', compact('announceInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userList = User::all();
        return view('pages.admin.announce.create', compact('userList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnounceCreateRequest $request)
    {
        if ($createAnnounce = Announcement::create($request->only('title', 'description', 'user_type', 'start_date', 'end_date'))) {
            //Private announcements
            if ($request->user_type == '1') {
                // ===========FCM=====================
                $pushyToken = FcmData::whereIn('user_id', $request->user_id)
                                    ->pluck('token')->toArray();
                    // if ($fcmToken != null) {
                        
                    
                    // $optionBuilder = new OptionsBuilder();
                    // $optionBuilder->setTimeToLive(60*20);

                    // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Announcements");
                    // $notificationBuilder->setBody("Title: ".$request->title."\nDescription: ".$request->description.".")
                    //                     ->setSound('default');
                                        
                    // $dataBuilder = new PayloadDataBuilder();
                    // $dataBuilder->addData(['a_data' => 'my_data']);

                    // $option = $optionBuilder->build();
                    // $notification = $notificationBuilder->build();
                    // $data = $dataBuilder->build();

                    // $tokens = $fcmToken;

                    // $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

                    // }
            // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => 'Stretchline | Announcements', 'body'  => "Title: ".$request->title."\nDescription: ".$request->description.".");

                    // The recipient device tokens
                    $to = $pushyToken;

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => 'Stretchline | Announcements',
                            'body'  => "Title: ".$request->title."\nDescription: ".$request->description."."
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }
            // ===========================================
            // ========create private job queue
                    $this->dispatch(new SendAnnounceEmail($request->title, $request->description, serialize($request->user_id), $request->user_type));

               // ========end private job queue======
                // var_dump($request->user_id);
                foreach ($request->user_id as $value) {
                    $createUserby = AnnounceUser::create(['announce_id' => $createAnnounce->id, 'user_id'=>$value]);

                }
                
                    return redirect()->route('announce.create')
                                    ->with('success', 'Private announce created successfully');
                }else{
                //public announcements
                // ===========FCM=====================
                $pushyToken = FcmData::pluck('token')->toArray();
                    // if ($fcmToken != null) {
                        
                    
                    // $optionBuilder = new OptionsBuilder();
                    // $optionBuilder->setTimeToLive(60*20);

                    // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Announcements");
                    // $notificationBuilder->setBody("Title: ".$request->title."\nDescription: ".$request->description.".")
                    //                     ->setSound('default');
                                        
                    // $dataBuilder = new PayloadDataBuilder();
                    // $dataBuilder->addData(['a_data' => 'my_data']);

                    // $option = $optionBuilder->build();
                    // $notification = $notificationBuilder->build();
                    // $data = $dataBuilder->build();

                    // $tokens = $fcmToken;

                    // $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

                    // }

                    // ===============Pushy=========================
                    if ($pushyToken != null) {
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | Announcements", 'body'  => "Title: ".$request->title."\nDescription: ".$request->description.".");

                    // The recipient device tokens
                    $to = $pushyToken;

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'title' => "Stretchline | Announcements",
                            'body'  => "Title: ".$request->title."\nDescription: ".$request->description."."
                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);
                }

            // ===========================================
            // 
            // ========create public job queue
                    $this->dispatch(new SendAnnounceEmail($request->title, $request->description, serialize($request->user_id), $request->user_type));

               // ========end public job queue======
                return redirect()->route('announce.create')
                ->with('success', 'Public announce created successfully');
            }
   
        }else{
            return redirect()->route('announce.create')
                ->with('error', 'Announcement create fail');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announceInfo = Announcement::where('id', '=', $id)->first();
        $announceUserInfo = AnnounceUser::where('announce_id', '=', $id)->pluck('user_id')->toArray();
        $userList = User::all();
        return view('pages.admin.announce.edit', compact('announceInfo', 'announceUserInfo', 'userList'));
        // var_dump($announceUserInfo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $updateAnnounce = Announcement::where('id', '=', $id)->first();
        if ($updateAnnounce->update($request->only('title', 'description', 'user_type', 'start_date', 'end_date'))) {
            if ($request->user_type == '1') {

                foreach ($request->user_id as $value) {
                    $updateUserby = AnnounceUser::where('announce_id', '=', $id)
                                    ->where('user_id', '=', $value)
                                    ->first();
                    $updateUserby->update(['announce_id' => $id, 'user_id'=>$value]);
                }
                
                    return redirect()->route('announce.index')
                                    ->with('success', 'Private announce updated successfully');
            }else{
                return redirect()->route('announce.index')
                ->with('success', 'Public announce updated successfully');
            }
   
        }else{
            return redirect()->route('announce.index')
                ->with('error', 'Announcement update fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $announceDelete = Announcement::where('id', '=', $id)->first();
        if(Announcement::where('id', '=', $id)->delete()){
            return redirect()->route('announce.index')
            ->with('success', 'Announcement Deleted Successfully');
        }else{
            return redirect()->route('announce.index')
            ->with('error', 'Announcement Delete Unsuccessful');
        }

    }
}
