<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class SuperAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // This gets null cuz it doesn't know which user id to check...
        // Probably I can do... if(Auth::user()->role_id != 2(which is an owner id)) ..... but this code seems to smell bad somehow... :(
        $user = $request->user();
        if(!$user){
            return redirect('/login');
        }

        if (Auth::user()->isAdmin()) {
            return $next($request);
        }

        return redirect('/');
    }
}