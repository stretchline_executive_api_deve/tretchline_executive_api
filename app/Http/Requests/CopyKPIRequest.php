<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CopyKPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|max:255',
            'title' => 'required|max:255',
            'kpidescription' => 'required|max:255',
            'milestone_id.*' => 'required',
            'name.*' => 'required|max:255',
            'description.*' => 'required|max:255',
            'start_date.*' => 'required',
            'end_date.*' => 'required',
            'weight.*' => 'required',
            'state.*' => 'required'
        ];
    }
}
