<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubuserMilestoneCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subuser_id' => 'required',
            'name.*' => 'required',
            'description.*' => 'required',
            'weight.*' => 'required',
            'start_date.*' => 'required',
            'end_date.*' => 'required',
            'kpi_id' => 'required'
        ];
    }
}
