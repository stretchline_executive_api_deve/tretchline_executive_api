<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'profile_image' => 'required',
            'is_admin' => 'required',
            'phone' => 'required',
            'grade_id' => 'required',
            'epf_no' => 'required|unique:users',
            'designation_id' => 'required',
            'department_id' => 'required',
            'email' => 'required|email|unique:users',
            'gmail' => ['required', 'max:255', 'email', 'regex:/(.*)@(outlook|hotmail|live|windowslive|msn|microsoft)(.*)/i', 'unique:users'],
            'password' => 'required|min:6'
        ];
    }
}
