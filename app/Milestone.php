<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    protected $table = 'milestones';
    protected $fillable = array('kpi_id', 'name', 'description', 'weight', 'start_date', 'end_date');

    public function kpi(){
    	return $this->belongsTo('App\Kpi', 'kpi_id', 'id');
    }
    public function kpiuser(){
    	return $this->hasMany('App\KpiUser', 'kpi_id', 'kpi_id');
    }
    public function milestoneuser(){
    	return $this->hasOne('App\KpiUser', 'milestone_id', 'id');
    }
}
