<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnounceUser extends Model
{
   protected $table = 'announce_users';
   protected $fillable = array('announce_id', 'user_id');

    public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function announcement(){
    	return $this->belongsTo('App\Announcement', 'announce_id', 'id');
    }
}
