<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KpiUser extends Model
{
    protected $table = 'kpi_users';
    protected $fillable = array('user_id', 'milestone_id', 'kpi_id', 'state');

    public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function milestone(){
    	return $this->belongsTo('App\Milestone', 'milestone_id', 'id');
    }
    public function kpi(){
    	return $this->belongsTo('App\Kpi', 'kpi_id', 'id');
    }
    public function miles(){
        return $this->hasMany('App\Milestone', 'kpi_id', 'kpi_id');
    }
}
