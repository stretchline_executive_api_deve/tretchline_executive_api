<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FcmData extends Model
{
    protected $table = 'fcm_datas';
    protected $fillable = array('user_id', 'token');

    public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
