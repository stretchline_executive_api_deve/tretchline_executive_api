<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestState extends Model
{
    protected $table = 'request_states';
    protected $fillable = array('request_id', 'receiver_id', 'state');


       public function requestMeeting(){
       	
    	return $this->belongsTo('App\RequestMeeting', 'request_id', 'id');
    }
}
