<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kpi extends Model
{
    protected $table = 'kpis';
    protected $fillable = array('title', 'description');

    public function milestone(){
    	return $this->hasMany('App\Milestone', 'kpi_id', 'id');
    }
    public function kpiuser(){
    	return $this->hasMany('App\KpiUser', 'kpi_id', 'id');
    }
}
