<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grades';
    protected $fillable = array('name', 'description');

    public function grade(){
        return $this->hasMany('App\Grade', 'grade_id', 'id');
    }

}
