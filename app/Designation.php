<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table = 'designations';
    protected $fillable = array('name', 'description');

    public function designation(){
        return $this->hasMany('App\User', 'designation_id', 'id');
    }

}
