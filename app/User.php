<?php

namespace App;

use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use File;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'designation_id',
        'department_id',
        'grade_id',
        'phone',
        'epf_no',
        'gmail',
        'profile_image',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string  $value
     * @return void
     */


    public function setProfileImageAttribute($photo) {

        if (!is_string($photo)) {

            $photoDir = "profile_img/";

            if (!is_null($photo) && $photo->isValid()) {

                $extension = $photo->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;
                $photo->move($photoDir, $fileName);

                if (isset($this->attributes['profile_image']) && $this->attributes['profile_image'] != null) {
                    File::Delete($this->attributes['profile_image']);
                }

                $this->attributes['profile_image'] = $photoDir . $fileName;
            }

        } else {

            // $this->attributes['photo'] = $photo;
            $this->attributes['profile_image'] = 'user_default.jpg';
        }

    }

    public function getProfileImageAttribute() {
        return $this->attributes['profile_image'];
    }



    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function isAdmin()
    {
        return ($this->is_admin == 1);

    }

     public function isUser()
    {
        return ($this->is_admin == 0);

    }

    //relations
    //
    public function kpiuser(){
        return $this->hasMany('App\KpiUser', 'user_id', 'id');
    }
    public function announceuser(){
        return $this->hasMany('App\AnnounceUser', 'user_id', 'id');
    }
    public function designation(){
        return $this->belongsTo('App\Designation', 'designation_id', 'id');
    }
    public function grade(){
        return $this->belongsTo('App\Grade', 'grade_id', 'id');
    }
    public function department(){
        return $this->belongsTo('App\Department', 'department_id', 'id');
    }
}
