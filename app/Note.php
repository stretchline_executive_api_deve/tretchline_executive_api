<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'notes';
    protected $fillable = array('user_id', 'kpi_id', 'milestone_id', 'title', 'note', 'next_review');

    public function kpi(){
    	return $this->hasOne('App\Kpi', 'kpi_id', 'id');
    }
    public function user(){
    	return $this->hasOne('App\User', 'user_id', 'id');
    }
    public function milestone(){
    	return $this->hasOne('App\Milestone', 'milestone_id', 'id');
    }
}
