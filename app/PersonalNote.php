<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalNote extends Model
{
    protected $table = 'personal_notes';
    protected $fillable = array('user_id', 'title', 'description');
}
