<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLink extends Model
{
    protected $table = 'user_links';
    protected $fillable = array('user_id', 'superuser_id', 'subuser_id');



    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function superuser(){
        return $this->belongsTo('App\User', 'superuser_id', 'id');
    }

}