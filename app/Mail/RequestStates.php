<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestStates extends Mailable
{
    use Queueable, SerializesModels;
    protected $senderName, $milestoneID, $reqTitle, $reqMessage, $meetingDate, $receiverName, $receiverState, $senderID;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($senderName, $milestoneID, $reqTitle, $reqMessage, $meetingDate, $receiverName, $receiverState, $senderID)
    {
        $this->senderName = $senderName;
        $this->milestoneID = $milestoneID;
        $this->reqTitle = $reqTitle;
        $this->reqMessage = $reqMessage;
        $this->meetingDate = $meetingDate;
        $this->receiverName = $receiverName;
        $this->receiverState = $receiverState;
        $this->senderID = $senderID;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Stretchline | Meeting Status')->markdown('emails.meeting.status')->with(['senderName' => $this->senderName, 'milestoneID' => $this->milestoneID, 'reqTitle'=> $this->reqTitle, 'reqMessage' => $this->reqMessage, 'meetingDate' => $this->meetingDate, 'receiverName' => $this->receiverName, 'receiverState' => $this->receiverState, 'baseUrl'=> url('/user/meeting-request/show/'.$this->senderID)]);;
    }
}
