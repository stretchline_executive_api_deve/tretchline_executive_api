<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAnnouncement extends Mailable
{
    use Queueable, SerializesModels;
    protected $title, $description, $name, $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $description, $name, $email)
    {
        $this->title = $title;
        $this->description = $description;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Stretchline | Announcements')->markdown('emails.announce.send_announce')->with(['title' => $this->title, 'description' => $this->description, 'name' => $this->name, 'email' => $this->email]);
    }
}
