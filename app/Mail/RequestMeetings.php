<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestMeetings extends Mailable
{
    use Queueable, SerializesModels;
    protected $senderName, $milestoneID, $reqTitle, $reqMessage, $meetingDate, $receiverName, $receiverEmail, $receiverID;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($senderName, $milestoneID, $reqTitle, $reqMessage, $meetingDate, $receiverName, $receiverEmail, $receiverID)
    {
        $this->senderName = $senderName;
        $this->milestoneID = $milestoneID;
        $this->reqTitle = $reqTitle;
        $this->reqMessage = $reqMessage;
        $this->meetingDate = $meetingDate;
        $this->receiverName = $receiverName;
        $this->receiverEmail = $receiverEmail;
        $this->receiverID = $receiverID;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Stretchline | Request Meeting')->markdown('emails.meeting.request')->with(['senderName' => $this->senderName, 'milestoneID' => $this->milestoneID, 'reqTitle'=> $this->reqTitle, 'reqMessage' => $this->reqMessage, 'meetingDate' => $this->meetingDate, 'receiverName' => $this->receiverName, 'receiverEmail' => $this->receiverEmail, 'baseUrl'=> url('/user/meeting-request/show/'.$this->receiverID)]);
    }
}
