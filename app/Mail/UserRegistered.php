<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;
    protected $username, $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $userpw)
    {
        $this->username = $username;
        $this->password = $userpw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Stretchline | User Registration')->markdown('emails.user.registered')->with(['username' => $this->username, 'password' => $this->password, 'baseUrl'=> url('/login')]);
    }
}
