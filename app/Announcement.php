<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
   protected $table = 'announcements';
   protected $fillable = array('title', 'description', 'user_type', 'start_date', 'end_date');


   public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
