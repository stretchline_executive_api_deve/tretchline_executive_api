<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

use App\User;
use Illuminate\Mail\Mailable;
use App\Mail\SendAnnouncement;
use Mail;

class SendAnnounceEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $title, $description, $user_id, $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $description, $user_id, $type)
    {
        $this->title = $title;
        $this->description = $description;
        $this->user_id = $user_id;
        $this->type = $type;
    }
 
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

     // Log::info("Send Email:".$this->title." === ".$this->description." === ".$this->user_id." ====".$this->type);
     if ($this->type == 1) {
        foreach (unserialize($this->user_id) as $value) {
            $userInfo = User::where('id', '=', $value)->first();
            Log::info("userInfo: ".$userInfo);
            Mail::to($userInfo->email)->send(new SendAnnouncement($this->title, $this->description, $userInfo->name, $userInfo->email));
        }
         
     }else {
        $userArray = User::where('is_admin', '=', '0')->get();
        foreach ($userArray as $value) {
            Mail::to($value->email)->send(new SendAnnouncement($this->title, $this->description, $value->name, $value->email));
        }
         
     }
     
        
    }

        /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
    }
}
