<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class GetUserRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.get_user.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
