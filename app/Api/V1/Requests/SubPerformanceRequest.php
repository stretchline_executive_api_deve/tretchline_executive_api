<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class SubPerformanceRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.get_subperform.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
