<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class AddPNoteRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.add_pnote.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
