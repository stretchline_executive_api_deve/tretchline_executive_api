<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class DeletePNoteRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.delete_pnote.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
