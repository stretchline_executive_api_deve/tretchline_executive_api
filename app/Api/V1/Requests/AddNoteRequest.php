<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class AddNoteRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.add_note.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
