<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class MeetingRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.get_request_meeting.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
