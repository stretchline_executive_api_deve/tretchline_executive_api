<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Api\V1\Requests\TokenRequest;
use App\Api\V1\Requests\AddNoteRequest;
use App\Api\V1\Requests\AddPNoteRequest;
use App\Api\V1\Requests\DeletePNoteRequest;

use App\Note;
use App\PersonalNote;
use App\Announcement;
use App\AnnounceUser;
use Carbon\Carbon;

class NoteController extends Controller
{
    //Insert Note
    public function addNote(AddNoteRequest $request){
	$user = JWTAuth::toUser($request->token);

        array_except($request, ['token', $request->token]);
        array_add($request, 'user_id', $user->id);

        if (Note::create($request->all())) {
        	return response()->json(['success']);
        }else{
        	return response()->json(['error'], 201);
        }
        

    }

    //Add Personal Note
    public function addPnote(AddPNoteRequest $request){
    	$user = JWTAuth::toUser($request->token);

    	array_except($request, ['token', $request->token]);
        array_add($request, 'user_id', $user->id);

        //Insert data
        if ($pnote = PersonalNote::create($request->all())) {
        	return response()->json(['success', 'pnote_id' => $pnote->id]);
        }else{
        	return response()->json(['error'], 201);
        }

    }

    //Delete Personal note
    public function deletePnote(DeletePNoteRequest $request){

    	$deletepnote = PersonalNote::where('id', '=', $request->pnote_id)->first();

    	if (isset($deletepnote ) && $deletepnote->delete()) {
    		return response()->json(['success']);
    	}else{
    		return response()->json(['error'], 201);
    	}


    }

    //Personal Notes & Announcements
    public function announcePnote(TokenRequest $request){
    	$user = JWTAuth::toUser($request->token);

    	$privateAnnounce_s = AnnounceUser::with(['announcement' => function($q){
                        $q->where('start_date', '<=', Carbon::today()->format('Y-m-d'))
                            ->where('end_date', '>=', Carbon::today()->format('Y-m-d'));
                        }])
                        ->where('user_id', '=', $user->id)
                        ->orderBy('created_at', 'DESC')
    					->get();
        $privateAnnounce = $privateAnnounce_s->where('announcement', '!=', null);
        // dd($privateAnnounce->where('announcement', '!=', null));
    	$publicAnnounce = Announcement::where('user_type', '=', '0')
                        ->where('start_date', '<=', Carbon::today()->format('Y-m-d'))
                        ->where('end_date', '>=', Carbon::today()->format('Y-m-d'))
                        ->orderBy('created_at', 'DESC')
    					->get();
        // dd($privateAnnounce);
    	$pnoteList = PersonalNote::where('user_id', '=', $user->id)->orderBy('created_at', 'DESC')->get();

    	return response()->json(['privateAnnounce'=> $privateAnnounce, 'publicAnnounce' => $publicAnnounce, 'pnoteList' => $pnoteList]);

    }

    // public function addannouce(AddAnnouceRequest $request){
    // 	$user = JWTAuth::toUser($request->token);

    // 	array_except($request, ['token', $request->token]);
        
    // 	//Save announcement and responce
    // 	if ($saveAnnoucement = Announcement::create($request->all())) {
    // 		return response()->json(['success']);
    // 	}

    //     if ($request->user_type == '1') {
        	
    //     }elseif ($request->user_type == '0') {
    //     	// array_add($request, 'user_id', $user->id);
    //     $announceUser = array('announce_id'=>$saveAnnoucement->id, 'user_id' => $user->id);

	   //      //Save and responce
	   //      if (AnnounceUser::create($announceUser)) {
	   //      	return response()->json(['success']);
	   //      }else{
	   //      	return response()->json(['error'], 201);
	   //      }

    //     }

    //     else{
    // 		return response()->json(['error'], 201);
    // 	}

    // }
}
