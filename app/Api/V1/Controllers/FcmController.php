<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Api\V1\Requests\AddFcmRequest;
use App\User;
use App\FcmData;

// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;
use App\Libraries\PushyAPI;


class FcmController extends Controller
{
    
    public function addFcm(AddFcmRequest $request){
    	$user = JWTAuth::toUser($request->token);

    	// if(FcmData::firstOrCreate(['user_id' => $user->id, 'token' => $request->fcm_token])) {
    	// 	return response()->json(['success']);
    	// }else {
    	// 	return response()->json(['error'], 201);
    	// }
    	// 
    	        // ===========FCM=====================
                // $optionBuilder = new OptionsBuilder();
                // $optionBuilder->setTimeToLive(60*20);

                // $notificationBuilder = new PayloadNotificationBuilder('Stretchline | ');
                // $notificationBuilder->setBody('Hello world')
                //                     ->setSound('default');
                                    
                // $dataBuilder = new PayloadDataBuilder();
                // $dataBuilder->addData(['a_data' => 'my_data']);

                // $option = $optionBuilder->build();
                // $notification = $notificationBuilder->build();
                // $data = $dataBuilder->build();

                // $token = "cZSjHjuA7Ps:APA91bHDOVFSKZCUA1GsMFu6RYWwgLgLUNypIgKpx_6UGi8qsoKcPEfl2fujK_21xacfm7RnS92GIlf8m6DKqXQgu-BTAr-Q21SK9esyJcNEp3BwgvyiYeEIok29Q8Wxd62jCvT_aV9x";

                // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);


                //=============pushy test=====================
                // Payload data you want to send to devices
                    $data = array('title' => "Stretchline | KPI's Info", 'body'  => "this is data body.");

                    // The recipient device tokens
                    $to = array('317676ade896724798c85c');

                    // Optionally, send to a publish/subscribe topic instead
                    // $to = '/topics/news';

                    // Optional push notification options (such as iOS notification fields)
                    $options = array(
                        'notification' => array(
                            'badge' => 1,
                            'sound' => 'ping.aiff',
                            'body'  => "this is msg body.",
                            'title' => 'this is title.'

                        )
                    );

                    // Send it with Pushy
                    PushyAPI::sendPushNotification($data, $to, $options);

            // ===========================================
    }
}
