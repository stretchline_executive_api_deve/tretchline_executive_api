<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Api\V1\Requests\TokenRequest;
use App\Api\V1\Requests\PerformanceRequest;
use App\Api\V1\Requests\SubPerformanceRequest;
use App\Api\V1\Requests\StateUpdateRequest;
use App\Api\V1\Requests\SubStateUpdateRequest;

use App\KpiUser;
use App\Milestone;
use Carbon\Carbon;
use App\UserLink;
use App\User;

use App\FcmData;
use App\Libraries\PushyAPI;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class KpiController extends Controller
{
    //Main kpi view for Overall Perfomance
    public function overallperf(TokenRequest $request){
    	$user = JWTAuth::toUser($request->token);
        //Milestone Acheved
        //
        $infoMilestoneUser = KpiUser::where('user_id', '=', $user->id)
                                ->whereNotNull('milestone_id')
                                ->whereNotNull('state')
                                ->get();
        $totalMilestoneUser = $infoMilestoneUser->count();
        $completedMilestoneUser = $infoMilestoneUser->where('state', '=', 'Complete')->count();
        //
        //
    	$kpis = KpiUser::where('user_id', '=', $user->id)->whereNotNull('milestone_id')->get()->unique('kpi_id');
    	$Totalkpis = $kpis->count();  //Total numbers of KPI's
    	//Response array
    	$UserKpi = array();
    	$Completedkpis = 0;   //Completed KPI's
    	foreach ($kpis as $key => $value) {

    		$kpiInfo = KpiUser::with('kpi')
           ->where('user_id', '=', $user->id)
           ->where('kpi_id', '=', $value->kpi_id)
           ->first();

    		//Overall performance
           $kpiMilestones = KpiUser::where('kpi_id', '=', $value->kpi_id)
           ->where('user_id', '=', $user->id)
           ->get(); 
            $totalMilestones = $kpiMilestones->count(); //Total number of milestones for single kpi
            $completedMilestones = $kpiMilestones->where('state', '=', 'Complete')
                                    ->count(); //Number of completed milestones for single kpi

                //target end date
                        $target_end_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->max('end_date');
                                if ($target_end_d == null) {
                                    $target_end = '----/--/--';
                                }else {
                                    $target_end = $target_end_d;
                                }
                                if ($totalMilestones == $completedMilestones) {
                                     $kpiState = 100;
                                     $Completedkpis = $Completedkpis + 1;
                                     $next_review = '----/--/--';
                                 }else{
                                    $completedWeight = 0;
                                    $totalWeight = 0;
                //next review date
                                    $next_review_d = Milestone::where('kpi_id', '=', $value->kpi_id)
                                    ->where('end_date', '>', Carbon::yesterday())
                                    ->min('end_date');
                                    if ($next_review_d != null) {
                                        $next_review = $next_review_d;
                                    }else {
                                        $next_review = '----/--/--';
                                    }

                //loop each milestone for single kpi
                                    foreach ($kpiMilestones as $keyy => $valuee) {
                                        $milestoneInfo = Milestone::where('id', '=', $valuee->milestone_id)
                                        ->first();
                                        if ($valuee->state == 'Complete') {
                                            $completedWeight = $completedWeight + $milestoneInfo->weight;
                                        }
                                        if ($milestoneInfo != null) {
                                        $totalWeight = $totalWeight + $milestoneInfo->weight;
                                        }
                                    }
                //Calculate kpi state for incomplete kpi
                                    // $kpiState = ($completedWeight / $totalWeight) *100;
                                    if ($completedWeight == 0 && $totalWeight == 0) {
                                        $kpiState = 0;
                                    }else {
                                        $kpiState = ($completedWeight / $totalWeight) *100; 
                                    } 

                                }

                                $UserKpi [] = array('kpi_id' => $value->kpi_id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'target_end' => $target_end, 'next_review' => $next_review, 'kpi_state' => round($kpiState));
                            }

    	       //Return data

                            $UserOverall = array('completed_kpis' => $Completedkpis , 'total_kpis' => $Totalkpis, 'total_milestones' => $totalMilestoneUser, 'completed_milestones' =>$completedMilestoneUser);

                            return response()->json(['overalldata' => $UserOverall, 'kpidata' => $UserKpi]);

                        }


                //Single Kpi method for performance
                        public function singleperf(PerformanceRequest $request){
                            $user = JWTAuth::toUser($request->token);

                            $kpiInfo = KpiUser::with('kpi')
                            ->where('user_id', '=', $user->id)
                            ->where('kpi_id', '=', $request->kpi_id)
                            ->first();
        //Retrive kpi milestones
                            $kpiMilestones = KpiUser::where('kpi_id', '=', $request->kpi_id)
                            ->whereNotNull('state')
                            ->whereNotNull('milestone_id')
                            ->where('user_id', '=', $user->id)
                            ->get();

        //next review date
                            $next_reviews = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->where('end_date', '>', Carbon::now())
                            ->min('end_date');
                            if ($next_reviews != null) {
                                $next_review = $next_reviews;
                            }else{
                                $next_review  = '----/--/--';
                            }
        //target end date
                            $kpi_targetends = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->max('end_date'); 
                            if ($kpi_targetends != null) {
                                $kpi_targetend = $kpi_targetends;
                            }else{
                                $kpi_targetend  = '----/--/--';
                            }
 
        //Array
                            $milestoneList = array();
        //Total Milestone presantage
                            $milestoneTotWeight = Milestone::where('kpi_id', '=', $request->kpi_id)
                                                    ->sum('weight');

                            // $milestoneTotWeight = 0;
                            foreach ($kpiMilestones as $key => $value) {
            //milestone complete or not
                            $milestoneStatus = $value->state;

            //Basic infomation of milestone
                            $milestoneInfo = Milestone::where('id', '=', $value->milestone_id)
                                ->first();
            // $milestoneTotWeight = $milestoneTotWeight + $milestoneInfo->weight ; //Total milestone weight
                             $milestonePercentage = round($milestoneInfo->weight*100/$milestoneTotWeight);

            //Add data to array
            $milestoneList [] = array('milestone_id' => $milestoneInfo->id, 'milestone_state' => $milestoneStatus, 'milestone_percentage' => $milestonePercentage, 'end_date' => $milestoneInfo->end_date, 'milestone_name' => $milestoneInfo->name, 'milestone_description' => $milestoneInfo->description);
        }

        if ($kpiInfo != null) {
            $kpiOverall = array('kpi_id'=> $kpiInfo->kpi->id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'next_review' => $next_review, 'kpi_targetend' =>$kpi_targetend);
        }else{
            $kpiOverall = array();
        }
        

            //Response
        return response()->json(['kpiOverall' => $kpiOverall, 'milestone_data' => $milestoneList, 'totalWeight' => $milestoneTotWeight]);
    }
 

    //Kpi Report
    public function milestoneReport(TokenRequest $request){
        $user = JWTAuth::toUser($request->token);

        //get users that under current user.
        $subUsers = UserLink::where('user_id', '=', $user->id)
        ->first();

        if ($subUsers->subuser_id != null) {
            $subUserArray = unserialize($subUsers->subuser_id);
        }else{
            $subUserArray = array();
        }
        

        $userReports = array();

        //Loop sub users
        foreach ($subUserArray as $subUserId) {
            $subUserInfo = User::with('designation', 'grade')
                            ->where('id', '=', $subUserId)
                            ->first();

            //except removed users
            if($subUserInfo != null){


            $totalMilestones = KpiUser::where('user_id', '=', $subUserId)
                                        ->whereNotNull('state')
                                        ->whereNotNull('milestone_id')
                                        ->count('kpi_id');

            $completedMilestones = KpiUser::where('user_id', '=', $subUserId)
                                            ->where('state', '=', 'Complete')
                                            ->whereNotNull('state')
                                            ->whereNotNull('milestone_id')
                                            ->count();

            $userKpi = KpiUser::with('kpi')->where('user_id', '=', $subUserId)
                            ->whereNotNull('state')
                            ->whereNotNull('milestone_id')
                            ->get()->unique('kpi_id');

            //privent empty object casting to array.
            // if ($userKpi->isEmpty()) {

            //     $userKpiList = collect(["null"=> null]);
            //     // $userKpiList = new \stdClass();
                
            // }else {
                
                $userKpiList = $userKpi->values();
            // }

            $milestoneReview = array();
            $totOverDueKpis = 0;
            foreach ($userKpi as $key => $value) {

            #next review date for single kpi
                $next_review_milestone = Milestone::where('kpi_id', '=', $value->kpi_id)
                ->whereNotNull('end_date')
                ->where('end_date', '>', Carbon::now())
                ->min('end_date');
                $milestoneReview [] = array($next_review_milestone);

                //new edit - user kpi overdue count
                $milestoneInfo = KpiUser::with(['milestone' => function($query){
                                    $query->where("end_date", "<", Carbon::now()->format("Y-m-d"));
                                    }])
                                    ->where('kpi_id', '=', $value->kpi_id)
                                    ->where('state', '=', 'Incomplete')
                                    ->get();
                $numOfDueMilestones = $milestoneInfo->where("milestone", "!=", null)->unique('kpi_id')->count();
                if ($numOfDueMilestones != 0) {
                    $totOverDueKpis++;
                }

                //end - new edit - user kpi overdue count

            }

        //Sub user next review date
            if (!empty($milestoneReview)) {

                    $next_review_1 = implode(min($milestoneReview));
                    //empty string fix
                if ($next_review_1 == "") {
                    $next_review = '----/--/--';
                }else{
                    $next_review = $next_review_1;
                }
                
            }else{
                $next_review = '----/--/--';
            }

            $userReports [] = array('user_id' => $subUserId, 'name' => $subUserInfo->name, 'designation' => $subUserInfo->designation->name, 'grade' => $subUserInfo->grade->name, 'completed_milestones' => $completedMilestones, 'total_milestone' => $totalMilestones, 'next_review' => $next_review,'user_img' => $subUserInfo->profile_image, 'totOverDueKpis' => $totOverDueKpis, 'user_kpi' => $userKpiList);
            }

        }

        //Response
        return response()->json(['reportData' => array_values($userReports)]);
    }


    //Single Kpi method for Sub User performance
                        public function sub_singleperf(SubPerformanceRequest $request){
                            // $user = JWTAuth::toUser($request->token);
                            $user = $request->user_id;
                            $kpiInfo = KpiUser::with('kpi')
                            ->where('user_id', '=', $user)
                            ->where('kpi_id', '=', $request->kpi_id)
                            ->first();
        //Retrive kpi milestones
                            $kpiMilestones = KpiUser::where('kpi_id', '=', $request->kpi_id)
                            ->where('user_id', '=', $user)
                            ->get();

        //next review date
                            $next_review = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->where('end_date', '>', Carbon::now())
                            ->min('end_date'); 
        //target end date
                            $kpi_targetend = Milestone::where('kpi_id', '=', $request->kpi_id)
                            ->max('end_date'); 

        //Array
                            $milestoneList = array();
            //Total Milestone presantage
                            $milestoneTotWeight = Milestone::where('kpi_id', '=', $request->kpi_id)
                                                    ->sum('weight');
                            // $milestoneTotWeight = 0;
                            $totMilestonePercentage = 0;
                            foreach ($kpiMilestones as $key => $value) {
            //milestone complete or not
                                $milestoneStatus = $value->state;

            //Basic infomation of milestone
                                $milestoneInfo = Milestone::where('id', '=', $value->milestone_id)
                                ->first();
            // $milestoneTotWeight = $milestoneTotWeight + $milestoneInfo->weight ; //Total milestone weight
                $milestonePercentage = round($milestoneInfo->weight*100/$milestoneTotWeight);
                if ($milestoneStatus == 'Complete') {
                    $totMilestonePercentage = $totMilestonePercentage + $milestonePercentage;
                }                         

            //Add data to array
            $milestoneList [] = array('milestone_id' => $milestoneInfo->id, 'milestone_state' => $milestoneStatus, 'milestone_percentage' => $milestonePercentage, 'end_date' => $milestoneInfo->end_date, 'milestone_name' => $milestoneInfo->name, 'milestone_description' => $milestoneInfo->description);
        }

        $numMilestoneCompleted = KpiUser::where('kpi_id', '=', $request->kpi_id)
                                        ->where('state', '=', 'Complete')
                                        ->count();
        $numMilestone = Milestone::where('kpi_id', '=', $request->kpi_id)
                                    ->count();
        if($numMilestoneCompleted == $numMilestone){
            $kpiOverallPercentage = 100;
        }else {
            $kpiOverallPercentage = $totMilestonePercentage;
        }                              

        if ($kpiInfo != null) {
            $kpiOverall = array('kpi_id'=> $kpiInfo->kpi->id, 'kpi_title' => $kpiInfo->kpi->title, 'kpi_description' => $kpiInfo->kpi->description, 'next_review' => $next_review, 'kpi_targetend' =>$kpi_targetend, 'kpiOverallPercentage'=> $kpiOverallPercentage);
        }else{
            $kpiOverall = array();
        }
        

            //Response
        return response()->json(['kpiOverall' => $kpiOverall, 'milestone_data' => $milestoneList, 'totalWeight' => $milestoneTotWeight]);
    }


    ///User milestone state update(own milestone state)
    public function stateUpdate(StateUpdateRequest $request){
            $user = JWTAuth::toUser($request->token);

            if (KpiUser::where('user_id', '=', $user->id)
                        ->where('kpi_id', '=', $request->kpi_id)
                        ->where('milestone_id', '=', $request->milestone_id)
                        ->update($request->only('state'))) 
            {
            // ===========FCM=====================
                $superUserInfo = UserLink::where('user_id', '=', $user->id)->first();
                $pushyToken = FcmData::where('user_id', $superUserInfo->superuser_id)->first();
                    // if ($fcmToken != null) {
                        
                    
                    // $optionBuilder = new OptionsBuilder();
                    // $optionBuilder->setTimeToLive(60*20);

                    // $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Subuser Milestone Status");
                    // $notificationBuilder->setBody("Your subuser has updated status as follow,\nSubuser Name: ".$user->name."\nMilestone ID: ".$request->milestone_id."\nKPI ID: ".$request->kpi_id."\nStatus: ".$request->state."\n")
                    //                     ->setSound('default');
                                        
                    // $dataBuilder = new PayloadDataBuilder();
                    // $dataBuilder->addData(['a_data' => 'my_data']);

                    // $option = $optionBuilder->build();
                    // $notification = $notificationBuilder->build();
                    // $data = $dataBuilder->build();

                    // $token = $fcmToken->token;

                    // $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                    // }
            // =============pushy=============
            if ($pushyToken != null) {
            // Payload data you want to send to devices
                $data = array('title' => 'Stretchline | Subuser Milestone Status', 'body'  => "Your subuser has updated status as follow,\nSubuser Name: ".$user->name."\nMilestone ID: ".$request->milestone_id."\nKPI ID: ".$request->kpi_id."\nStatus: ".$request->state."\n");

                // The recipient device tokens
                $to = $pushyToken;

                // Optionally, send to a publish/subscribe topic instead
                // $to = '/topics/news';

                // Optional push notification options (such as iOS notification fields)
                $options = array(
                    'notification' => array(
                        'badge' => 1,
                        'sound' => 'ping.aiff',
                        'title' => 'Stretchline | Subuser Milestone Status',
                        'body'  => "Your subuser has updated status as follow,\nSubuser Name: ".$user->name."\nMilestone ID: ".$request->milestone_id."\nKPI ID: ".$request->kpi_id."\nStatus: ".$request->state."\n"
                    )
                );

                // Send it with Pushy
                PushyAPI::sendPushNotification($data, $to, $options);
            }
            // ===========================================

                return response()->json(['success']);
            }else {
                return response()->json(['error'], 201);
            }


    }

    ///Sub User milestone state update(review and update)
    public function subStateUpdate(SubStateUpdateRequest $request){

            if (KpiUser::where('user_id', '=', $request->user_id)
                        ->where('kpi_id', '=', $request->kpi_id)
                        ->where('milestone_id', '=', $request->milestone_id)
                        ->update($request->only('state')))
            {
                return response()->json(['success']);
            }else {
                return response()->json(['error'], 201);
            }


    }


}
