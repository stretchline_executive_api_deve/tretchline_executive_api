<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
// use JWTAuth;

use App\User;
use App\FcmData;

class LoginController extends Controller
{
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['epf_no', 'password']);

        try {
            $token = $JWTAuth->attempt($credentials);

            if(!$token) {
                // throw new AccessDeniedHttpException();
                if (User::where('epf_no', '=', $request->epf_no)->count() == 1) {
                    return response()->json(['error' => 'invalid password'], 201);
                }else{
                    return response()->json(['error' => 'invalid credentials'], 403);
                }
            }

        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        if ($token) {
            $user = \JWTAuth::toUser($token);

            if($request->fcm_token != null){

                if(FcmData::updateOrCreate(['user_id' => $user->id], ['user_id' => $user->id, 'token' => $request->fcm_token])) {
                return response()->json(['status' => 'success', 'token' => $token]);
                }else {
                    
                }
 
            }else{
                
                return response()->json(['status' => 'success', 'token' => $token]);
            }
            
        }else{
            return response()->json(['error'], 201);
        }
        // return response()
        //     ->json([
        //         'status' => 'success',
        //         'token' => $token
        //     ]);
    }
}
