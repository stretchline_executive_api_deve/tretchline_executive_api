<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Api\V1\Requests\TokenRequest;
use App\Api\V1\Requests\MeetingRequest;

use App\RequestMeeting;
use App\RequestState;
use App\User;

use App\Mail\RequestMeetings;
use Mail;

use App\Libraries\PushyAPI;
use App\FcmData;
// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
// use FCM;

class CalendarController extends Controller
{
	//Request a meeting
    public function requestMeeting(MeetingRequest $request){
    	$user = JWTAuth::toUser($request->token);

    	// $receiverArray = unserialize($request->receivers);
        $receiverArray = array($request->receivers); 

    	array_except($request, ['token', $request->token]);
        array_add($request, 'user_id', $user->id);

        if ($insertRequest = RequestMeeting::create($request->all())) {
                // ===========FCM=====================
                // $fcmToken = FcmData::whereIn('user_id', $receiverArray)
                //                     ->pluck('token')->toArray();
                //     if ($fcmToken != null) {
                        
                    
                //     $optionBuilder = new OptionsBuilder();
                //     $optionBuilder->setTimeToLive(60*20);

                //     $notificationBuilder = new PayloadNotificationBuilder("Stretchline | Meeting Request");
                //     $notificationBuilder->setBody("User Name: ".$user->name."\nRequest ID: ".$insertRequest->id."\nMilestone ID: ".$request->milestone_id."\nTitle: ".$request->title."\nMessage: ".$request->message."\nMeeting Date: ".$request->meeting_date."\n")
                //                         ->setSound('default');
                                        
                //     $dataBuilder = new PayloadDataBuilder();
                //     $dataBuilder->addData(['a_data' => 'my_data']);

                //     $option = $optionBuilder->build();
                //     $notification = $notificationBuilder->build();
                //     $data = $dataBuilder->build();

                //     $tokens = $fcmToken;

                //     $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

                //     }

            //============Pushy==========================
                $pushyToken = FcmData::whereIn('user_id', $receiverArray)
                                    ->pluck('token')->toArray();
            if ($pushyToken != null) {
            // Payload data you want to send to devices
                $data = array('title' => 'Stretchline | Meeting Request', 'body' => "User Name: ".$user->name."\nRequest ID: ".$insertRequest->id."\nMilestone ID: ".$request->milestone_id."\nTitle: ".$request->title."\nMessage: ".$request->message."\nMeeting Date: ".$request->meeting_date."\n");

                // The recipient device tokens
                $to = $pushyToken;

                // Optionally, send to a publish/subscribe topic instead
                // $to = '/topics/news';

                // Optional push notification options (such as iOS notification fields)
                $options = array(
                    'notification' => array(
                        'badge' => 1,
                        'sound' => 'ping.aiff',
                        'title' => 'Stretchline | Meeting Request',
                        'body'  => "User Name: ".$user->name."\nRequest ID: ".$insertRequest->id."\nMilestone ID: ".$request->milestone_id."\nTitle: ".$request->title."\nMessage: ".$request->message."\nMeeting Date: ".$request->meeting_date."\n"
                    )
                );

                // Send it with Pushy
                PushyAPI::sendPushNotification($data, $to, $options);
            }
            // ===========================================
        		# create request state
        		foreach ($receiverArray as $receiver) {

        		$reqStateArray = array('request_id' => $insertRequest->id , 'receiver_id' => $receiver, 'state' => 'Pending');
                $receiverInfo = User::where('id', '=', $receiver)->first();
                Mail::to($receiverInfo->email)->send(new RequestMeetings($user->name, $request->milestone_id, $request->title, $request->message, $request->meeting_date, $receiverInfo->name, $receiverInfo->email, $receiver));
        		if (RequestState::create($reqStateArray)) {
        			// return response()->json(['success' => 'Request State & Request']);
        		}else{
        			return response()->json(['error' => 'Request State Fail'], 201);
        		}

        		}
        	return response()->json(['success' => 'Request Meeting']);
        }else{
        	return response()->json(['error' => 'Request Meeting'], 201);
        }

    }
}
