<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use JWTAuth;
use App\Api\V1\Requests\GetUserRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use App\Department;
use App\Grade;
use App\Designation;
use App\User;
use App\UserLink;


use App\Api\V1\Requests\TokenRequest;

class GetUserController extends Controller
{
    /// Retreve user information
    /// 
    public function getAuthUser(GetUserRequest $request){
        $user = JWTAuth::toUser($request->token);

		// retrive data
		$department = Department::where('id', '=', $user->department_id)->first();
		$grade = Grade::where('id','=', $user->grade_id)->first();
		$designation = Designation::where('id', '=', $user->designation_id)->first();

		//add data to array
		array_add($user, 'department_name', $department->name);
		array_add($user, 'grade_name', $grade->name);
		array_add($user, 'designation_name', $designation->name);
        return response()->json(['data' => $user]);
    }



    public function getSubSuperList(TokenRequest $request){

    	$user = JWTAuth::toUser($request->token);
    	//retrive sub -super users
    	$subSuperList = UserLInk::where('user_id', '=', $user->id)->first();
    	
    	 if ($subSuperList != null) {

    	 	// sub user insert
    	 	$subUserList = array();
            if ($subSuperList->subuser_id != null) {
                
            
		    	foreach (unserialize($subSuperList->subuser_id) as $value) {
		    		$subUserInfo = User::where('id', '=', $value)->first();
		    		$subUserList[] = array('id' => $subUserInfo->id, 'name' => $subUserInfo->name, 'email' => $subUserInfo->email, 'gmail' => $subUserInfo->gmail);
		    	}
            }
    		//super user info
    		$superUserInfo = User::where('id', '=', $subSuperList->superuser_id)->first();
            if ($superUserInfo != null) {
    		$superUserList = [array('id' => $superUserInfo->id, 'name' => $superUserInfo->name, 'email' => $superUserInfo->email, 'gmail' => $superUserInfo->gmail)];
             } else{
                // $superUserList = array('id' => null, 'name' => null, 'email' => null, 'gmail' => null);
                $superUserList = array();
             }
    	 	return response()->json(['sub_users' => $subUserList, 'super_user' => $superUserList]);
    	 }else {
    	 	return response()->json(['error']);
    	 }



    }
}
