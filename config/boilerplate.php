<?php

return [

    'sign_up' => [
        'release_token' => env('SIGN_UP_RELEASE_TOKEN'),
        'validation_rules' => [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'login' => [
        'validation_rules' => [
            'epf_no' => 'required',
            'password' => 'required'
        ]
    ],

    'forgot_password' => [
        'validation_rules' => [
            'email' => 'required|email'
        ]
    ],

    'reset_password' => [
        'release_token' => env('PASSWORD_RESET_RELEASE_TOKEN', false),
        'validation_rules' => [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]
    ],

    'get_user'=>[
    'validation_rules'=> [
    'token' => 'required'
        ]
    ],

    'get_token'=>[
    'validation_rules'=> [
    'token' => 'required'
        ]
    ],

    'get_perform'=>[
    'validation_rules'=> [
    'token' => 'required',
    'kpi_id' => 'required'
        ]
    ],

    'get_subperform'=>[
    'validation_rules'=> [
    'token' => 'required',
    'kpi_id' => 'required',
    'user_id' => 'required'
        ]
    ],

    'add_pnote'=>[
    'validation_rules'=> [
    'token' => 'required',
    'title' => 'required',
    'description' => 'required'
        ]
    ],

    'delete_pnote'=>[
    'validation_rules'=> [
    'token' => 'required',
    'pnote_id' => 'required'
       ]
    ],

    'add_note'=>[
    'validation_rules'=> [
    'token' => 'required',
    'kpi_id' => 'required',
    'milestone_id' => 'required',
    'title' => 'required',
    'note' => 'required',
    'next_review' => 'required'
        ]
    ],

    'add_announce'=>[
    'validation_rules'=> [
    'token' => 'required',
    'title' => 'required',
    'description' => 'required',
    'user_type' => 'required'
       ]
    ],

    'get_request_meeting'=>[
    'validation_rules'=> [
    'kpi_id' => 'required',
    'title' => 'required',
    'milestone_id' => 'required',
    'message' => 'required',
    'meeting_date' => 'required',
    'receivers' => 'required'

       ]
    ],

    'user_state'=>[
    'validation_rules'=> [
    'kpi_id' => 'required',
    'milestone_id' => 'required',
    'state' => 'required',
       ]
    ],

    'sub_state'=>[
    'validation_rules'=> [
    'kpi_id' => 'required',
    'milestone_id' => 'required',
    'user_id' => 'required',
    'state' => 'required',
       ]
    ],

    'add_fcm_token'=>[
    'validation_rules'=> [
    'token' => 'required',
    'fcm_token' => 'required'
        ]
    ]



];
