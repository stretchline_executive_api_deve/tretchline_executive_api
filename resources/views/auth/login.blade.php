@extends('layouts.auth')

@section('content')
{{-- <div class="container-fluid login"> --}}
<div class="container">
    <div class="row" style="margin-top: 20%;">

  @if (count($errors) > 0)
  <div  class="col-md-8 col-md-offset-2" >
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;<strong>Whoops!</strong> something went wrong....
{{--             <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul> --}}
        </div>
    </div>
    @endif

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-weight: 400;">
                Login
             {{--    <button type="button" class="btn btn-default pull-right" style="margin-top: -7px; color: #3c8dbc;">
                    <i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>
                </button> --}}
                <a class="btn btn-social-icon btn-dropbox pull-right" data-toggle="modal" data-target="#modal-default" style="margin-top: -7px;"><i class="fa fa-info-circle"></i></a>
            </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus style="font: initial;">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox pull-left">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary pull-left">
                                    Login
                                </button>

                                {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}

{{-- ========login-info modal --}}
  <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Login Infomation</h3>
              </div>
              <div class="modal-body">
                <h3  style="font-weight: 400;" class="text-light-blue">The system has two login roles. </h3>
                <br>
                <h4><strong>ADMIN</strong></h4>
                <p  style="font-weight: 400;">Admin has privilege to manage complete system. But can't enrol with system progress.</p>
                <ul style="text-align: left; font-weight: 400;">
                <li>View Overall Performance of Company and Can filter as Department, Individual or Date range.</li>
                <li>Create and Manage Users.</li>
                <li>Create and Manage Departments, Grades, Designations, KPI, Milestones and Announcements.</li>
                <li>View/Delete Meeting requests and Notes.</li>
              </ul>

                <br>

                <h4><strong>USER</strong></h4>
                <p style="font-weight: 400;">User enrol with system progress and who having following features.</p>
                <ul style="text-align: left; font-weight: 400;">
                <li>View Overall Performance of Subusers.</li>
                <li>View self Overall Performance.</li>
                <li>View Public and Private Announcements.</li>
                <li>View/Confirm/Reject Meeting requests.</li>
                <li>Assigned from Superusers,
                  <ul>
                    <li>View, Overall of assigned KPI.</li>
                    <li>Change status of milestones.</li>
                  </ul>
                </li>
                <li>To Subusers,
                  <ul>
                    <li>View, Overall, Change State of assigned KPI's</li>
                    <li>Create/Copy New KPI</li>
                    <li>Create/Manage Milestones.</li>
                  </ul>
                </li>
              </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
{{-- ===== --}}
</div>
@endsection
