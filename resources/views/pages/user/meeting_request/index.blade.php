@extends('layouts.user.app')

@section('main-content')

{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
 <div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <p>{{ $message }}</p>
</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
 <div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <p>{{ $message }}</p>
</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
 <div class="container col-md-6 col-sm-6">
  <div class="alert alert-danger" role="alert">
   <p>{{ $errors->first() }}</p>
 </div>
</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">

  <div class="row">

    <div class="col-md-12">

      {{-- <h3>Meeting Requests</h3> --}}
      <div class="box box-info">
        <div class="box-header with-border">
          {{-- <h3 class="box-title">Meeting Requests</h3> --}}
          <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            MEETING REQUESTS
                        </h4>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-striped table-hover table-bordered table-responsive table-condensed" id="userMeetingRequestsTable">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Message</th>
                <th>State</th>
                <th>Sender</th>  
                <th>Meeting Time</th>            
                <th>Sent Time</th>
                <th width="140px">Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach ($request_meeting as $request)
              <tr>
                <td>{{ $request->requestMeeting->id }}</td>
                <td>{{ $request->requestMeeting->title }}</td>
                <td>{{ $request->requestMeeting->message }}</td>  
                <td class="alert <?php if($request->state=='Confirmed'){echo 'alert-success';}elseif($request->state=='Pending'){echo 'alert-warning';}elseif($request->state=='Rejected'){echo 'alert-danger';} ?> ">{{ $request->state }}</td>    
                <td>{{ $request->requestMeeting->user->name }}</td>       
                <td>{{ $request->requestMeeting->meeting_date}}</td>                  
                <td>{{ $request->requestMeeting->created_at }}</td>
                
                
                <td>
                  
                  <div class="col-md-6">
                    <?php if($request->state !='Confirmed') {?>
                    <form method="POST" action="{{  url('user/meeting-request/state',$request->requestMeeting->id)  }}" accept-charset="UTF-8">
                      {{ csrf_field() }}
                      
                      <input type="hidden" name="receiver_id" value="{{Auth::user()->id }}">
                      <input type="hidden" name="state" value="Confirmed">
                      <button type="submit" class="btn btn-warning btn-block btn-xs" style="width: 50px;"  data-placement="left" data-singleton="true">Confirm</button>
                    </form>
                    <?php } ?>
                  </div><div class="col-md-6">
                  <form method="POST" action="{{  url('user/meeting-request/state',$request->requestMeeting->id)  }}" accept-charset="UTF-8">
                    {{ csrf_field() }}
                    <input type="hidden" name="receiver_id" value="{{Auth::user()->id }}">
                    <input type="hidden" name="state" value="Rejected">
                    <button type="submit" class="btn btn-danger btn-block btn-xs" style="width: 50px;" data-toggle="confirmation" data-placement="left" data-singleton="true">Reject</button>
                  </form>
                  {{-- <a class="btn btn-warning btn- btn-xs" href="{{}}">Accept</a> --}}
                </div>
                
                
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
      </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div></div>
</div> {{-- row end --}}


@endsection
