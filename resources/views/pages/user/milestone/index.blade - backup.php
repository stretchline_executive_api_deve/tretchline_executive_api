@extends('layouts.user.app')

@section('main-content')
<div class="container-fluid">
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <p>{{ $message }}</p>
  </div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <p>{{ $message }}</p>
  </div>
</div>
@endif
  <div class="" style="background-color: white;">

   <div class="col-sm-12">

<div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">{{$milestonelist[0]->kpi->title}}</h3>
        <hr>
        <br>
        <?php 
// dd($sumoftask);
        if($sum0fweight>0) { ?>
        <div class="progress">
          <div class="progress-bar progress-bar-success progress-bar-striped" style="width: <?php echo round($sumoftask*100/$sum0fweight); ?>%" role="progressbar" aria-valuenow="<?php echo round($sumoftask*100/$sum0fweight); ?>" aria-valuemin="0" aria-valuemax="100">
            <?php echo round($sumoftask*100/$sum0fweight); ?>% Completed
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

      <ol style="font-size: 20px;">
         @if($milestonelist)
         @foreach ($milestonelist as $kpi)
         <li><!-- Task item -->

           <h3>    {{$kpi->milestone->name}}    </h3>

           <div class="row" style="font-size: 14px !important;">
           <div class="col-md-2">
             <span> <strong>weight :</strong> {{round(($kpi->milestone->weight) * 100 / $sum0fweight)}}</span>
           </div>
           <div class="col-md-2">
             <span><strong> Status : </strong>{{$kpi->milestone->milestoneuser->state}}  </span>
           </div>
           <div class="col-md-6">
           <form class="form-inline" action="{{url('user/milestone/stateupdate/'.$kpi->milestone->id.'/'.$milestonelist[0]->kpi->id)}}" method="post" accept-charset="utf-8">
               {{ csrf_field() }}
             <span><strong>Change State to: </strong>
               {{-- button for state --}}
               @if($kpi->milestone->milestoneuser->state == "Complete" or  $kpi->milestone->milestoneuser->state == "Submitted")
               <button type="submit" value="Incomplete" style="width: 100px;" name="state" class="btn btn-primary btn-xs">Incomplete</button>
               @elseif($kpi->milestone->milestoneuser->state == "Incomplete" or  $kpi->milestone->milestoneuser->state == "Submitted")
               <button type="submit" value="Submitted" style="width: 100px;" name="state" class="btn btn-primary btn-xs">Submitted</button>
               @endif
             </span>
             </form>
           </div>
           </div>
         </li>
         @endforeach
         @endif
         </ol>
</div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->


   {{-- ============================================================= --}}
     <div class="panel panel-default">
      <div class="panel-heading"> <h2>{{$milestonelist[0]->kpi->title}}</h2>
        <?php 
// dd($sumoftask);
        if($sum0fweight>0) { ?>
        <small class="pull-right"> <?php echo round($sumoftask*100/$sum0fweight); ?>%</small>   
        <div class="progress sm">
          <div class="progress-bar progress-bar-aqua" style="width: <?php echo round($sumoftask*100/$sum0fweight); ?>%" role="progressbar" aria-valuenow="<?php echo round($sumoftask*100/$sum0fweight); ?>" aria-valuemin="0" aria-valuemax="100">
            <span class="sr-only"><?php echo round($sumoftask*100/$sum0fweight); ?>% Complete</span>
          </div>
        </div>
        <?php } ?>
      </div>

      <div class="panel-body">

       <ol class="menu">
         @if($milestonelist)
         @foreach ($milestonelist as $kpi)
         <li><!-- Task item -->

           <h3>    {{$kpi->milestone->name}}    </h3>

           <div class="row">
           <div class="col-md-2">
             <span> <strong>weight :</strong> {{round(($kpi->milestone->weight) * 100 / $sum0fweight)}}</span>
           </div>
           <div class="col-md-2">
             <span><strong> Status : </strong>{{$kpi->milestone->milestoneuser->state}}  </span>
           </div>
           <div class="col-md-6">
           <form class="form-inline" action="{{url('user/milestone/stateupdate/'.$kpi->milestone->id.'/'.$milestonelist[0]->kpi->id)}}" method="post" accept-charset="utf-8">
               {{ csrf_field() }}
             <span><strong>Change State to: </strong>
               {{-- button for state --}}
               @if($kpi->milestone->milestoneuser->state == "Complete" or  $kpi->milestone->milestoneuser->state == "Submitted")
               <button type="submit" value="Incomplete" style="width: 100px;" name="state" class="btn btn-primary btn-xs">Incomplete</button>
               @elseif($kpi->milestone->milestoneuser->state == "Incomplete" or  $kpi->milestone->milestoneuser->state == "Submitted")
               <button type="submit" value="Submitted" style="width: 100px;" name="state" class="btn btn-primary btn-xs">Submitted</button>
               @endif
             </span>
             </form>
           </div>
           </div>
         </li>
         @endforeach
         @endif

         <!-- end task item -->
               <!--   <li>
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
                <!--   <li>
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
               <!--    <li>
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a> -->
                    <!-- end task item -->
                  </ol>

                </div>
              </div>

            </div>

          </div>
        </div>

        @endsection
