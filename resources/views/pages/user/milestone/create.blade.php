@extends('layouts.user.app')


@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class=" col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="row">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif

<div class="container-fluid">

{{-- <div class="col-md-1"></div> --}}
	<div class="col-md-12">

		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3>Create Milestone for Sub Users</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Create Milestone for Sub Users
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

			{{-- <div class="panel panel-default">
				<div class="panel-heading-old box-header with-border">	<h2>Create Milestone for Sub Users</h2> </div>
				<div class="panel-body"> --}}
					{{-- {{ dd($userInfoArray[0]['user_id'])}} --}}
					<form action="{{ url('user/milestone/store') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
						{{ csrf_field() }}

						<br>
						<div class="form-inline">

						<div class="row">
						<div class="col-md-6">
							<div class="input-group col-sm-12">
								<span class="input-group-addon" id="basic-addon1"><b>Sub User:</b><span style="color:red;">*</span></span>
								<select data-placeholder="Choose a Sub User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" name="subuser_id">
									<option value=""></option>
									@foreach($userInfoArray as $row)
									<option value="{{$row['user_id']}}" @if( old('subuser_id') == $row['user_id']) selected @endif>{{$row['user_name']}}</option>
									@endforeach

								</select> 
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group col-sm-12">
								<span class="input-group-addon" id="basic-addon13"><b>KPI:</b><span style="color:red;">*</span></span>
								<select data-placeholder="Choose a KPI" class="chosen-select2" tabindex="2" aria-describedby="basic-addon13" name="kpi_id">
									<option value=""></option>
					{{-- @foreach($userInfoArray as $row)
					<option value="{{$row['user_id']}}">{{$row['user_name']}}</option>
					@endforeach --}}
				</select>
			</div>
						</div>
						</div>
							
							
							
			<?php if($errors->has('kpi_id')){ ?>
			<label class="error-msg" role="alert"><?php echo $errors->first('kpi_id'); ?></label> 
			<?php }?>
		</div>
		<br/>

		<div class="form-group">
			<label for="text">Milestone name:<span style="color:red;">*</span></label>
			<input type="text" class="form-control"  name="name[]" value="{{ old('name.0') }}" required>
			<?php if($errors->has('name.*')){ ?>
			<label class="error-msg" role="alert"><?php echo $errors->first('name.*'); ?></label> 
			<?php }?>
		</div>

		<div class="form-group">
			<label for="text">Milestone description:<span style="color:red;">*</span></label>
			<input type="text" class="form-control"  name="description[]" value="{{ old('description.0') }}" required>
			<?php if($errors->has('description.*')){ ?>
			<label class="error-msg" role="alert"><?php echo $errors->first('description.*'); ?></label> 
			<?php }?>
		</div>
		<div class="form-group">
			<label for="number">Weight:<span style="color:red;">*</span></label>
			<input type="number" min="0" id="number" class="form-control"  name="weight[]" value="{{ old('weight.0') }}">
			<?php if($errors->has('weight.*')){ ?>
			<label class="error-msg" role="alert"><?php echo $errors->first('weight.*'); ?></label> 
			<?php }?>
		</div>
		<div class="form-group">
			<label for="text">Start Date:<span style="color:red;">*</span></label>
			<input type="text" class="form-control date-basic-addon15"  name="start_date[]" id="date-basic-addon15" value="{{ old('start_date.0') }}" required>
			<?php if($errors->has('start_date.*')){ ?>
			<label class="error-msg" role="alert"><?php echo $errors->first('start_date.*'); ?></label> 
			<?php }?>

		</div> 
		<div class="form-group">
			<label for="text">End Date:<span style="color:red;">*</span></label>
			<input type="text" class="form-control date-basic-addon16"  name="end_date[]" id="date-basic-addon16" value="{{ old('end_date.0') }}" required>
			<?php if($errors->has('end_date.*')){ ?>
			<label class="error-msg" role="alert"><?php echo $errors->first('end_date.*'); ?></label> 
			<?php }?>
		</div> 
		<br>
			<!-- <div class="input-group">
				<span class="input-group-addon" id="basic-addon2">Milestone: </span>
				<select data-placeholder="Choose a Milestone" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" required name="milestone_id">
					<option value=""></option>

				</select>
			</div>
			
		-->
		<div class="milestone-box input_fields_wrap">
		</div>

	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<div class="col-sm-6"> <button type="submit" style="width: 100px;" class="btn btn-primary">Save</button></div>
		<div class="col-sm-6">	<a href="#" id="button-add-more" class="btn btn-default btn-block btn-flat pull-right" style="width: 100px;">Add More&nbsp;&nbsp;<i class="fa fa-plus" aria-hidden="true"></i>
		</a></div>
	</form>
</div>
<!-- /.box-footer -->
</div>
<!-- /.box -->
{{-- </div>
</div> --}}

</div>
</div>



@endsection
