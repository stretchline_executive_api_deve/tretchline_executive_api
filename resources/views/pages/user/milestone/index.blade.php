@extends('layouts.user.app')

@section('main-content')
<div class="container-fluid">
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <p>{{ $message }}</p>
  </div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <p>{{ $message }}</p>
  </div>
</div>
@endif
  <div class="row">

   <div class="col-sm-12">

<div class="box box-info">
      <div class="box-header with-border">
        {{-- <h3 class="box-title">{{$milestonelist[0]->kpi->title}}</h3> --}}
        <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            {{$milestonelist[0]->kpi->title}}
                        </h4>
        <hr>
        <br>
        <?php 
// dd($sumoftask);
        if($sum0fweight>0) { ?>
        <div class="progress col-md-8 col-md-offset-2">
          <div class="progress-bar progress-bar-success progress-bar-striped active" style="width: <?php echo round($sumoftask*100/$sum0fweight); ?>%" role="progressbar" aria-valuenow="<?php echo round($sumoftask*100/$sum0fweight); ?>" aria-valuemin="0" aria-valuemax="100">
            <?php echo round($sumoftask*100/$sum0fweight); ?>% Completed
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

      <ol style="font-size: 20px;">
         @if($milestonelist)
         @foreach ($milestonelist as $kpi)
         <li><!-- Task item -->

           <h3>    {{$kpi->milestone->name}}    </h3>

           <div class="row" style="font-size: 14px !important;">
           <div class="col-md-2">
             <span> <strong>Weight :</strong> {{round(($kpi->milestone->weight) * 100 / $sum0fweight)}}%</span>
           </div>
           <div class="col-md-2">
             <span><strong> Status : </strong>{{$kpi->milestone->milestoneuser->state}}  </span>
           </div>
           <div class="col-md-6">
           <form class="form-inline" action="{{url('user/milestone/stateupdate/'.$kpi->milestone->id.'/'.$milestonelist[0]->kpi->id)}}" method="post" accept-charset="utf-8">
               {{ csrf_field() }}
             <span><strong>Change State to: </strong>
               {{-- button for state --}}
               @if($kpi->milestone->milestoneuser->state == "Complete" or  $kpi->milestone->milestoneuser->state == "Submitted")
               <button type="submit" value="Incomplete" style="width: 100px;" name="state" class="btn btn-primary btn-xs">Incomplete</button>
               @elseif($kpi->milestone->milestoneuser->state == "Incomplete" or  $kpi->milestone->milestoneuser->state == "Submitted")
               <button type="submit" value="Submitted" style="width: 100px;" name="state" class="btn btn-primary btn-xs">Submitted</button>
               @endif
             </span>
             </form>
           </div>
           </div>
         </li>
         @endforeach
         @endif
         </ol>
</div>
        <!-- /.box-body -->
        <div class="box-footer">
        <a href="{{ URL::previous() }}" style="width: 100px;" class="btn btn-default btn-sm""><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->

            </div>

          </div>
        </div>

        @endsection
