@extends('layouts.user.app')


@section('main-content')


	 @include('pages.user.milestone.showfield')


<div class="container-fluid box-form">

<div class="col-sm-12">


	
		 <div class="panel panel-default">
		  <div class="panel-heading-old box-header with-border">	<h2>Create Milestone for This KPI</h2> </div>
		  <div class="panel-body">
	
		<form action="{{ url('user/milestone/store') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
			{{ csrf_field() }}

			<br>
			<div class="input-group">
				
				<input type="hidden" name="kpi_id" class="form-control" readonly="readonly"  value="{{ old('kpi_id', isset($kpi->id) ? $kpi->id : null) }}">									
				
			</div>
  			

<br/>

				 <div class="form-group">
				    <label for="text">Milestone name <span style="color:red;">*</span> :</label>
				    <input type="text" class="form-control"  name="name[]">
				      <?php if($errors->has('name[]')){ ?>
					          <label class="error-msg" role="alert"><?php echo $errors->first('name[]'); ?></label> 
				 <?php }?>
				 </div>

				 <div class="form-group">
				    <label for="text">Milestone description <span style="color:red;">*</span>:</label>
				    <input type="text" class="form-control"  name="description[]">
				      <?php if($errors->has('description[]')){ ?>
					          <label class="error-msg" role="alert"><?php echo $errors->first('description[]'); ?></label> 
			 <?php }?>
				  </div>
				  <div class="form-group">
				    <label for="number">weight <span style="color:red;">*</span>:</label>
				    <input type="number" class="form-control"  name="weight[]">
				      <?php if($errors->has('weight[]')){ ?>
					          <label class="error-msg" role="alert"><?php echo $errors->first('weight[]'); ?></label> 
			 <?php }?>
				  </div>
				   <div class="form-group">
				    <label for="text">start date <span style="color:red;">*</span>:</label>
				    <input type="text" class="form-control selector2"  name="start_date[]">
				      <?php if($errors->has('start_date[]')){ ?>
					          <label class="error-msg" role="alert"><?php echo $errors->first('start_date[]'); ?></label> 
			 <?php }?>

				  </div> 
				     <div class="form-group">
				    <label for="text">End date <span style="color:red;">*</span>:</label>
				    <input type="text" class="form-control selector"  name="end_date[]">
				      <?php if($errors->has('end_date[]')){ ?>
					          <label class="error-msg" role="alert"><?php echo $errors->first('end_date[]'); ?></label> 
			 <?php }?>
				  </div> 
				  <br>
			<!-- <div class="input-group">
				<span class="input-group-addon" id="basic-addon2">Milestone: </span>
				<select data-placeholder="Choose a Milestone" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" required name="milestone_id">
					<option value=""></option>

				</select>
			</div>
			
		 -->
		 <div class="milestone-box input_fields_wrap">
		 </div>

		
		<div class="col-sm-6"> <button type="submit" style="width: 100px;" class="btn btn-primary">Save</button></div>
		<div class="col-sm-6">	<a href="#" id="button-add-more" class="btn btn-default btn-block btn-flat pull-right" style="width: 100px;">Add More<i class="fa fa-plus" aria-hidden="true"></i>
</a></div>
		</form>

		</div>
	</div>

</div>
</div>



@endsection
