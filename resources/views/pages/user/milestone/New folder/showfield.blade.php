
<div class="container-fluid">

<div class="" style="background-color: white;">
      
    
 <div class="panel panel-default">
  <div class="panel-heading"> <h2>{{$kpi->title}}</h2>

              <small class="pull-right"> <?php echo round($sumoftask*100/$sum0fweight); ?>%</small>   
              <div class="progress sm">
                        <div class="progress-bar progress-bar-aqua" style="width: <?php echo round($sumoftask*100/$sum0fweight); ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                  </div>
  </div>

     

  <div class="panel-body">
    

 <ol class="menu">
 @if($milestonelist)
 	@foreach ($milestonelist as $kpi)
                  <li><!-- Task item -->
                  <!-- <a href="{{url('user/milestone/delete/'.$kpi->milestone->id)}}" class="pull-right btn-xs btn-danger"> Delete</a> -->
                    <a href="#">
                      <h3>    {{$kpi->milestone->name}}    </h3>
                   <p> weight : {{$kpi->milestone->weight}}  <span> Status : {{$kpi->milestone->milestoneuser->state}}  </span> </p>
                    </a>
                  </li>
	@endforeach
  @endif

                  <!-- end task item -->
               <!--   <li>
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
                <!--   <li>
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
               <!--    <li>
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a> -->
                  <!-- end task item -->
                </ol>

  </div>
</div>



</div>
</div>
