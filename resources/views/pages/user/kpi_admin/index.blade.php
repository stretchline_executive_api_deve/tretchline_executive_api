@extends('layouts.user.app')

@section('main-content')
<div class="container-fluid">
  



       {{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}

<div class="row">

	<div class="col-md-12">
		
		{{-- <h3>My KPI List</h3> --}}
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3>My KPI List</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            My KPI List
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

		<table class="table table-striped table-hover table-bordered table-responsive table-condensed" id="myKpiListTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th> 
					<th>Description</th>
					
					<th>Created</th>
					<th width="280px">Action</th>
				</tr>
			</thead>
			
			@foreach ($users_kpis as $key => $kpi)
			<tr>
				<td>{{ $kpi->kpi->id }}</td>
				<td>{{ $kpi->kpi->title }}</td>
				<td>{{ $kpi->kpi->description }}</td>
				<td>{{ date_format($kpi->kpi->created_at, 'd-m-Y') }}</td>
				<td style="width: 20%">
					
				<div class="col-md-6">
						<a class="btn btn-warning btn-block btn-xs" 
						@if($kpi->milestone_id == null)
						disabled="disabled"
						@endif
						href="{{ url('user/milestone',$kpi->kpi_id) }}">VIEW</a>
					</div>
					
				</td>
			</tr>
			@endforeach
		</table>
</div>
				<!-- /.box-body -->
				<div class="box-footer">
			<div class="pull-left padd-top">
		<a class="btn btn-default btn-sm" style="width: 100px;" href="{{ url('user/super_user', Auth::user()->id ) }}"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp; Back</a>
		</div>
		</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}

</div>

@endsection
