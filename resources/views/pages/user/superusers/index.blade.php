@extends('layouts.user.app')

@section('main-content')
<div class="container-fluid">


<div class="box box-info">
			<div class="box-header with-border">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Super User & Assigned KPIs
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

			<div class="col-sm-8" style="padding-top: 20px;">
       	<div class="list-group">

{{--       <a href=" {{url('/user/admin_kpis/'.Auth::user()->id) }}" class="list-group-item "><span class="badge">@isset($super_users_link->user->kpiuser){{ count($super_users_link->user->kpiuser)}} @endisset @empty($super_users_link->user->kpiuser) 0 @endempty</span> @isset($super_users->name)  {{ $super_users->name}}  @endisset @empty($super_users->name) You haven't any super users!@endempty</a> --}}
      @if($super_users == null)
      		<a href="#" class="list-group-item ">You haven't any super users!</a>
      @else
      		<a href=" {{url('/user/admin_kpis/'.Auth::user()->id) }}" class="list-group-item "><span class="badge">@isset($super_users_link->user->kpiuser){{ count($super_users_link->user->kpiuser)}} @endisset @empty($super_users_link->user->kpiuser) 0 @endempty</span> @isset($super_users->name)  {{ $super_users->name}}  @endisset @empty($super_users->name) You haven't any super users!@endempty</a>
      @endif


		</div>
       </div>

			</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
</div>

@endsection
