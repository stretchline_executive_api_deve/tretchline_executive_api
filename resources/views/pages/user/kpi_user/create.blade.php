@extends('layouts.user.app')


@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class=" col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="row">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
<div class="container-fluid">

	<div class="row">
		<div class="col-md-12">

			<div class="box box-info">
				<div class="box-header with-border">
					{{-- <h3>Create KPI for Subuser</h3> --}}
					<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Create KPI for Subuser
                        </h4>
				</div>
				<!-- /.box-header -->
				<div class="box-body">


							<form action="{{ url('/user/kpi/store') }}" id="kpi-form" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
								{{ csrf_field() }}

								<br>
								<div class="input-group col-md-6">
									<span class="input-group-addon" id="basic-addon1"><b>User:</b><span style="color:red;">*</span> </span>
									<select data-placeholder="Choose a User" class="chosen-select kpi-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px"  name="user_id">
										<option value=""></option>
										@isset($userList)
										@foreach($userList as $row)
										<option value="{{$row->id}}" @if (old('user_id') == $row->id) selected @endif>{{$row->name }}</option>
										@endforeach
										@endisset
									</select>

								</div>

								<?php if($errors->has('user_id')){ ?>
								<label class="error-msg" role="alert"><?php echo $errors->first('user_id'); ?></label> 
								<?php }?>
								<br>

								<div class="form-group">
									<label for="text">KPI Title:<span style="color:red;">*</span></label>
									<input type="text" class="form-control title" value="{{old('title')}}"  name="title" required>
									<?php if($errors->has('title')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('title'); ?></label> 
									<?php }?>
								</div>
								<div class="form-group">
									<label for="text">KPI Description:<span style="color:red;">*</span></label>
									<input type="text" class="form-control description" value="{{old('description')}}"  name="description" required>
									<?php if($errors->has('description')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('description'); ?></label> 
									<?php }?>
								</div>

								


								<br>
			<!-- <div class="input-group">
				<span class="input-group-addon" id="basic-addon2">Milestone: </span>
				<select data-placeholder="Choose a Milestone" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" required name="milestone_id">
					<option value=""></option>

				</select>
			</div>
			<br>
		-->

	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<button type="submit"  style="width: 100px;"  class="btn btn-primary">Save</button>
		{{-- <a href="{{url('/user/milestone/create')}}" id='saveandcontinu' name="add_milestone" class="btn pull-right btn-primary"> Save & Create Mileston</a> --}}
		<button type="submit" name="add_milestone" id='saveandcontinu' class="btn btn-primary pull-right">Save & Create Mileston</button>
	</form>
</div>
<!-- /.box-footer -->
</div>
<!-- /.box -->

</div></div></div>

@endsection
