@extends('layouts.user.app')

@section('main-content')
<div class="container-fluid">
  



       {{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}

<div class="row">

	<div class="col-md-12">
	
		{{-- {{dd(count($users_kpis[0]->miles->))}} --}}
		{{-- {{dd($users_kpis[0]['kpi_id'])}} --}}
		{{-- <h3>User KPI List</h3> --}}
<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3>User KPI List</h3> --}}
				{{-- <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            User KPI List
                        </h4> --}}
{{-- 
						<div class="user-block">
			                <img class="img-circle" src="{{ asset(Auth::user()->profile_image)}}" alt="User Image">
			                <span class="username"><a href="#">{{$userInfo->name}}</a></span>
			                <span class="description">{{$userInfo->designation->name}}</span>
			              </div>
              			<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            User KPI List
                        </h4> --}}
<div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="{{ asset($userInfo->profile_image)}}" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">{{$userInfo->name}}</h3>
              <h5 class="widget-user-desc">{{$userInfo->designation->name}}</h5>
            </div>
   {{--          <div class="box-footer no-padding">
            </div> --}}
          </div>


			</div>
			<!-- /.box-header -->
			<div class="box-body">
		<table class="table table-hover display nowrap" id="subuserKpiMilesTable" style="width: 100%;">
			<thead>
				<tr>
					<th>#</th>
					<th>KPI Title</th>
					<th>KPI Description</th>
					<th>KPI Overall Progress</th>
					<th>Created</th>
					<th width="10px;">Milestone Review</th>
					<th width="10px;">Milestone Overdue</th>
					<th width="280px">Action</th>
				</tr>
			</thead>
			{{-- {{dd($users_kpis)}} --}}
			@php $i = 0; @endphp
			<tbody>
			@foreach ($users_kpis as $kpi)
			@php ++$i;  @endphp
			<tr>
				<td>{{ $i }}</td>
				<td>{{ $kpi['title'] }}</td>
				<td>{{ $kpi['description'] }}</td>
				<td style="width: 15%;">
					@if($kpi['milestone_nullcount'] > 0)
					@else
					<a href="{{ url('user/milestone/edit',$kpi['kpi_id'] ) }}">
					@endif
					{{-- <div class="row" style="width: 100%;padding-right: initial;padding-left: inherit;">
						<div class="col-md-10" style="padding-left: 10px;padding-right: 0px;">
							<div class="progress-group"> --}}
								{{-- <span class="progress-text">&nbsp;</span> --}}
								{{-- <span class="progress-number badge bg-purple"><b>{{$kpi['kpiState']}}%</b></span --}}
							<div class="progress progress-xs progress-striped active" style="height: 18px;border-radius: 9px;">
		                      <div class="progress-bar progress-bar-info" style="width: {{$kpi['kpiState']}}%"><font color="#800080">{{$kpi['kpiState']}}%</font></div>
		                    </div>
		                	</div>
						{{-- </div>
						<div class="col-md-2" style="padding-right: 0px;padding-left: 1px; padding-top: 2px">
							<span class="progress-number badge bg-purple"><b>{{$kpi['kpiState']}}%</b></span>
						</div>
						
					</div> --}}
					@if($kpi['milestone_nullcount'] > 0)
					@else
					</a>
					@endif
				</td>
				<td>{{ date_format($kpi['created_at'], 'd-m-Y') }}</td>
				<td @if($kpi['submitted'] != '0') style=" border: 1px solid; border-color: #e5c335; border-radius: 5px; border-width: 2px; margin: -2px; width: 10px;" @endif><strong><center>{{ $kpi['submitted'] }}</center></strong>
				</td>
				<td @if($kpi['numOfDueMilestones'] > 0) style="border: 1px solid; border-color: #e53535; border-radius: 5px; border-width: 2px;  margin: -2px; width: 10px;" @endif>
					<strong><center>{{$kpi['numOfDueMilestones']}}</center><strong>
				</td>
				<td style="width: 20%">
					{{-- {{ dd($kpi) }} --}}
					<div class="row" style="max-width: 300px;">
					<div class="col-md-4">
						<a class="btn btn-warning btn-xs"
						@if($kpi['milestone_nullcount'] > 0)
						disabled="disabled"
						@endif
						href="{{ url('user/milestone/edit',$kpi['kpi_id'] ) }}" style="width: 50px;"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;VIEW</a>
					</div>
					<div class="col-md-4">
						<a class="btn btn-primary btn-xs"
						@if($kpi['milestone_nullcount'] > 0)
						disabled="disabled"
						@endif
						href="{{ url('user/milestone/copy',$kpi['kpi_id'] ) }}" style="width: 50px;">Copy</a>
					</div>
					<div class="col-md-4">
						<form method="POST" action="{{ url('user/kpi/delete', $kpi['kpi_id'] ) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
						
								<input type="hidden" name="user_id" value="{{$kpi['user_id']}}">
							<button type="submit" class="btn btn-danger btn-xs" data-toggle="confirmation" data-placement="left" data-singleton="true" style="width: 50px;">DELETE</button>
						</form>
					</div>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
		</table>
		</div>
				<!-- /.box-body -->
				<div class="box-footer">
		<div class="pull-left padd-top">
			<a class="btn btn-default btn-sm" style="width: 100px;" href="{{ url('user/sub_users', Auth::user()->id ) }}"> Back</a>
		</div>
		</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}

</div>

@endsection
