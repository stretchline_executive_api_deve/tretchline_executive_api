@extends('layouts.user.app')


@section('main-content')


<div class="container-fluid ">

	<div class="row">

		<div class="col-md-12">

<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3>Edit Milestone for This KPI</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Copy Milestone And KPI from Previous
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">


					{{-- <form action="{{ url('user/milestone/copyupdate',$milestonelist[0]->kpi->id) }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
						{{ csrf_field() }} --}}
					<form action="{{ url('user/milestone/copyupdate') }}" id="kpi-form" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
								{{ csrf_field() }}

						<br>
						<div class="col-sm-12">
							<div class="input-group col-md-6">
						<span class="input-group-addon" id="basic-addon1"><b>User:</b></span>
						<select data-placeholder="Choose a User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="user_id">
							<option value=""></option>
							@foreach($userList as $row)
							<option value="{{$row->id}}">{{$row->name }}</option>
							@endforeach 
						</select>
					</div>
					<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
					<br>
							<div class="form-group">
								<label for="text">Kpi Title <span style="color:red;">*</span> :</label>
								<input type="text" class="form-control"  name="title" value="{{$milestonelist[0]->kpi->title}}" required>
								<?php if($errors->has('title')){ ?>
								<label class="error-msg" role="alert"><?php echo $errors->first('title'); ?></label> 
								<?php }?>
							</div>
							<div class="form-group">
								<label for="text">Kpi Description <span style="color:red;">*</span> :</label>
								<input type="text" class="form-control"  name="kpidescription" value="{{$milestonelist[0]->kpi->description}}" required>
								<?php if($errors->has('kpidescription')){ ?>
								<label class="error-msg" role="alert"><?php echo $errors->first('kpidescription'); ?></label> 
								<?php }?>
							</div>
							<hr style="border-color: #93c0ff;">
						</div>
						@if($milestonelist)
						@foreach ($milestonelist as $kpi)
						{{-- {{dd($kpi->state)}} --}}
						<input type="hidden" name="milestone_id[]" value="{{$kpi->milestone->id}}">
						<div class=" box-body"> 	
							<div class="col-sm-6">
								<div class="form-group">
									<label for="text">Milestone Name  <span style="color:red;">*</span> :</label>
									<input type="text" class="form-control"  name="name[]" value="{{$kpi->milestone->name}}" required>
									<?php if($errors->has('name[]')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('name[]'); ?></label> 
									<?php }?>
								</div>
								<div class="form-group">
									<label for="text">Milestone Description  <span style="color:red;">*</span> :</label>
									<input type="text" class="form-control"  name="description[]" value="{{$kpi->milestone->description}}" required>
									<?php if($errors->has('description[]')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('description.*'); ?></label> 
									<?php }?>
								</div>

							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label for="text">Milestone start date  <span style="color:red;">*</span> :</label>
									<input type="text" class="form-control"  name="start_date[]" value="{{$kpi->milestone->start_date}}" required>
									<?php if($errors->has('start_date[]')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('start_date[]'); ?></label> 
									<?php }?>
								</div>

								<div class="form-group">
									<label for="text">Milestone end date  <span style="color:red;">*</span> :</label>
									<input type="text" class="form-control"  name="end_date[]" value="{{$kpi->milestone->end_date}}" required>
									<?php if($errors->has('end_date[]')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('end_date.*'); ?></label> 
									<?php }?>
								</div>
							</div>

							<div class="col-sm-6">

								<div class="form-group">
									<label for="number">Milestone weight  <span style="color:red;">*</span> :</label>
									<input type="number" id="number" min="0" class="form-control"  name="weight[]" value="{{$kpi->milestone->weight}}" required>
									<?php if($errors->has('weight[]')){ ?>
									<label class="error-msg" role="alert"><?php echo $errors->first('weight.*'); ?></label> 
									<?php }?>
								</div>
							</div>
							<div class="col-sm-6">
								<br>
								<div class="input-group

								@if($kpi->state == 'Incomplete' && Carbon\Carbon::today()->format('Y-m-d') > $kpi->milestone->end_date)
								has-error
								@endif
								@if($kpi->state == 'Complete')
								has-success
								@endif
								@if($kpi->state == 'Submitted')
								has-warning
								@endif
								">
								<span class="input-group-addon" id="basic-addon2"><b>Milestone State:</b></span>
								<select class="form-control" aria-describedby="basic-addon2" style="width: 40%" name="state[]">
									<option value="" disabled selected>Choose a Milestone State</option>

									<option value="Incomplete"
									@if($kpi->state == 'Incomplete')
									selected
									@endif

									>Incomplete</option>
									<option value="Submitted"
									@if($kpi->state == 'Submitted')
									selected
									@endif
									>Submitted</option>
									<option value="Complete"
									@if($kpi->state == 'Complete')
									selected
									@endif
									>Complete</option>
								</select>
								<span style="color: red">&nbsp;&nbsp;{{ $errors->first('state.*') }}</span>
							</div>

						</div>
						</div>
						<hr>
						@endforeach
						@endif


						</div>
				<!-- /.box-body -->
				<div class="box-footer">
				<a href="{{ URL::previous() }}" style="width: 100px;" class="btn btn-default btn-sm""><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
						<button type="submit" style="width: 100px;" class="btn btn-primary pull-right btn-sm">Copy</button>

						

				</div>
				</form>
</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
			
		</div>
		</div>
	</div>

	@endsection
