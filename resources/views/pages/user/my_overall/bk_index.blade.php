@extends('layouts.user.app')

@section('main-content')
<div class="container-fluid">
	{{-- <h2>Home</h2> --}}
	<div class="row">
		{{-- {{dd($UserOverall)}} --}}
		{{-- {{dd($UserKpi)}} --}}
		<div class="box box-solid">
			<div class="box-body">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
					OVERALL PERFORMANCE
				</h4>
				<br>
				@php  
					function remove_element($array,$value) {
  						return array_diff($array, (is_array($value) ? $value : array($value)));
					}
 
					$myArray = (array_column($UserKpi, 'next_review'));
 
					$next_review_filtered = min(remove_element($myArray,'----/--/--'));
					// dd($next_review_filtered);
					
				@endphp
				<div class="col-md-6">
					<div class="box box-widget widget-user-2">
						<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class="widget-user-header bg-yellow">
							<div class="widget-user-image">
								<img class="img-circle" src="{{asset(Auth::user()->profile_image)}}" alt="User Avatar">
							</div>
							<!-- /.widget-user-image -->
							<h3 class="widget-user-username">{{ Auth::user()->name }}</h3>
							<h5 class="widget-user-desc">{{$UserOverall['designation']}}</h5>
						</div>
						<div class="box-footer no-padding">
							<ul class="nav nav-stacked">
								<li><a href="#">KPI's <span class="pull-right badge bg-blue">{{$UserOverall['total_kpis']}}</span></a></li>
								<li><a href="#">Completed KPI's <span class="pull-right badge bg-aqua">{{$UserOverall['completed_kpis']}}</span></a></li>
								<li><a href="#">Milestones <span class="pull-right badge bg-purple">{{$UserOverall['userTotMilestone']}}</span></a></li>
								<li><a href="#">Completed Milestones<span class="pull-right badge bg-green">{{$UserOverall['userTotCompMiles']}}</span></a></li>
								<li><a href="#">Overdue Milestones<span class="pull-right badge bg-red">{{$UserOverall['userTotOverDueMiles']}}</span></a></li>
								<li><a href="#">Next Review Date <span class="pull-right badge bg-yellow">{{$next_review_filtered}}</span></a></li>
							</ul>
						</div>
					</div>
					{{-- {{dd(min(array_column($UserKpi, 'next_review')))}} --}}
				</div>
				<div class="col-md-6">
					<div class="box box-default">
						<div class="box-header with-border">
							{{-- <i class="fa fa-warning"></i> --}}

							<h4 class="text-center">KPIs Completion</h4>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="scrollbar" id="style-10">
								<div class="force-overflow">

									@php $i=0 @endphp
									@foreach($UserKpi as $value)
									@php ++$i @endphp
									<div class="progress-group" style="padding-right: 20px;padding-left: 10px;">
										<span class="progress-text">{{$value['kpi_title']}}</span>
										<span style="text-align: center;color: #cccccc"><small> | Target End: {{$value['target_end']}}</small></span>
										<span class="progress-number"><b>{{$value['kpi_state']}}%</b></span>

										<div class="progress sm">
										<div class="progress-bar progress-bar-striped active
											@if($i == 1)
											progress-bar-aqua
											@elseif($i == 2)
											progress-bar-blue
											@elseif($i == 3)
											progress-bar-green
											@elseif($i == 4)
											progress-bar-yellow
											@elseif($i == 5)
											progress-bar-red
											@endif
											" style="width: {{$value['kpi_state']}}%"></div>
										</div>
									</div>
									<!-- /.progress-group -->
									@if($i == 5)
									@php $i=0 @endphp
									@endif
									@endforeach
								</div>
							</div>
						</div>
						<!-- /.box-body -->
					</div>

				</div>
			</div>



		</div>
	</div>
</div>

@endsection
