@extends('layouts.user.app')

@section('main-content')

{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
       <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <p>{{ $message }}</p>
       </div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
       <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <p>{{ $message }}</p>
       </div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
       <div class="container col-md-6 col-sm-6">
              <div class="alert alert-danger" role="alert">
                     <p>{{ $errors->first() }}</p>
              </div>
       </div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">

<div class="row">
       <div class="col-md-12">
              
<div class="box box-info">
      <div class="box-header with-border">
        {{-- <h3>Announcements</h3> --}}
        <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            ANNOUNCEMENTS
                        </h4>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Public Announcement</a></li>
  <li><a data-toggle="tab" href="#menu1">Private Announcement</a></li>
  
</ul>
<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    {{-- <h3>Public Announcement</h3> --}}
    <table class="table table-striped table-hover table-bordered table-responsive table-condensed" id="pubannounceTable" style="width: 100%;">
                     <thead>
                            <tr>
                                   <th>#</th>
                                   <th>Title</th>
                                   <th>Description</th>
                                    <th>Expire</th>
                                   <th>Created</th>
                                
                            </tr>
                     </thead>
               <tbody>
                     @foreach ($publicAnnounces as $key => $announceIn)
                     <tr>
                            <td>{{ $announceIn->id }}</td>
                            <td>{{ $announceIn->title }}</td>
                            <td>{{ $announceIn->description }}</td>
                            <td>{{ $announceIn->end_date }}</td>                         
                            <td>{{ date_format($announceIn->created_at, 'd-m-Y') }}</td>
                       
                     </tr>
                     @endforeach
                     </tbody>
              </table>
  </div>
  <div id="menu1" class="tab-pane fade">
    {{-- <h3>Private Announcement</h3> --}}
    <table class="table table-striped table-hover table-bordered table-responsive table-condensed" id="pvtannounceTable" style="width: 100%;">
                     <thead>
                            <tr>
                                   <th>#</th>
                                   <th>Title</th>
                                   <th>Description</th>
                                  <th>Expire</th>
                                   <th>Created</th>
                                   <!-- <th width="280px">Action</th> -->
                            </tr>
                     </thead>
                  <tbody>
                    
                  
               
                     @foreach ($privateAnnounces as $key => $announceIn)
                     @if( $announceIn->announcement->start_date <= \Carbon\Carbon::today()->format('Y-m-d') && $announceIn->announcement->end_date >= \Carbon\Carbon::today()->format('Y-m-d'))
                     <tr>
                            <td>{{ $announceIn->id }}</td>
                            <td>{{ $announceIn->announcement->title }}</td>
                            <td>{{ $announceIn->announcement->description }}</td>
                            <td>{{ $announceIn->announcement->end_date }}</td>   
                            <td>{{ date_format($announceIn->announcement->created_at, 'd-m-Y') }}</td>
                     
                     </tr>
                     @endif
                     @endforeach
                     </tbody>
              </table>
  </div>
  
</div>

</div>
        <!-- /.box-body -->
        <div class="box-footer">
</div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
             
       </div>
</div> {{-- row end --}}
</div>

@endsection
