@extends('layouts.user.app')

@section('main-content')

<div class="container-fluid">

<div class="row">
       <div class="col-md-12">


<div class="box box-info">
      <div class="box-header with-border">
        <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            SUB USERS
                        </h4>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
<div class="col-sm-8">
        <div class="list-group">
      @if($sub_users != null)
       @foreach ($sub_users as $sub_user)
      <a href=" {{url('/user/user_kpis/'.$sub_user->id) }}" class="list-group-item "><span class="badge bg-purple">{{count($sub_user->kpiuser)}}</span>  {{ $sub_user->name}}  </a>
        @endforeach
        @else
        <a href="" class="list-group-item "> You haven't any subuser assigned yet!</a>
        @endif
    </div>
       </div>
      </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->


</div>
</div>

</div>

@endsection
