@extends('layouts.app')

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
      <div class="col-md-12">
      <div class="row">
        <section class="content-header">
        <img src="{{'../img/icons/bar_chart.png'}}" class="img-responsive header_img" >
        <h1 class="header_title">OVERALL PERFORMANCE</h1>
      {{-- <div class="breadcrumb"> --}}
          <div class="info-box user_count">
            <span class="info-box-number">{{$overallPerf['totUsers']}}</span>
              <span class="info-box-text">USERS</span>
              
            <!-- /.info-box-content -->
          </div>
          <div class="info-box user_count" style="margin-right: 1px;" data-toggle="modal" data-target="#modal-default">
            <span class="info-box-number" style="color: #89eb92;"><i class="fa fa-filter" aria-hidden="true"></i></span>
              <span class="info-box-text" style="color: #89eb92;">FILTER</span>
              
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
      {{-- </div> --}}
        </section>
      </div>

              <!-- Info boxes -->
      <div class="row dash_box">
        <div class="col-md-3 col-sm-6 col-xs-12 box-col1">
        <a class="showSingle" target="1">
          <div class="info-box" tabindex="0">
            {{-- <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> --}}

            <div class="info-box-content">
              <span class="info-box-text">COMPLETED KPI'S</span>
              <span class="info-box-number">{{$overallPerf['completeKpiPre']}}<small>%</small></span>
              <i class="fa fa-sort-desc fa-lg" aria-hidden="true" style="position: absolute; margin-top: 36px; margin-left: -25px; visibility: hidden; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);color: #ffffff"></i>
               <div class="progress">
                <div class="progress-bar" style="width: {{$overallPerf['completeKpiPre']}}%"></div>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </a>
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12 box-col2">
        <a class="showSingle" target="2" id="showall">
          <div class="info-box" tabindex="1">
            {{-- <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span> --}}

            <div class="info-box-content">
              <span class="info-box-text">ONGOING KPI'S</span>
              <span class="info-box-number">{{$overallPerf['ongoinKpiPre']}}<small>%</small></span>
              <i class="fa fa-sort-desc fa-lg" aria-hidden="true" style="position: absolute; margin-top: 36px; margin-left: -36px;  visibility: hidden; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);color: #ffffff"></i>
              <div class="progress">
                <div class="progress-bar" style="width: {{$overallPerf['ongoinKpiPre']}}%"></div>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          </a>
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12 box-col3">
        <a class="showSingle" target="3">
          <div class="info-box" tabindex="2">
            {{-- <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span> --}}

            <div class="info-box-content">
              <span class="info-box-text">OVERDUE KPI'S</span>
              <span class="info-box-number">{{$overallPerf['overdueKpiPre']}}<small>%</small></span>
              <i class="fa fa-sort-desc fa-lg" aria-hidden="true" style="position: absolute; margin-top: 36px; margin-left: -36px; visibility: hidden; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);color: #ffffff"></i>
              <div class="progress">
                <div class="progress-bar" style="width: {{$overallPerf['overdueKpiPre']}}%"></div>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          </a>
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12 box-col4">
          <div class="info-box">
            {{-- <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span> --}}

            <div class="info-box-content">
              <span class="info-box-text">NEXT REVIEW DATE</span>
              {{-- <span class="info-box-number">2<small>%</small></span> --}}
              <div class="progress">
                {{-- <div class="progress-bar" style="width: 70%"></div> --}}
                <span class="info-box-text text-center">{{$overallPerf['overall_next_review']}}</span>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>
    </div>

          {{-- ====milestone and kpi row===== --}}
      <div class="row">
        <div class="col-md-12 targetDiv"  id="div1" style="display: none;">
        {{-- kpi container --}}
        <div class="col-md-6 kpi_box">
          <div class="row kpi_box_head text-center">
               <span class="head1">{{$overallPerf['completed_kpis']}}</span><span class="head2">/{{$overallPerf['total_kpis']}}</span>
               <p>COMPLETED KPI'S</p>
          </div>
          {{-- <div class="row"> --}}
              @php $i=100 @endphp
              @php $p=10 @endphp
              @foreach ($byKpi as $comValue)
              @if($comValue['kpiCurrentState'] == 'COMPLETE')
              @php ++$p @endphp
               <div class="row kpi_box_body" tabindex="{{++$i}}" onclick="getCompleteMilestones({{$comValue['kpi_user_id']}}, {{$comValue['kpi_id']}})">
               {{-- <div> --}}
                <div class="col-md-4">
                  <span class="text-uppercase"><b>{{$comValue['kpi_title']}}</b></span>
                  <a data-toggle="collapse" data-target="#kpidis{{++$i}}">READ MORE</a>
                  <p id="kpidis{{$i}}" class="collapse lead">{{$comValue['kpi_description']}}</p>
                </div>
                <div class="col-md-6">
                    <span style="float: left;"><b>NEXT REVIEW:</b><small>{{$comValue['next_review']}}</small></span> <span style="float: right;"><b>TARGET END:</b><small>{{$comValue['target_end']}}</small></span>
                    <br>
                      <div class="progress
                      @if($p == 11)
                      completed-bar-color1
                      @elseif($p == 12)
                      completed-bar-color2
                      @elseif($p == 13)
                      completed-bar-color3
                      @elseif($p == 14)
                      completed-bar-color4
                      @elseif($p == 15)
                      completed-bar-color5
                      @endif
                      ">
                        <div class="progress-bar" style="width: {{$comValue['kpi_state']}}%"></div>
                      </div>
                </div>
                <div class="col-md-2">
                  <span><b>BY {{$comValue['kpi_user_name']}}</b></span>
                  <img src="../{{$comValue['kpi_user_img']}}" class="img-circle img-responsive center-block" style="width: 40px;">
                  <i class="fa fa-angle-right fa-lg" style="float: right; margin-top: -27px; color: #5f87a3;" aria-hidden="true"></i>
                </div>
                {{-- </div> --}}
              </div>
              @if($p == 15)
              @php $p=10 @endphp
              @endif
              @endif
          @endforeach
          {{-- </div> --}}
        </div>
        {{-- -end kpi container- --}}
         <div class="col-md-6 miles_box">
        <div id="sticky-anchor1"></div>
        <div id="sticky1">
              <div class="row miles_box_head text-center" >
               {{-- <p></p> --}}
               <span>MILESTONES</span>
              </div>
              <div id="milestone_row1"></div>
              </div>
        </div>
      </div>


{{-- ================================= --}}

              <div class="col-md-12 targetDiv"  id="div2">
        {{-- kpi container --}}
        <div class="col-md-6 kpi_box">
          <div class="row kpi_box_head text-center">
               <span class="head1">{{$overallPerf['totOngingKpis']}}</span><span class="head2">/{{$overallPerf['total_kpis']}}</span>
               <p>ONGOING KPI'S</p>
          </div>
          {{-- <div class="row"> --}}
              @php $i=0 @endphp
              @php $p=0 @endphp
              @foreach ($byKpi as $comValue)
              @if($comValue['kpiCurrentState'] == 'ONGOING')
              @php ++$p @endphp
               <div class="row kpi_box_body" tabindex="{{++$i}}" onclick="getOngoingMilestones({{$comValue['kpi_user_id']}}, {{$comValue['kpi_id']}})">
               {{-- <div> --}}
                <div class="col-md-4">
                  <span class="text-uppercase"><b>{{$comValue['kpi_title']}}</b></span>
                  <a data-toggle="collapse" data-target="#kpidis{{++$i}}">READ MORE</a>
                  <p id="kpidis{{$i}}" class="collapse lead">{{$comValue['kpi_description']}}</p>
                </div>
                <div class="col-md-6">
                    <span style="float: left;"><b>NEXT REVIEW:</b><small>{{$comValue['next_review']}}</small></span> <span style="float: right;"><b>TARGET END:</b><small>{{$comValue['target_end']}}</small></span>
                    <br>
                      <div class="progress
                      @if($p == 1)
                      ongoing-bar-color1
                      @elseif($p == 2)
                      ongoing-bar-color2
                      @elseif($p == 3)
                      ongoing-bar-color3
                      @elseif($p == 4)
                      ongoing-bar-color4
                      @elseif($p == 5)
                      ongoing-bar-color5
                      @endif
                      ">
                        <div class="progress-bar" style="width: {{$comValue['kpi_state']}}%"></div>
                      </div>
                </div>
                <div class="col-md-2">
                  <span><b>BY {{$comValue['kpi_user_name']}}</b></span>
                  <img src="../{{$comValue['kpi_user_img']}}" class="img-circle img-responsive center-block" style="width: 40px;">
                  <i class="fa fa-angle-right fa-lg" style="float: right; margin-top: -27px; color: #5f87a3;" aria-hidden="true"></i>
                </div>
                {{-- </div> --}}
              </div>
              @if($p == 5)
                  @php $p=0 @endphp
                  @endif
              @endif
          @endforeach
          {{-- </div> --}}
        </div>
        {{-- -end kpi container- --}}
        
        <div class="col-md-6 miles_box">
        <div id="sticky-anchor2"></div>
        <div id="sticky2">
              <div class="row miles_box_head text-center" >
               {{-- <p></p> --}}
               <span>MILESTONES</span>
              </div>
              <div id="milestone_row2"></div>
              </div>
              {{-- <div class="row miles_box_body">
                <div class="col-md-2">
                <i class="fa fa-check-circle fa-lg" aria-hidden="true"></i>
                  <span>30<small>%</small></span>
                </div>
                <div class="col-md-10">
                  <span class="lead">Tortor fusce ullamcorper fugiat. Quae habitasse? Debitis? Duis atque lorem, luctus sed. Architecto inceptos, gravida venenatis.</span><a data-toggle="collapse" data-target="#readMore">READ MORE</a>
                  <p id="readMore" class="collapse lead">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                  <p><strong>2017-08-08<strong></p>
                </div>
              </div>
              <div class="row miles_box_body">
                <div class="col-md-2">
                <i class="fa fa-circle-thin fa-lg" aria-hidden="true"></i>
                  <span>45<small>%</small></span>
                </div>
                <div class="col-md-10">
                  <span class="lead">Tortor fusce ullamcorper fugiat. Quae habitasse? Debitis? Duis atque lorem, luctus sed. Architecto inceptos, gravida venenatis.</span>
                  <span>2017-08-08</span>
                </div>
              </div> --}}
        </div>
      </div>

{{-- ================================= --}}


              <div class="col-md-12 targetDiv"  id="div3" style="display: none;">
        {{-- kpi container --}}
        <div class="col-md-6 kpi_box">
          <div class="row kpi_box_head text-center">
               <span class="head1">{{$overallPerf['totOverDueKpis']}}</span><span class="head2">/{{$overallPerf['total_kpis']}}</span>
               <p>OVERDUE KPI'S</p>
          </div>
          {{-- <div class="row"> --}}
                @php $i=200 @endphp
                @php $p=20 @endphp
              @foreach ($byKpi as $comValue)
              @if($comValue['kpiCurrentState'] == 'OVERDUE')
              @php ++$p @endphp
               <div class="row kpi_box_body" tabindex="{{++$i}}" onclick="getOverDueMilestones({{$comValue['kpi_user_id']}}, {{$comValue['kpi_id']}})">
               {{-- <div> --}}
                <div class="col-md-4">
                  <span class="text-uppercase"><b>{{$comValue['kpi_title']}}</b></span>
                  <a data-toggle="collapse" data-target="#kpidis{{++$i}}">READ MORE</a>
                  <p id="kpidis{{$i}}" class="collapse lead">{{$comValue['kpi_description']}}</p>
                </div>
                <div class="col-md-6">
                    <span style="float: left;"><b>NEXT REVIEW:</b><small>{{$comValue['next_review']}}</small></span> <span style="float: right;"><b>TARGET END:</b><small>{{$comValue['target_end']}}</small></span>
                    <br>
                      <div class="progress
                      @if($p == 21)
                      overdue-bar-color1
                      @elseif($p == 22)
                      overdue-bar-color2
                      @elseif($p == 23)
                      overdue-bar-color3
                      @elseif($p == 24)
                      overdue-bar-color4
                      @elseif($p == 25)
                      overdue-bar-color5
                      @endif
                      ">
                        <div class="progress-bar" style="width: {{$comValue['kpi_state']}}%"></div>
                      </div>
                </div>
                <div class="col-md-2">
                  <span><b>BY {{$comValue['kpi_user_name']}}</b></span>
                  <img src="../{{$comValue['kpi_user_img']}}" class="img-circle img-responsive center-block" style="width: 40px;">
                  <i class="fa fa-angle-right fa-lg" style="float: right; margin-top: -27px; color: #5f87a3;" aria-hidden="true"></i>
                </div>
                {{-- </div> --}}
              </div>
               @if($p == 25)
                @php $p=20 @endphp
                  @endif
              @endif
          @endforeach
          {{-- </div> --}}
        </div>
        {{-- -end kpi container- --}}
         <div class="col-md-6 miles_box">
        <div id="sticky-anchor3"></div>
        <div id="sticky3">
              <div class="row miles_box_head text-center" >
               {{-- <p></p> --}}
               <span>MILESTONES</span>
              </div>
              <div id="milestone_row3"></div>
              </div>
</div>
      </div>
      </div>
      {{-- =========end kpi milestone row======= --}}
      {{-- ======Filter Results======== --}}
       <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Filter Overall Performance</h4>
              </div>
              <div class="modal-body">
                
{{-- ============================= --}}
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Department level</a></li>
              <li><a href="#tab_2" data-toggle="tab">Individual level</a></li>
              <li><a href="#tab_3" data-toggle="tab">Time period</a></li>
            </ul>
            <div class="tab-content" style="height: 150px;">
              <div class="tab-pane active" id="tab_1">
                  <form action="{{ url('admin/home') }}" method="GET" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
          {{ csrf_field() }}

                <div class="form-group">
                  <label class="col-sm-2 control-label" style="padding-top: 7px;">Department:</label>
                  {{-- <span class="input-group-addon" id="basic-addon8"><b>Department: </b></span> --}}
                  <div class="col-sm-10">
                  <select placeholder="Choose a Departmant" class="form-control chosen-select5" tabindex="2" id="basic-addon8" style="width: 300px" name="department_id">
                    <option value=""></option>
                    @foreach($departmentList as $row)
                    <option value="{{$row->id}}" @if (old('department_id') == $row->id) selected @endif>{{$row->name }}</option>
                    @endforeach 
                  </select>
                  <span style="color: red">&nbsp;&nbsp;@if($errors->first('department_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('department_id') }}</span>
                </div>
                </div>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-top: 20px;">Close</button>
                <button type="submit" name="dipartment_filter" class="btn btn-primary pull-right" style="margin-top: 20px;">Filter</button>
                </form>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                 <form action="{{ url('admin/home') }}" method="GET" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
          {{ csrf_field() }}

                <div class="form-group">
                  <label class="col-sm-2 control-label" style="padding-top: 7px;">USER:</label>
                  {{-- <span class="input-group-addon" id="basic-addon8"><b>Department: </b></span> --}}
                  <div class="col-sm-10">
                  <select placeholder="Choose a Departmant" class="form-control chosen-select5" tabindex="2" id="basic-addon8" style="width: 300px" name="user_id">
                    <option value=""></option>
                    @foreach($userList as $row)
                    <option value="{{$row->id}}" @if (old('user_id') == $row->id) selected @endif>{{$row->name }}</option>
                    @endforeach 
                  </select>
                  <span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
                </div>
                </div>

                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-top: 20px;">Close</button>
                <button type="submit" name="individual_filter" class="btn btn-primary pull-right" style="margin-top: 20px;">Filter</button>
                </form>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <form action="{{ url('admin/home') }}" method="GET" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
          {{ csrf_field() }}

                <div class="form-inline" style="margin-top: 35px;">
                <div class="form-group">
                  <label for="date-basic-addon13">Start Date:</label>
                  <input type="text" class="form-control date-basic-addon13" placeholder="Milestone Start Date" name="start_date" id="date-basic-addon13" value="{{old('start_date')}}" autofocus required>
                  <span style="color: red">&nbsp;&nbsp;@if($errors->first('start_date'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('start_date') }}</span>
                </div>
                <div class="form-group">
                  <label for="date-basic-addon14">&nbsp;&nbsp;End Date:</label>
                  <input type="text" class="form-control date-basic-addon14" placeholder="Milestone End Date" name="end_date" id="date-basic-addon14" value="{{old('end_date')}}" autofocus required>
                  <span style="color: red">&nbsp;&nbsp;@if($errors->first('end_date'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('end_date') }}</span>
                </div>
                </div>

                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-top: 20px;">Close</button>
                <button type="submit" name="time_filter" class="btn btn-primary pull-right" style="margin-top: 20px;">Filter</button>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->


{{-- ===================================== --}}
              </div>
              {{-- <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Filter</button>
                </form>
              </div> --}}
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

      {{-- ==========end filter results====== --}}
      
  </div>

{{-- Ajax call for milestone load --}}
  <script>
         function getCompleteMilestones(userId, kpiId){
          $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
               type:'GET',
               url:'../admin/getMilestones',
               data:{user_id:userId, kpi_id:kpiId},
               success:function(data){
                   loop: { // response loop
                      $("#milestone_row1").empty()
                        for(var i=0; i<data.milestone_data.length; i++) {
                              //if complete milestone
                                if (data.milestone_data[i].milestone_state == 'Complete') {
                                $("#milestone_row1").append("<div class='row miles_box_body'><div class='col-md-2'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i><span>"+data.milestone_data[i].milestone_percentage+"<small>%</small></span></div><div class='col-md-10'><span class='lead'>"+data.milestone_data[i].milestone_name+"</span><a data-toggle='collapse' data-target='#1readMore"+i+"'>READ MORE</a><p id='1readMore"+i+"' class='collapse lead'>"+data.milestone_data[i].milestone_description+"</p><p><strong>"+data.milestone_data[i].end_date+"<strong></p></div></div>");
                                    console.log("Found: "+data.milestone_data[i].milestone_id);
                                    // break loop;
                                } else {
                                  //incomplete
                                $("#milestone_row1").append("<div class='row miles_box_body'><div class='col-md-2'><i class='fa fa-circle-thin fa-lg' aria-hidden='true'></i><span>"+data.milestone_data[i].milestone_percentage+"<small>%</small></span></div><div class='col-md-10'><span class='lead'>"+data.milestone_data[i].milestone_name+"</span><a data-toggle='collapse' data-target='#1readMore"+i+"'>READ MORE</a><p id='1readMore"+i+"' class='collapse lead'>"+data.milestone_data[i].milestone_description+"</p><p><strong>"+data.milestone_data[i].end_date+"<strong></p></div></div>");
                                    console.log("Found: "+data.milestone_data[i].milestone_id);
                                    // break loop;
                                }

                            }
                            console.log("No even number found.");
                        }


               },
               error:function(data){
                  $("#milestone_row1").empty()
               }
            });
         }
      </script>
       <script>
         function getOngoingMilestones(userId, kpiId){
          $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
               type:'GET',
               url:'../admin/getMilestones',
               data:{user_id:userId, kpi_id:kpiId},
               success:function(data){
                   loop: { // response loop
                      $("#milestone_row2").empty()
                        for(var i=0; i<data.milestone_data.length; i++) {
                              //if complete milestone
                                if (data.milestone_data[i].milestone_state == 'Complete') {
                                $("#milestone_row2").append("<div class='row miles_box_body'><div class='col-md-2'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i><span>"+data.milestone_data[i].milestone_percentage+"<small>%</small></span></div><div class='col-md-10'><span class='lead'>"+data.milestone_data[i].milestone_name+"</span><a data-toggle='collapse' data-target='#2readMore"+i+"'>READ MORE</a><p id='2readMore"+i+"' class='collapse lead'>"+data.milestone_data[i].milestone_description+"</p><p><strong>"+data.milestone_data[i].end_date+"<strong></p></div></div>");
                                    console.log("Found: "+data.milestone_data[i].milestone_id);
                                    // break loop;
                                } else {
                                  //incomplete
                                $("#milestone_row2").append("<div class='row miles_box_body'><div class='col-md-2'><i class='fa fa-circle-thin fa-lg' aria-hidden='true'></i><span>"+data.milestone_data[i].milestone_percentage+"<small>%</small></span></div><div class='col-md-10'><span class='lead'>"+data.milestone_data[i].milestone_name+"</span><a data-toggle='collapse' data-target='#2readMore"+i+"'>READ MORE</a><p id='2readMore"+i+"' class='collapse lead'>"+data.milestone_data[i].milestone_description+"</p><p><strong>"+data.milestone_data[i].end_date+"<strong></p></div></div>");
                                    console.log("Found: "+data.milestone_data[i].milestone_id);
                                    // break loop;
                                }

                            }
                            console.log("No even number found.");
                        }


               },
               error:function(data){
                  $("#milestone_row2").empty()
               }
            });
         }
      </script>
       <script>
         function getOverDueMilestones(userId, kpiId){
          $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
            $.ajax({
               type:'GET',
               url:'../admin/getMilestones',
               data:{user_id:userId, kpi_id:kpiId},
               success:function(data){
                   loop: { // response loop
                      $("#milestone_row3").empty()
                        for(var i=0; i<data.milestone_data.length; i++) {
                              //if complete milestone
                                if (data.milestone_data[i].milestone_state == 'Complete') {
                                $("#milestone_row3").append("<div class='row miles_box_body'><div class='col-md-2'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i><span>"+data.milestone_data[i].milestone_percentage+"<small>%</small></span></div><div class='col-md-10'><span class='lead'>"+data.milestone_data[i].milestone_name+"</span><a data-toggle='collapse' data-target='#3readMore"+i+"'>READ MORE</a><p id='3readMore"+i+"' class='collapse lead'>"+data.milestone_data[i].milestone_description+"</p><p><strong>"+data.milestone_data[i].end_date+"<strong></p></div></div>");
                                    console.log("Found: "+data.milestone_data[i].milestone_id);
                                    // break loop;
                                } else {
                                  //incomplete
                                $("#milestone_row3").append("<div class='row miles_box_body'><div class='col-md-2'><i class='fa fa-circle-thin fa-lg' aria-hidden='true'></i><span>"+data.milestone_data[i].milestone_percentage+"<small>%</small></span></div><div class='col-md-10'><span class='lead'>"+data.milestone_data[i].milestone_name+"</span><a data-toggle='collapse' data-target='#3readMore"+i+"'>READ MORE</a><p id='3readMore"+i+"' class='collapse lead'>"+data.milestone_data[i].milestone_description+"</p><p><strong>"+data.milestone_data[i].end_date+"<strong></p></div></div>");
                                    console.log("Found: "+data.milestone_data[i].milestone_id);
                                    // break loop;
                                }

                            }
                            console.log("No even number found.");
                        }


               },
               error:function(data){
                  $("#milestone_row3").empty()
               }
            });
         }
      </script>
@endsection
