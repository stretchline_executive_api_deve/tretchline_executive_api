@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid" style="min-height: 100%;height: auto !important;">

<div class="row">
	<div class="col-md-12">
		{{-- <h3>Grades</h3> --}}
		
		<div class="box box-info">
			<div class="box-header with-border">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Grades
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
		<div class="pull-right">
			<a class="btn btn-success btn-sm" href="{{ route('grade.create')}}"> Add Grade</a>
		</div>
		<table class="table table-bordered" id="gradeTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Description</th>
					<th>Created</th>
					<th width="280px">Action</th>
				</tr>
			</thead>
			@php $i=0 @endphp
			@foreach ($gradeInfo as $key => $gradeInfo)
			<tr>
				<td>{{ $gradeInfo->id }}</td>
				<td>{{ $gradeInfo->name }}</td>
				<td>{{ $gradeInfo->description }}</td>
				<td>{{ date_format($gradeInfo->created_at, 'd-m-Y') }}</td>
				<td style="width: 20%">
					{{-- <div class="col-md-2"></div> --}}
					<div class="col-md-6">
						<a class="btn btn-warning btn-block btn-xs" href="{{ route('grade.edit',$gradeInfo->id) }}">EDIT</a>
					</div>
					<div class="col-md-6">
						<form method="POST" action="{{ route('grade.destroy', $gradeInfo->id) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-block btn-xs disabled" data-toggle="confirmation" data-placement="left" data-singleton="true">DELETE</button>
						</form>
					</div>
					{{-- <div class="col-md-2"></div> --}}
				</td>
			</tr>
			@endforeach
		</table>
		</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}
</div>
@endsection
