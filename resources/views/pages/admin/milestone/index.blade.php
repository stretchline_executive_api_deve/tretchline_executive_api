@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">

<div class="row">
	<div class="col-md-12">
		{{-- <h3>Milestone</h3> --}}
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">Update Grade</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           Milestone
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
		<div class="pull-right">
			<a class="btn btn-success btn-sm" href="{{ route('milestone.create')}}"> Add Milestones</a>
		</div>
		<table class="table table-bordered" id="departmentTable" style="width: 100%">
			<thead>
				<tr>
					<th>#</th>
					<th>KPI</th>
					<th>Name</th>
					<th>Description</th>
					<th>Weight</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>User</th>
					<th>State</th>
					<th>Created</th>
					<th width="280px">Action</th>
				</tr>
			</thead>

			@php $i=0 @endphp
			@foreach ($milestoneInfo as $key => $milestoneInfo)
			<tr>
				<td>{{ $milestoneInfo->id }}</td>
				<td>{{ $milestoneInfo->kpi->title}}</td>
				<td>{{ $milestoneInfo->name }}</td>
				<td>{{ $milestoneInfo->description }}</td>
				<td>{{ $milestoneInfo->weight }}</td>
				<td>{{ $milestoneInfo->start_date }}</td>
				<td>{{ $milestoneInfo->end_date }}</td>
				<td>@isset($milestoneInfo->milestoneuser->user){{$milestoneInfo->milestoneuser->user->name}} @endisset</td>
				<td>
					@if(isset($milestoneInfo->milestoneuser->state) && $milestoneInfo->milestoneuser->state == 'Complete')
						<span class="label label-success">{{ $milestoneInfo->milestoneuser->state }}</span>
					@elseif(isset($milestoneInfo->milestoneuser->state) && $milestoneInfo->milestoneuser->state == 'Submitted')
						<span class="label label-warning">{{ $milestoneInfo->milestoneuser->state }}</span>
					@elseif(isset($milestoneInfo->milestoneuser->state) && $milestoneInfo->milestoneuser->state == 'Incomplete')
						<span class="label label-danger">{{ $milestoneInfo->milestoneuser->state }}</span>
					@endif

				</td>
				<td>{{ date_format($milestoneInfo->created_at, 'd-m-Y') }}</td>
				<td style="width: 20%">
					<div class="row">
					<div class="col-md-6">
						<a class="btn btn-warning btn-xs" style="width: 50px;" href="{{ route('milestone.edit',$milestoneInfo->id) }}">EDIT</a>
					</div>
					<div class="col-md-6">
						<form method="POST" action="{{ route('milestone.destroy', $milestoneInfo->id) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-xs" style="width: 50px;" data-toggle="confirmation" data-placement="left" data-singleton="true">DELETE</button>
						</form>
					</div>
					</div>
				</td>
			</tr>
			@endforeach
		</table>
		</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}
</div>
@endsection
