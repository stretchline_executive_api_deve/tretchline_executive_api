@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
<div class="container-fluid">
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
</div>
<div class="container-fluid">
<div class="row">
<div class="col-md-1"></div>

	<div class="col-md-10">

          <div class="box box-info">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Create Milestone</h3> --}}
              <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            CREATE MILESTONE
                        </h4>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
 
              <form action="{{ route('milestone.store') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
			{{ csrf_field() }}
			 
				{{-- <div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><b>KPI:</b></span>
					<select data-placeholder="Choose a Kpi" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="kpi_id">
						<option value=""></option>
						@foreach($kpiList as $row)
						<option value="{{$row->id}}">{{$row->title }}</option>
						@endforeach 
					</select>
					<span style="color: red">&nbsp;&nbsp;@if($errors->first('kpi_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('kpi_id') }}</span>
				</div> --}}
				<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><b>User:</b><span style="color:red;">*</span></span>
								<select data-placeholder="Choose a User" class="chosen-select3" tabindex="2" aria-describedby="basic-addon1" style="width: 300px"  name="user_id">
									<option value=""></option>
									@foreach($userList as $row)
									<option value="{{$row['id']}}" @if(Session::get('user_id') == $row['id']) selected @endif @if (old('user_id') == $row['id']) selected @endif>{{$row['name']}}</option>
									@endforeach

								</select>
							</div>
							<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon13"><b>KPI:</b><span style="color:red;">*</span></span>
								<select data-placeholder="Choose a KPI" class="chosen-select4" tabindex="2" aria-describedby="basic-addon13" style="width: 300px"  name="kpi_id">
									<option value=""></option>
									@if (old('kpi_id') != null)
										<option value="{{old('kpi_id')}}" selected>{{old('kpi_id')}}</option>
									@endif
								@if(Session::get('kpi_id'))	<option value="{{Session::get('kpi_id')}}" selected>{{Session::get('kpi_name')}}</option> @endif
								</select>
							</div>
							<span style="color: red">&nbsp;&nbsp;@if($errors->first('kpi_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('kpi_id') }}</span>
						</div>
						</div>
						<br>
				
			<div id="sections">
				<div class="section">
				<fieldset style="border: solid 2px; border-color: #d8d8d8; padding: 10px; margin-bottom: 2px;">
				<button class="remove pull-right btn btn-danger" style="display: none;">x</button>
				{{-- <legend>Personalia:</legend> --}}
					<div class="form-group">
						<label for="basic-addon1">Name:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('name.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('name.0') }}</span>
						<input type="text" class="form-control" placeholder="Milestone Name" name="name[]" id="basic-addon1" value="{{old('name.0')}}" autofocus required>
					</div>
					<div class="form-group">
						<label for="basic-addon2">Description:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('description.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description.0') }}</span>
						<textarea class="form-control" placeholder="Milestone Description" name="description[]" id="basic-addon2" value="{{old('description.0')}}" rows="3" required>{{old('description.0')}}</textarea>
					</div>
					<div class="form-group">
						<label for="number">Weight:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('weight.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('weight.0') }}</span>
						<input type="number" min="0" class="form-control" placeholder="Milestone Weight" name="weight[]" id="number" value="{{old('weight.0')}}" autofocus required>
					</div>
					<div class="form-inline">
					<div class="form-group">
						<label for="date-basic-addon13">Start Date:<span style="color:red;">*</span></label>
						<input type="text" class="form-control date-basic-addon13" placeholder="Milestone Start Date" name="start_date[]" id="date-basic-addon13" value="{{old('start_date.0')}}" autofocus required>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('start_date.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('start_date.0') }}</span>
					</div>
					<div class="form-group">
						<label for="date-basic-addon14">&nbsp;&nbsp;End Date:<span style="color:red;">*</span></label>
						<input type="text" class="form-control date-basic-addon14" placeholder="Milestone End Date" name="end_date[]" id="date-basic-addon14" value="{{old('end_date.0')}}" autofocus required>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('end_date.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('end_date.0') }}</span>
					</div>
					</div>
					</fieldset>
				</div>
			</div>
		
              <!-- /.box-body -->
              <div class="box-footer">
              	<a href="{{ route('milestone.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
                <button id="add-milestone" class="btn btn-success addsection" style="width: 110px;">Add another&nbsp;&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></button>
                <button type="submit" class="btn btn-info pull-right" style="width: 100px;">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.box -->

	</div>
<div class="col-md-1"></div>
</div>
</div>
@endsection