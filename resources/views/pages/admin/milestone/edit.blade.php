@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">Update Milestone</h3> --}}
				<div class="user-block col-sm-6">
                    <img class="img-circle img-bordered-sm" src="@isset($milestoneInfo->milestoneuser->user){{asset($milestoneInfo->milestoneuser->user->profile_image)}} @endisset" alt="User Image">
                        <span class="username">
                          <a href="#">@isset($milestoneInfo->milestoneuser->user){{$milestoneInfo->milestoneuser->user->name}} @endisset</a>
                        </span>
                    <span class="description">@isset($milestoneInfo->milestoneuser->user){{$milestoneInfo->milestoneuser->user->designation->name}} @endisset</span>
                  </div>
                  <div class="col-sm-6">
                  	<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update Milestone
                        </h4>
                  </div>
				<br>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<form action="{{ route('milestone.update', $milestoneInfo->id) }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
<br>
					<div class="input-group col-md-8">
						<span class="input-group-addon" id="basic-addon1"><b>KPI<span style="color:red;">*</span></b></span>
						<select data-placeholder="Choose a Kpi" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="kpi_id">
							<option value=""></option>
							@foreach($kpiList as $row)
							<option value="{{$row->id}}"
								@if($row->id == $milestoneInfo->kpi_id)
								selected
								@endif
								>{{$row->title }}</option>
								@endforeach 
							</select>
							
						</div>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('kpi_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('kpi_id') }}</span>
						<br>

						<fieldset style="border: solid 2px; border-color: #d8d8d8; padding: 10px; margin-bottom: 2px;">
							<div class="form-group">
								<label for="basic-addon1"><b>Name:<span style="color:red;">*</span></b></label>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('name'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('name') }}</span>
								<input type="text" class="form-control" placeholder="Milestone Name" name="name" id="basic-addon1" value="{{$milestoneInfo->name}}" autofocus>
							</div>
							
							<div class="form-group">
								<label for="basic-addon2">Description:<span style="color:red;">*</span></label>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('description'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description') }}</span>
								<textarea class="form-control" placeholder="Milestone Description" name="description" id="basic-addon2" value="" rows="3">{{$milestoneInfo->description}}</textarea>
							</div>
							
							<div class="form-group">
								<label for="number">Weight:<span style="color:red;">*</span></label>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('weight'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('weight') }}</span>
								<input type="number" min="0" class="form-control" placeholder="Milestone Weight" name="weight" id="number" value="{{$milestoneInfo->weight}}" autofocus>
							</div>
							
							<div class="form-group">
								<label for="basic-addon1">Start Date:<span style="color:red;">*</span></label>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('start_date'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('start_date') }}</span>
								<input type="date" class="form-control" placeholder="Milestone Start Date" name="start_date" id="basic-addon1" value="{{$milestoneInfo->start_date}}"  autofocus>
							</div>
							
							<div class="form-group">
								<span for="basic-addon1">End Date:<span style="color:red;">*</span></span>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('end_date'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('end_date') }}</span>
								<input type="date" class="form-control" placeholder="Milestone End Date" name="end_date" id="basic-addon1" value="{{$milestoneInfo->end_date}}" autofocus>
							</div>
							

						</fieldset>
						<!-- /.box-body -->
						<div class="box-footer">
							<a href="{{ route('milestone.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
							<button type="submit" class="btn btn-info pull-right" style="width: 100px;">Submit</button>
						</div>
						<!-- /.box-footer -->
					</form>
				</div>
			</div>
			<!-- /.box -->
		</div>

<div class="col-md-1"></div>
	</div>

	@endsection