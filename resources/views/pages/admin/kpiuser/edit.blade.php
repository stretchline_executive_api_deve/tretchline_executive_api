@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>

	<div class="col-md-10">
		
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">Update KPI User</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Update KPI User
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ route('kpiuser.update', $kpiUserInfo->id) }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}

					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">User: </span>
						<select data-placeholder="Choose a User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" required name="user_id">
							<option value=""></option>
							@foreach($userList as $row)
							<option value="{{$row->id}}"

								@if($kpiUserInfo->user_id == $row->id)
								selected
								@endif

								>{{$row->name }}</option>
								@endforeach 
							</select>
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2">Milestone: </span>
							<select data-placeholder="Choose a Milestone" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" required name="milestone_id">
								<option value=""></option>
								@foreach($milestoneList as $row)
								<option value="{{$row->id}}"

									@if($kpiUserInfo->milestone_id == $row->id)
									selected
									@endif

									>{{$row->name }}</option>
									@endforeach 
								</select>
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon3">KPI: </span>
								<select data-placeholder="Choose a KPI" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" required name="kpi_id">
									<option value=""></option>
									@foreach($kpiList as $row)
									<option value="{{$row->id}}"

										@if($kpiUserInfo->kpi_id == $row->id)
										selected
										@endif


										>{{$row->title }}</option>
										@endforeach 
									</select>
								</div>

								<br>
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon4">State:</span>
									<select class="form-control" aria-describedby="basic-addon4" style="width: 30%" required name="state">
										<option value="" disabled selected>Choose a State</option>
										<option value="Incomplete"
										@if($kpiUserInfo->state == 'Incomplete')
										selected
										@endif

										>Incomplete</option>
										<option value="Complete"
										@if($kpiUserInfo->state == 'Complete')
										selected
										@endif
										>Complete</option>
									</select>
								</div>
								<br>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" style="width: 100px;" class="btn btn-primary">Submit</button>
							</form>
						</div>
						<!-- /.box-footer -->
					</div>
					<!-- /.box -->
				</div>
<div class="col-md-1"></div>
			</div>

			@endsection