@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
        	<div class="box-header with-border">
		<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            KPI USER
                        </h4>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
           {{--  <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
              <i class="fa fa-times"></i></button> --}}
          </div>
        </div>
		{{-- <div class="pull-right">
			<a class="btn btn-success btn-sm disabled" href="{{ route('kpiuser.create')}}"> Add KPI User</a>
		</div> --}}
		<div class="box-body">
		<table class="table table-bordered" id="kpiUserTable" style="width: 100%">
			<thead>
				<tr>
					<th>#</th>
					<th>User</th>
					<th>Milestone</th>
					<th>KPI</th>
					<th>State</th>
					<th>Created</th>
					{{-- <th width="280px">Action</th> --}}
				</tr>
			</thead>
			@php $i=0 @endphp
			@foreach ($kpiUserInfo as $key => $kpiUserInfo)
			<tr>
				<td>{{ $kpiUserInfo->id }}</td>
				<td>{{ $kpiUserInfo->user->name }}</td>
				<td>{{ $kpiUserInfo->milestone->name }}</td>
				<td>{{ $kpiUserInfo->kpi->title }}</td>
				<td>
					@if($kpiUserInfo->state == 'Complete')
						<span class="label label-success">{{ $kpiUserInfo->state }}</span>
					@elseif($kpiUserInfo->state == 'Submitted')
						<span class="label label-warning">{{ $kpiUserInfo->state }}</span>
					@elseif($kpiUserInfo->state == 'Incomplete')
						<span class="label label-danger">{{ $kpiUserInfo->state }}</span>
					@endif

				</td>
				<td>{{ date_format($kpiUserInfo->created_at, 'd-m-Y') }}</td>
				{{-- <td style="width: 20%">
					<div class="col-md-6">
						<a class="btn btn-warning btn-block btn-xs disabled" href="{{ route('kpiuser.edit',$kpiUserInfo->id) }}">EDIT</a>
					</div>
					<div class="col-md-6">
						<form method="POST" action="{{ route('kpiuser.destroy', $kpiUserInfo->id) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-block btn-xs disabled" data-toggle="confirmation" data-placement="left" data-singleton="true">DELETE</button>
						</form>
					</div>
				</td> --}}
			</tr>
			@endforeach
		</table>
		<!-- /.box-body -->
        <div class="box-footer">
        	</div>
        <!-- /.box-footer-->
      </div>
	</div>
</div> {{-- row end --}}
</div>
@endsection
