@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">User Assign</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           User Assign
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ route('userlink.update', $userLinkInfo->id) }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					{{-- ============New UI=========== --}}
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						<!-- Widget: user widget style 1 -->
				          <div class="box box-widget widget-user">
				            <!-- Add the bg color to the header using any of the bg-* classes -->
				            <div class="widget-user-header bg-aqua-active">
				              <h3 class="widget-user-username">{{$userLinkInfo->user->name}}</h3>
				              <h5 class="widget-user-desc">{{$userLinkInfo->user->designation->name}}</h5>
				            </div>
				            <div class="widget-user-image">
				              <img class="img-circle" src="{{asset($userLinkInfo->user->profile_image)}}" alt="User Avatar">
				            </div>
				            <div class="box-footer">
				              <div class="row">
				                <div class="col-sm-4 border-right">
				                  <div class="description-block">
				                    <h5 class="description-header">SUPER USER</h5>
				                    <span class="description-text">
				                    	@if($userLinkInfo->superuser != null)
										{{$userLinkInfo->superuser->name}}
										@else
											Not Yet..
										@endif
				                    </span>
				                  </div>
				                  <!-- /.description-block -->
				                </div>
				                <!-- /.col -->
				                <div class="col-sm-8 border-right">
				                  <div class="description-block">
				                    <h5 class="description-header">SUB USERS</h5>
				                    <span class="description-text">
				                    	<select data-placeholder="Choose a Users" class="chosen-select" multiple tabindex="4" style="width: 100%" name="subuser_id[]" aria-describedby="basic-addon3">
										<option value=""></option>
										@isset($userList)
										@foreach($userList as $row)
										@if(in_array($row->id, $validUserList))
										<option value="{{$row->id}}"
											@if($subUserArray != null)
											@if(in_array($row->id, $subUserArray))
											selected 
											@endif
											@endif
											>{{$row->name }}</option>
										@endif
										@endforeach
										@endisset
										</select>
				                    </span>
				                  </div>
				                  <!-- /.description-block -->
				                </div>
				                <!-- /.col -->
				                {{-- <div class="col-sm-4">
				                  <div class="description-block">
				                    <h5 class="description-header">35</h5>
				                    <span class="description-text">PRODUCTS</span>
				                  </div>
				                  <!-- /.description-block -->
				          		</div> --}}
				          	</div>
				          </div>
				      </div></div>
					</div> {{-- end row --}}
					{{-- ===================== --}}
					{{-- =============================== --}}
					<div class="row">
						<div class="col-md-3">
						<h4 class="text-center">Super User</h4>
							<hr>
							<div class="input-group">
								<span>
									@if($userLinkInfo->superuser != null)
									{{$userLinkInfo->superuser->name}}
									@else
										--------
									@endif
								</span>
								{{-- <select data-placeholder="Choose a Super User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 100%" name="superuser_id">
									<option value=""></option>
									<option value="">No Super User</option>
									@isset($userList)
									@foreach($userList as $row)
									<option value="{{$row->id}}"
										@if($userLinkInfo != null)
										@if($userLinkInfo->superuser_id == $row->id)
										selected
										@endif
										@endif
										>{{$row->name }}</option>
									@endforeach
									@endisset
									</select> --}}
								</div>
 
							</div>
							<div class="col-md-6">
								<h4 class="text-center">Profile</h4>
								<hr>
								<img src="{{asset($userLinkInfo->user->profile_image)}}" class="img-thumbnail img-responsive center-block" width="20%" height="20%">

								<dl class="dl-horizontal">
									<dt>Name:</dt><dd>{{$userLinkInfo->user->name}}</dd>
									<dt>EPF Number:</dt><dd>{{$userLinkInfo->user->epf_no}}</dd>
									<dt>Phone:</dt><dd>{{$userLinkInfo->user->phone}}</dd>
								</dl>


							</div>
							<div class="col-md-3">
								<h4 class="text-center">Sub Users</h4>
								<hr>
								<div class="input-group" id="privateUsers">
									{{-- <span class="input-group-addon" id="basic-addon3">Users: </span> --}}
									<select data-placeholder="Choose a Users" class="chosen-select" multiple tabindex="4" style="width: 100%" name="subuser_id[]" aria-describedby="basic-addon3">
										<option value=""></option>
										@isset($userList)
										@foreach($userList as $row)
										@if(in_array($row->id, $validUserList))
										<option value="{{$row->id}}"
											@if($subUserArray != null)
											@if(in_array($row->id, $subUserArray))
											selected 
											@endif
											@endif
											>{{$row->name }}</option>
										@endif
										@endforeach
										@endisset
										</select>
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<a href="{{ route('users.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
							<button type="submit" style="width: 100px;" class="btn btn-info pull-right">Submit</button>
						</form>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /.box -->

			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
	@endsection