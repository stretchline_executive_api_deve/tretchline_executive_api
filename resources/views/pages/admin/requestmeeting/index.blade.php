@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">

<div class="row">
	<div class="col-md-12">
		{{-- <h3>Request Meeting</h3> --}}
		<div class="box box-info">
			<div class="box-header with-border">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           Request Meeting
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
		<div class="pull-right">
			{{-- <a class="btn btn-success btn-sm" href="{{ route('grade.create')}}"> Add Grade</a> --}}
		</div>
		{{-- {{ dd($requestInfo)}} --}}
		<table class="table table-bordered" id="requestMeetingTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Requested</th>
					<th>Milestone ID</th>
					<th>Title</th>
					<th>Message</th>
					<th>Date</th>
					<th>Sender ID</th>
					{{-- <th>Receiver</th> --}}
					{{-- <th>State</th> --}}
					<th width="280px">Action</th>
				</tr>
			</thead>
			@php $i=0 @endphp
			@foreach ($requestInfo as $key => $requestInfo)
			<tr>
				<td>{{ $requestInfo->requestMeeting->id }}</td>
				<td>{{ $requestInfo->requestMeeting->user_id }}</td>
				<td>{{ $requestInfo->requestMeeting->milestone_id }}</td>
				<td>{{ $requestInfo->requestMeeting->title }}</td>
				<td>{{ $requestInfo->requestMeeting->message }}</td>
				<td>{{ $requestInfo->requestMeeting->meeting_date }}</td>
				<td>{{ $requestInfo->requestMeeting->user_id }}</td>
				{{-- <td>{{ $requestInfo->receiver_id }}</td> --}}
				{{-- <td>{{ $requestInfo->state }}</td> --}}
				{{-- <td>{{ date_f;ormat($gradeInfo->created_at, 'd-m-Y') }}</td> --}}
				<td style="width: 5%">
						<form method="POST" action="{{ route('request_meeting.destroy', $requestInfo->requestMeeting->id) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-xs center-block" data-toggle="confirmation" data-placement="left" data-singleton="true">DELETE</button>
						</form>
				</td>
			</tr>
			@endforeach
		</table>
		</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}
</div>
@endsection
