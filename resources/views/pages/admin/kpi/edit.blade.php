@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>

	<div class="col-md-10">
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">KPI Update</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           KPI Update
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ route('kpi.update', $kpiInfo->id) }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}

					<div class="input-group col-md-6">
						<span class="input-group-addon" id="basic-addon1">User:<span style="color:red;">*</span></span>
						<select data-placeholder="Choose a User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="user_id">
							<option value=""></option>
							@foreach($userList as $row)
							<option value="{{$row->id}}"
								@if($kpiUser != null)
								@if($row->id == $kpiUser->user_id)
								selected
								@endif 
								@endif
								>{{$row->name }}</option>
								@endforeach 
							</select>
							
						</div>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
						<br>

						<div class="form-group">
							<label for="basic-addon1">Title:<span style="color:red;">*</span></label>
							<span style="color: red">&nbsp;&nbsp;@if($errors->first('title'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('title') }}</span>
								<input type="text" class="form-control" placeholder="KPI Title" name="title" id="basic-addon1" value="{{ $kpiInfo->title }}" autofocus required>
							</div>
							<br>
							<div class="form-group">
								<label for="basic-addon2">Description:</label>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('description'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description') }}</span>
									<textarea class="form-control" placeholder="KPI Description" name="description" id="basic-addon2" value="{{ $kpiInfo->description }}" rows="3">{{ $kpiInfo->description }}</textarea>
								</div>
								<br>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
							<a href="{{ route('kpi.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
							<button type="submit" style="width: 100px;" class="btn btn-info pull-right">Submit</button>
							</form>
						</div>
						<!-- /.box-footer -->
					</div>
					<!-- /.box -->
				</div>

<div class="col-md-1"></div>
			</div>

			@endsection