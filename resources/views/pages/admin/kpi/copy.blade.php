@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>

	<div class="col-md-10">
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">KPI Update</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           Create KPI & Milestones from Previous
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form action="{{ url('/admin/copy_kpi_milestone') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
					{{ csrf_field() }}

					<div class="input-group col-md-6">
						<span class="input-group-addon" id="basic-addon1">User:<span style="color:red;">*</span></span>
						<select data-placeholder="Choose a User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="user_id">
							<option value=""></option>
							@foreach($userList as $row)
							<option value="{{$row->id}}"
								@if($kpiUser != null)
								@if($row->id == $kpiUser->user_id)
								
								@endif 
								@endif
								>{{$row->name }}</option>
								@endforeach 
							</select>
						</div>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
						<h3 style="text-align: center;">KPI</h3>
						<div class="form-group">
							<label for="basic-addon1">Title:<span style="color:red;">*</span></label>
							<span style="color: red">&nbsp;&nbsp;@if($errors->first('title'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('title') }}</span>
								<input type="text" class="form-control" placeholder="KPI Title" name="title" id="basic-addon1" value="{{ $kpiInfo->title }}" autofocus required>
							</div>
							<br>
							<div class="form-group">
								<label for="basic-addon2">Description:<span style="color:red;">*</span></label>
								<span style="color: red">&nbsp;&nbsp;@if($errors->first('kpidescription'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('kpidescription') }}</span>
									<textarea class="form-control" placeholder="KPI Description" name="kpidescription" id="basic-addon2" value="{{ $kpiInfo->description }}" rows="3" required>{{ $kpiInfo->description }}</textarea>
								</div>
							

								<h3 style="text-align: center;">Milestones</h3>
		@foreach($milestoneByKpi as $milestoneByKpis)
			<div id="sections">
				<div class="section">
				<fieldset style="border: solid 2px; border-color: #d8d8d8; padding: 10px; margin-bottom: 2px;">
				<button class="remove pull-right btn btn-danger" style="display: none;">x</button>
				{{-- <legend>Personalia:</legend> --}}
					<div class="form-group">
						<label for="basic-addon1">Name:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('name.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('name.0') }}</span>
						<input type="text" class="form-control" placeholder="Milestone Name" name="name[]" id="basic-addon1" value="{{$milestoneByKpis->milestone->name}}" autofocus required>
					</div>
					<div class="form-group">
						<label for="basic-addon2">Description:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('description.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description.0') }}</span>
						<textarea class="form-control" placeholder="Milestone Description" name="description[]" id="basic-addon2" value="" rows="3" required>{{$milestoneByKpis->milestone->description}}</textarea>
					</div>
					<div class="form-group">
						<label for="number">Weight:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('weight.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('weight.0') }}</span>
						<input type="number" min="0" class="form-control" placeholder="Milestone Weight" name="weight[]" id="number" value="{{$milestoneByKpis->milestone->weight}}" autofocus required>
					</div>
					<div class="form-inline">
					<div class="form-group">
						<label for="date-basic-addon13">Start Date:<span style="color:red;">*</span></label>
						<input type="text" class="form-control date-basic-addon13" placeholder="Milestone Start Date" name="start_date[]" id="date-basic-addon13" value="{{$milestoneByKpis->milestone->start_date}}" autofocus required>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('start_date.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('start_date.0') }}</span>
					</div>
					<div class="form-group">
						<label for="date-basic-addon14">&nbsp;&nbsp;End Date:<span style="color:red;">*</span></label>
						<input type="text" class="form-control date-basic-addon14" placeholder="Milestone End Date" name="end_date[]" id="date-basic-addon14" value="{{$milestoneByKpis->milestone->end_date}}" autofocus required>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('end_date.0'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('end_date.0') }}</span>
					</div>
					</div>
					</fieldset>
				</div>
			</div>
		@endforeach
</div>

							<!-- /.box-body -->
							<div class="box-footer">
							{{-- <a href="{{ route('kpi.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
							<button type="submit" style="width: 100px;" class="btn btn-info pull-right">Submit</button> --}}
							<a href="{{ route('kpi.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
							<button type="submit" name="copy_kpi" style="width: 100px;" class="btn btn-info pull-right">Submit</button>
							{{-- <button type="submit" name="add_milestone" style="width: 150px;" class="btn btn-default pull-right">Submit & Milestone&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button> --}}
							</form>
						</div>
						<!-- /.box-footer -->
					</div>
					<!-- /.box -->
				</div>

<div class="col-md-1"></div>
			</div>

			@endsection