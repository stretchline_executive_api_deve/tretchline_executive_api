@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>
	<div class="col-md-10">
		<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">Create KPI</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           Create KPI
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<form action="{{ route('kpi.store') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
					{{ csrf_field() }}

					<div class="input-group col-md-6">
						<span class="input-group-addon" id="basic-addon1"><b>User:<span style="color:red;">*</span></b></span>
						<select data-placeholder="Choose a User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="user_id">
							<option value=""></option>
							@foreach($userList as $row)
							<option value="{{$row->id}}" @if (old('user_id') == $row->id) selected @endif>{{$row->name }}</option>
							@endforeach 
						</select>
					</div>
					<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
					<br>
					<div class="form-group">
						<label for="title">Title:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('title'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('title') }}</span>
						<input type="text" class="form-control" id="title" placeholder="KPI Title" name="title" value="{{ old('title') }}" autofocus required>
					</div>
					<br>
					<div class="form-group">
						<label for="description">Description:<span style="color:red;">*</span></label>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('description'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description') }}</span>
						<textarea class="form-control" id="description" placeholder="KPI Description" name="description" value="{{ old('description') }}" rows="3">{{old('description')}}</textarea>
					</div>
					<br>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
				<a href="{{ route('kpi.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
				<button type="submit" style="width: 100px;" class="btn btn-info">Submit</button>
				<button type="submit" name="add_milestone" style="width: 150px;" class="btn btn-default pull-right">Submit & Milestone&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
				{{-- <a  class="btn btn-default pull-right" href="{{ route('milestone.create') }}">Milestones &nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></a> --}}
				</div>
				<!-- /.box-footer -->
			</form>
		</div>
		<!-- /.box -->
	</div>
<div class="col-md-1"></div>
</div>

@endsection 