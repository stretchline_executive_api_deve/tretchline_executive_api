@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
@if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif
{{-- End: Error message handle--}}
<div class="container-fluid">

<div class="row">
	<div class="col-md-12">
		{{-- <h3>KPI</h3> --}}
		
		<div class="box box-info">
			<div class="box-header with-border">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            KPI
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
		<div class="pull-right">
			<a class="btn btn-success btn-sm" href="{{ route('kpi.create')}}"> Add Kpi</a>
		</div>
		<table class="table table-hover display nowrap" id="kpiTable"  cellspacing="0" style="width: 100%;">
			<thead>
				<tr>
					<th><i class="fa fa-expand" aria-hidden="true"></i></th>
					<th>User</th>
					<th>Title</th>
					<th>Description:</th>
					<th>Overall Progress</th>
					<th>Created:</th>
					<th width="280px">Action:</th>
				</tr>
			</thead>
			@php $i=0; @endphp
			@foreach ($byKpi as $key => $kpiInfo)
			@php ++$i; @endphp
			<tr>
				<td></td>
				<td>{{$kpiInfo->kpi_user}}</td>
				<td>{{ $kpiInfo->title }}</td>
				<td>{{ $kpiInfo->description }}</td>
				<td style="width: 15%">
								<div class="progress progress-xs progress-striped active" style="height: 18px;border-radius: 9px;">
			                      <div class="progress-bar progress-bar-info" style="width: {{$kpiInfo->kpiState}}%"><font color="#800080">{{$kpiInfo->kpiState}}%</font></div>
			                    </div>
				</td>
				<td>{{ date_format($kpiInfo->created_at, 'd-m-Y') }}</td>
				<td style="width: 20%">
					{{-- <div class="col-md-2"></div> --}}
					<div class="row" style="width: 300px;">
					<div class="col-md-4">
						<a class="btn btn-warning btn-xs" href="{{ route('kpi.edit',$kpiInfo->id) }}" style="width: 50px;">EDIT</a>
					</div>
					<div class="col-md-4">
						<a class="btn btn-primary btn-xs" href="{{ route('kpi.edit',$kpiInfo->id) }}?copy" style="width: 50px;">COPY</a>
					</div>
					<div class="col-md-4">
						<form method="POST" action="{{ route('kpi.destroy', $kpiInfo->id) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-xs" data-toggle="confirmation" data-placement="left" data-singleton="true" style="width: 50px;">DELETE</button>
						</form>
					</div>
					{{-- <div class="col-md-2"></div> --}}
					</div>
				</td>
			</tr>
			@endforeach
		</table>
		</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}
</div>
@endsection
