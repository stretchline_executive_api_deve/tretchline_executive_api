@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif 
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
	{{-- <h3>USER ASSIGN</h3> --}}
	<div class="box box-info">
			<div class="box-header with-border">
				{{-- <h3 class="box-title">Update Grade</h3> --}}
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           USER ASSIGN
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
	<form method="GET" action="{{ route('userlink.edit', 2) }}" accept-charset="UTF-8">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-6" style="padding-right: 0px;">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">User: </span>
					<select data-placeholder="Choose a User" class="chosen-select" tabindex="2" aria-describedby="basic-addon1" style="width: 300px" name="user_id">
						<option value=""></option> 
						@foreach($userOnly as $row)
						<option value="{{$row->id}}">{{$row->name }}</option>
						@endforeach 
					</select>
					
				</div>
			</div>
			<div class="col-md-1" style="padding-left: 0px;">
					<button type="submit" class="btn btn-info" style="width: 100px;">View</button>
					<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id') }}</span>
				</div>
		</div>
		
			</form>
			</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		{{-- <h3>USERS</h3> --}}
		<div class="box box-info">
			<div class="box-header with-border">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           USERS
                        </h4>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
		<div class="pull-right">
			<a class="btn btn-success btn-sm" href="{{ route('users.create')}}"> Add User</a>
		</div>
		<table class="table table-bordered table-responsive" id="usersTable" style="width: 100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Pro. Image</th>
					<th>Is Admin</th>
					<th>Phone</th>
					<th>EPF</th>
					<th>Grade</th>
					<th>Designation</th>
					<th>Department</th>
					<th>Email</th>
					{{-- <th>Created</th> --}}
					<th width="280px" data-priority="1">Action</th>
				</tr>
			</thead>
			@php $i=0 @endphp
			@foreach ($usersInfo as $key => $usersInfo)
			<tr>
				<td>{{ $usersInfo->id }}</td>
				<td>{{ $usersInfo->name }}</td>
				<td><img src="{{asset($usersInfo->profile_image)}}" class="img-thumbnail img-responsive center-block"></td>
				<td>
					@if($usersInfo->is_admin == 0) USER @endif
					@if($usersInfo->is_admin == 1) ADMIN @endif
				</td>
				<td>{{ $usersInfo->phone }}</td>
				<td>{{ $usersInfo->epf_no }}</td>
				<td>{{ $usersInfo->grade->name }}</td>
				<td>{{ $usersInfo->designation->name }}</td>
				<td>{{ $usersInfo->department->name }}</td>
				<td>{{ $usersInfo->email }}</td>
				{{-- <td>{{ date_format($usersInfo->created_at, 'd-m-Y') }}</td> --}}
				<td style="width: 10%">
					{{-- <div class="col-md-2"></div> --}}
					{{-- <div class="col-md-6"> --}}
					<div class="btn-group">
						
					
						<a class="btn btn-warning btn-xs" style="width: 100px;" href="{{ route('users.edit',$usersInfo->id) }}" role="button">EDIT</a>
					{{-- </div> --}}
					{{-- <div class="col-md-6"> --}}
						<form method="POST" action="{{ route('users.destroy', $usersInfo->id) }}" accept-charset="UTF-8">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger btn-xs" style="width: 100px;" data-toggle="confirmation" data-placement="left" data-singleton="true">DELETE</button>
						</form>
					</div>
					{{-- </div> --}}
					{{-- <div class="col-md-2"></div> --}}
				</td>
			</tr>
			@endforeach
		</table>
		</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div> {{-- row end --}}
</div>
@endsection
