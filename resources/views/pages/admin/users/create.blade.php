@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
{{-- Validation errors handle --}}
{{-- @if ($errors->first())
<div class="row">
	<div class="container col-md-6 col-sm-6">
		<div class="alert alert-danger" role="alert">
			<p>{{ $errors->first() }}</p>
		</div>
	</div>
</div>
@endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>
	<div class="col-md-10">

		<div class="box box-info">
	<div class="box-header with-border">
		{{-- <h3 class="box-title">Create KPI User</h3> --}}
		<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                           CREATE USER
                        </h4>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<form action="{{ route('users.store') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
			{{ csrf_field() }}

			<br>
			<div class="form-group">
				<label for="basic-addon1">Name:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('name'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('name') }}</span>
				<input type="text" class="form-control" placeholder="User Name" name="name" id="basic-addon1" value="{{ old('name') }}">
			</div>
			<br>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon2"><b>User Type:<span style="color:red;">*</span></b></span>
				<select class="form-control" aria-describedby="basic-addon2" style="width: 30% !important;" name="is_admin">
					<option value="" disabled selected>Choose a User Type</option>
					<option value="1" @if (old('is_admin') == '1') selected @endif>Admin</option>
					<option value="0" @if (old('is_admin') == '0') selected @endif}}>User</option>
				</select>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('is_admin'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('is_admin') }}</span>
			</div>
			<br>
			<div class="form-group">
				<label for="number">Phone:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('phone'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('phone') }}</span>
				<input type="number" min="0" class="form-control" placeholder="User Phone" name="phone" id="number" value="{{ old('phone') }}" required>
			</div>
			<br>
			<div class="form-group">
				<label id="basic-addon4">EPF No:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('epf_no'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('epf_no') }}</span>
				<input type="text" class="form-control" placeholder="User EPF Number" name="epf_no" id="basic-addon4" value="{{ old('epf_no') }}" required>
			</div>
			<br>
			<div class="input-group col-md-6">
				<span class="input-group-addon" id="basic-addon6"><b>Grade:<span style="color:red;">*</span></b></span>
				<select data-placeholder="Choose a Grade" class="chosen-select" tabindex="2" aria-describedby="basic-addon6" style="width: 30% !important;" name="grade_id">
					<option value=""></option>
					@foreach($gradeList as $row)
					<option value="{{$row->id}}" @if (old('grade_id') == $row->id) selected @endif>{{$row->name }}</option>
					@endforeach 
				</select>
				
			</div>
			<span style="color: red">&nbsp;&nbsp;@if($errors->first('grade_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('grade_id') }}</span>
			<br>
			<div class="input-group col-md-6">
				<span class="input-group-addon" id="basic-addon7"><b>Designation:<span style="color:red;">*</span></b></span>
				<select data-placeholder="Choose a Designation" class="chosen-select" tabindex="2" aria-describedby="basic-addon7" style="width: 300px" name="designation_id">
					<option value=""></option>
					@foreach($designationList as $row)
					<option value="{{$row->id}}" @if (old('designation_id') == $row->id) selected @endif>{{$row->name }}</option>
					@endforeach 
				</select>
				
			</div>
			<span style="color: red">&nbsp;&nbsp;@if($errors->first('designation_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('designation_id') }}</span>
			<br>
			<div class="input-group col-md-6">
				<span class="input-group-addon" id="basic-addon8"><b>Department:<span style="color:red;">*</span></b></span>
				<select data-placeholder="Choose a Departmant" class="chosen-select" tabindex="2" aria-describedby="basic-addon8" style="width: 300px" name="department_id">
					<option value=""></option>
					@foreach($departmentList as $row)
					<option value="{{$row->id}}" @if (old('department_id') == $row->id) selected @endif>{{$row->name }}</option>
					@endforeach 
				</select>
				
			</div>
			<span style="color: red">&nbsp;&nbsp;@if($errors->first('department_id'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('department_id') }}</span>
			<br>

			<div class="form-group">
				<label for="basic-addon5">Email:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('email'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('email') }}</span>
				<input type="email" class="form-control" placeholder="Email" name="email" id="basic-addon5" value="{{ old('email') }}" required>
			</div>
			<br>
			<div class="form-group">
				<label for="basic-addon11">Outlook Mail:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('gmail'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('gmail') }}</span>
				<input type="email" class="form-control" placeholder="Any outlook|hotmail|live|windowslive|msn|microsoft Address" name="gmail" id="basic-addon11" value="{{ old('gmail') }}" required>
			</div>
			<br>
			<div class="form-group">
				<label for="basic-addon9">Password:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('password'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('password') }}</span>
				<input type="password" class="form-control" placeholder="Password" name="password" id="basic-addon9" value="{{str_random(6)}}" readonly>
			</div>
			<br>
			<div class="form-group">
				<label for="basic-addon10">Profile Image:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('profile_image'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('profile_image') }}</span>
				<input type="file" class="form-control" placeholder="Profile Image" name="profile_image" id="basic-addon10" value="{{ old('profile_image') }}">
			</div>
			<br>
			</div>
	<!-- /.box-body -->
	<div class="box-footer">
	<a href="{{ route('users.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
			<button type="submit" style="width: 100px;" class="btn btn-info pull-right">Submit</button>
		</form>
		</div>
	<!-- /.box-footer -->
</div>
<!-- /.box -->
	</div>
	<div class="col-md-1"></div>
</div>

@endsection