@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
    {{-- Validation errors handle --}}
{{--     @if ($errors->first())
    <div class="row">
        <div class="container col-md-6 col-sm-6">
            <div class="alert alert-danger" role="alert">
                <p>{{ $errors->first() }}</p>
            </div>
        </div>
    </div>
    @endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>
	<div class="col-md-10">
<div class="box box-info">
	<div class="box-header with-border">
		{{-- <h3 class="box-title">Create Department</h3> --}}
		<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            CREATE DEPARTMENT
                        </h4>
	</div>
	<div class="box-body">
		<form action="{{ route('department.store') }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="basic-addon1">Name:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('name'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('name') }}</span>
				<input class="form-control" type="text" placeholder="Department Name" name="name" id="basic-addon1" value="{{ old('name') }}" autofocus required>
			</div>
			<br>
			<div class="form-group">
				<label id="basic-addon2">Description:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('description'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description') }}</span>
				<textarea class="form-control" placeholder="Department Description" name="description" id="basic-addon2" value="{{ old('description') }}" rows="3" required></textarea>
			</div>
			<br>
			</div>
	<!-- /.box-body -->
	<div class="box-footer">
			<a href="{{ route('department.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
			<button type="submit" style="width: 100px;" class="btn btn-info pull-right">Submit</button>
		</form>
	</div>
	<!-- /.box-footer -->
</div>
<!-- /.box -->
	</div>
<div class="col-md-1"></div>
</div>

@endsection