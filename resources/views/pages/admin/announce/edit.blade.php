@extends('layouts.app')

@section('main-content')
{{-- Error message handle --}}
@if ($message = Session::get('success'))
<div class="container col-md-8">
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
@if ($message = Session::get('error'))
<div class="container">
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>{{ $message }}</p>
	</div>
</div>
@endif
    {{-- Validation errors handle --}}
{{--     @if ($errors->first())
    <div class="row">
        <div class="container col-md-6 col-sm-6">
            <div class="alert alert-danger" role="alert">
                <p>{{ $errors->first() }}</p>
            </div>
        </div>
    </div>
    @endif --}}
{{-- End: Error message handle--}}
<div class="container-fluid">
<div class="col-md-1"></div>
	<div class="col-md-10">
<div class="box box-info">
	<div class="box-header with-border">
		{{-- <h3 class="box-title">Update Announcements</h3> --}}
		<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            UPDATE ANNOUNCEMENT
                        </h4>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<form action="{{ route('announce.update', $announceInfo->id) }}" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}

			<div class="form-group">
				<label for="basic-addon1">Title:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('title'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('title') }}</span>
				<input type="text" class="form-control" placeholder="Announcement Title" name="title" id="basic-addon1" value="{{ $announceInfo->title }}" autofocus required>
			</div>
			<br>
			<div class="form-group">
				<label for="basic-addon2">Description:<span style="color:red;">*</span></label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('description'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('description') }}</span>
				<textarea class="form-control" placeholder="Announcement Description" name="description" id="basic-addon2" rows="3" required>{{ $announceInfo->description }}</textarea>
			</div>
			<div class="form-inline">
					<div class="form-group">
						<label for="date-basic-addon13">Start Date:<span style="color:red;">*</span></label>
						<input type="text" class="form-control date-basic-addon13" placeholder="Announce appear from" name="start_date" id="date-basic-addon13" value="{{$announceInfo->start_date}}" autofocus>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('start_date'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('start_date') }}</span>
					</div>
					<div class="form-group">
						<label for="date-basic-addon14">&nbsp;&nbsp;End Date:<span style="color:red;">*</span></label>
						<input type="text" class="form-control date-basic-addon14" placeholder="Announce appear expire" name="end_date" id="date-basic-addon14" value="{{$announceInfo->end_date}}" autofocus>
						<span style="color: red">&nbsp;&nbsp;@if($errors->first('end_date'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('end_date') }}</span>
					</div>
					</div>
			<br>
			<div class="form-group">
				<label for="basic-addon2">Announcement Type:&nbsp;&nbsp;</label>
				<label class="radio-inline" for="basic-addon2">
				<input type="radio" id="public" name="user_type" value="0" onclick="hide();"
				@if($announceInfo->user_type == '0')
						checked 
				@endif
				>Public
				</label>
				<label class="radio-inline" for="basic-addon2">
					<input type="radio" id="private" name="user_type" value="1" onclick="show();"
					@if($announceInfo->user_type == '1')
						checked 
					@endif
					>Private
				</label>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_type'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_type') }}</span>
			</div>
			<br>
			<div class="input-group" 
			@if($announceInfo->user_type == '1')
			style="visibility: visible"
			@elseif($announceInfo->user_type == '0')
			style="visibility: hidden"
			@endif
			id="privateUsers">
				<span class="input-group-addon" id="basic-addon3">Users: </span>
				 <select data-placeholder="Choose a Users" class="chosen-select" multiple tabindex="4" style="width: 300px" name="user_id[]" aria-describedby="basic-addon3">
					<option value=""></option>
					@foreach($userList as $row)
					<option value="{{$row->id}}"
					@if(in_array($row->id, $announceUserInfo))
						selected 
					@endif
					>{{$row->name }}</option>
					@endforeach 
				</select>
				<span style="color: red">&nbsp;&nbsp;@if($errors->first('user_id[]'))<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>@endif{{ $errors->first('user_id[]') }}</span>
			</div>
			<br>
			</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<a href="{{ route('announce.index')}}" style="width: 100px;" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
			<button type="submit" style="width: 100px;" class="btn btn-info pull-right">Submit</button>
		</form>
	</div>
	<!-- /.box-footer -->
	</div>
<!-- /.box -->
	</div>

<div class="col-md-1"></div>
</div>
{{-- =====Java script for select private users==== --}}
<script type="text/javascript">
        function show() { document.getElementById('privateUsers').style.visibility = 'visible'; }
        function hide() { document.getElementById('privateUsers').style.visibility = 'hidden'; }
      </script>
{{-- ===end=== --}}
@endsection