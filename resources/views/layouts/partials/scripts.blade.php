
<!-- jQuery 2.2.3 -->
{{-- <!-- <script src="{{ asset('/plugins/jQuery/jquery-2.2.3.min.js')}}"></script> --> --}}
<!-- jQuery UI 1.11.4 -->
 {{-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"  crossorigin="anonymous"></script> --}}
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
{{-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> --}}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  // $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
{{-- <script src="{{ asset('/js/bootstrap.min.js')}}"></script> --}}
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Chosen Version 1.7.0 -->
{{-- <script src="{{ asset('/js/chosen.jquery.min.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.jquery.js"></script>
<!-- Chosen Proto Version 1.7.0 -->
{{-- <script src="{{ asset('/js/chosen.proto.min.js')}}"></script> --}}
<!-- Miltiple Fields JS -->
{{-- <script src="{{ asset('/js/jquery.multifield.min.js')}}"></script> --}}
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
{{-- <script src="{{ asset('/plugins/morris/morris.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js" integrity="sha256-0rg2VtfJo3VUij/UY9X0HJP7NET6tgAY98aMOfwP0P8=" crossorigin="anonymous"></script>
<!-- Sparkline -->
{{-- <script src="{{ asset('/plugins/sparkline/jquery.sparkline.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js" integrity="sha256-BuAkLaFyq4WYXbN3TFSsG1M5GltEeFehAMURi4KBpUM=" crossorigin="anonymous"></script>
<!-- jvectormap -->
{{-- <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.min.js" integrity="sha256-u7YP3pXtcXxu/QlDbQ4gFAOqCBau7SIJLLaSNsaqbRs=" crossorigin="anonymous"></script>
<script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
{{-- <script src="{{ asset('/plugins/knob/jquery.knob.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js" integrity="sha256-2144q+NOM/XU6ZxSqRTJ8P0W/CkY6zXc6mXYt4+mF9s=" crossorigin="anonymous"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/moment.min.js" integrity="sha256-TkEcmf5KSG2zToAaUzkq6G+GWezMQ4lEtaBiyaq6Jb4=" crossorigin="anonymous"></script>
{{-- <script src="{{ asset('/plugins/daterangepicker/daterangepicker.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js" integrity="sha256-WniDtKHyk2OPgNtTFKu/sL8zGBzmCfMcmEKRAqfi25c=" crossorigin="anonymous"></script>
<!-- datepicker -->
{{-- <script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0/js/bootstrap-datepicker.min.js" integrity="sha256-FOV0q1Ks/eXoUwtkcN6OxWV4u9OSq7LDomNYnfF/0Ys=" crossorigin="anonymous"></script>
<!-- Bootstrap WYSIHTML5 -->
{{-- <script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/amd/bootstrap3-wysihtml5.all.min.js" integrity="sha256-vmFC/1+WhM3B4T3wZ6kISZX/ec4yWxIDb15cdAbekqU=" crossorigin="anonymous"></script>
<!-- Slimscroll -->
{{-- <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js" integrity="sha256-qE/6vdSYzQu9lgosKxhFplETvWvqAAlmAuR+yPh/0SI=" crossorigin="anonymous"></script>
<!-- FastClick -->
{{-- <script src="{{ asset('/plugins/fastclick/fastclick.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js" integrity="sha256-t6SrqvTQmKoGgi5LOl0AUy+lBRtIvEJ+++pLAsfAjWs=" crossorigin="anonymous"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js" integrity="sha256-j007R7R6ijEWPa1df7FeJ6AFbQeww0xgif2SJWZOhHw=" crossorigin="anonymous"></script>============ --}}
{{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.15/r-2.1.1/datatables.min.js"></script> --}}
{{-- //data tables - with export buttons --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/b-1.4.2/b-colvis-1.4.2/b-flash-1.4.2/b-html5-1.4.2/b-print-1.4.2/kt-2.3.2/r-2.2.0/datatables.min.js"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/dataTables.bootstrap.min.js" integrity="sha256-X/58s5WblGMAw9SpDtqnV8dLRNCawsyGwNqnZD0Je/s=" crossorigin="anonymous"></script> --}}
<!-- Data Table Responsive JS -->
{{-- <script src="{{ asset('/js/dataTables.responsive.min.js')}}"></script> --}}
{{-- <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>=============== --}}
<!-- Bootstrap Confirmation JS -->
{{-- <script src="{{ asset('/js/bootstrap-confirmation.min.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/mistic100-Bootstrap-Confirmation/2.4.0/bootstrap-confirmation.min.js" integrity="sha256-HJIr5giC2AeO3oRU5F/tMHbrhwSJZTY5IYbHaMikBHY=" crossorigin="anonymous"></script>
<!-- Custom JS -->
{{-- <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
<script src="{{ asset('/js/custom.js')}}"></script>
{{-- <script src="{{ asset('/js/ajaxnotify.js')}}"></script> --}}
<script src="https://rawgit.com/notifyjs/notifyjs/master/dist/notify.js"></script>
{{-- <script>
    $(".selector2").datepicker({ dateFormat: 'yy-mm-dd' });
    $(".selector").datepicker({ dateFormat: 'yy-mm-dd' });
</script> --}}

<script >





    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#button-add-more"); //Add button ID

    var x = 1; //initlal text box count



    $(add_button).click(function(e){ //on add input button click
        // alert('ads');
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment



            var body_html = '<div class="panel panel-default"><a href="#" class="pull-right remove_field"><i class="fa fa-times" aria-hidden="true"></i></a> <div class="panel-heading"> Create Milestone for KPI </div> <div class="panel-body">  <div class="form-group">   <label for="text">Milestone name:</label>  <input type="text" class="form-control"  name="name[]"> </div>  <div class="form-group">   <label for="text">Milestone description:</label>  <input type="text" class="form-control"  name="description[]"> </div>   <div class="form-group"> <label for="number">weight:</label>  <input type="number" class="form-control"  name="weight[]"> </div> <div class="form-group"> <label for="text">start date:</label>  <input type="text" class="form-control sdatepick'+x+'"  name="start_date[]">      </div> <div class="form-group"><label for="text">End date:</label>  <input type="text" class="form-control edatepick'+x+'"  name="end_date[]"> </div> </div></div> ';

            $(wrapper).append(body_html); //add input box

              $(".sdatepick"+x).datepicker({ dateFormat: 'yy-mm-dd' });
             $(".edatepick"+x).datepicker({ dateFormat: 'yy-mm-dd' });

        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })



$(document).ready(function(){
    $('#saveandcontinu').attr('disabled',true);
    $('.description, .title').bind('keyup', function(){
        if($('.description').val().length !=0 && $('.title').val().length !=0 && $('.kpi-select').val().length !=0 )
            $('#saveandcontinu').attr('disabled', false);
        else
            $('#saveandcontinu').attr('disabled',true);
    })
    $('.kpi-select').change(function(){
            $('#saveandcontinu').attr('disabled', false);

    })
});

</script>
