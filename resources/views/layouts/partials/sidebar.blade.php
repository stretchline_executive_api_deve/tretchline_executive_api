<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar bar-custom">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
{{--         @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset(Auth::user()->profile_image)}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> online</a>
                </div>
            </div>
        @endif --}}

        <!-- search form (Optional) -->
       
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"></li>
            <!-- Optionally, you can add icons to the links -->
            {{-- <li class="{{ Request::path() == 'admin/home' ? 'active' : '' }}"><a href="{{ url('admin/home') }}"><i class='fa fa-link'></i> <span>Home</span></a></li> --}}
            <li class="{{ Request::path() == 'admin/home' ? 'active' : '' }}"><a href="{{ url('admin/home') }}"><img src="{{ asset('img/sidebar/dashboard.png')}}" class="img-responsive sidebar-icon"> <span>DASHBOARD</span></a></li>
            <li class="{{ Request::path() == 'admin/users' ? 'active' : '' }}"><a href="{{ url('admin/users') }}"><img src="{{ asset('img/sidebar/user.png')}}" class="img-responsive sidebar-icon"> <span>USERS</span></a></li>

            <li class="{{ Request::path() == 'admin/kpi' ? 'active' : '' }}"><a href="{{ url('admin/kpi') }}"><img src="{{ asset('img/sidebar/KPI.png')}}" class="img-responsive sidebar-icon"> <span>KPI</span></a></li>

            <li class="{{ Request::path() == 'admin/milestone' ? 'active' : '' }}"><a href="{{ url('admin/milestone') }}"><img src="{{ asset('img/sidebar/Milestone.png')}}" class="img-responsive sidebar-icon"> <span>MILESTONES</span></a></li>

            <li class="{{ Request::path() == 'admin/kpiuser' ? 'active' : '' }}"><a href="{{ url('admin/kpiuser') }}"><img src="{{ asset('img/sidebar/user_milestone.png')}}" class="img-responsive sidebar-icon"> <span>MILESTONES BY USER</span></a></li>

       {{--      <li class="treeview">
                <a href="#"><img src="{{ asset('img/sidebar/kpi_milestone.png')}}" class="img-responsive sidebar-icon"></i> <span>KPI & MILESTONE</span> <i class="fa fa-angle-right pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::path() == 'admin/kpi' ? 'active' : '' }}"><a href="{{ url('admin/kpi') }}">KPI</a></li>
                    <li class="{{ Request::path() == 'admin/milestone' ? 'active' : '' }}"><a href="{{ url('admin/milestone') }}">Milestone</a></li>
                    <li class="{{ Request::path() == 'admin/kpiuser' ? 'active' : '' }}"><a href="{{ url('admin/kpiuser') }}">KPI & Milestone by User</a></li>
                </ul>
            </li> --}}

            <li class="{{ Request::path() == 'admin/department' ? 'active' : '' }}"><a href="{{ url('admin/department') }}"><img src="{{ asset('img/sidebar/department.png')}}" class="img-responsive sidebar-icon"></i> <span>DEPARTMENTS</span></a></li>
            <li class="{{ Request::path() == 'admin/grade' ? 'active' : '' }}"><a href="{{ url('admin/grade') }}"><img src="{{ asset('img/sidebar/grade.png')}}" class="img-responsive sidebar-icon"> <span>GRADES</span></a></li>
            <li class="{{ Request::path() == 'admin/designation' ? 'active' : '' }}"><a href="{{ url('admin/designation') }}"><img src="{{ asset('img/sidebar/designation.png')}}" class="img-responsive sidebar-icon"></i> <span>DESIGNATIONS</span></a></li>
            <li class="{{ Request::path() == 'admin/announce' ? 'active' : '' }}"><a href="{{ url('admin/announce') }}"><img src="{{ asset('img/sidebar/announcement.png')}}" class="img-responsive sidebar-icon"> <span>ANNOUNCEMENTS</span></a></li>
            <li class="{{ Request::path() == 'admin/request_meeting' ? 'active' : '' }}"><a href="{{ url('admin/request_meeting') }}"><img src="{{ asset('img/sidebar/request_meeting.png')}}" class="img-responsive sidebar-icon"></i> <span>MEETING REQUESTS</span></a></li>
            <li class="{{ Request::path() == 'admin/notes' ? 'active' : '' }}"><a href="{{ url('admin/notes') }}"><img src="{{ asset('img/sidebar/notes.png')}}" class="img-responsive sidebar-icon"> <span>NOTES</span></a></li>

              {{-- <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>General</span> <i class="fa fa-angle-right pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/department') }}">Departments</a></li>
                    <li><a href="{{ url('admin/grade') }}">Grades</a></li>
                    <li><a href="{{ url('admin/designation') }}">Designation</a></li>
                    <li><a href="{{ url('admin/announce') }}">Announcements</a></li>
                    <li><a href="{{ url('admin/request_meeting') }}">Request Meeting</a></li>
                    <li><a href="{{ url('admin/notes') }}">Notes</a></li>
                </ul>
            </li> --}}

              {{-- <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Hello</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Uses</a></li>
                    <li><a href="#">KTI</a></li>
                </ul>
            </li> --}}

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
