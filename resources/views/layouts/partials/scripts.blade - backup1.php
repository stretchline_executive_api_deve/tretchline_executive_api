
<!-- jQuery 2.2.3 -->
<!-- <script src="{{ asset('/plugins/jQuery/jquery-2.2.3.min.js')}}"></script> -->
<!-- jQuery UI 1.11.4 -->
 <script src="https://code.jquery.com/jquery-3.1.1.min.js"  crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  // $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/js/bootstrap.min.js')}}"></script>
<!-- Chosen Version 1.7.0 -->
<script src="{{ asset('/js/chosen.jquery.min.js')}}"></script>
<!-- Chosen Proto Version 1.7.0 -->
<script src="{{ asset('/js/chosen.proto.min.js')}}"></script>
<!-- Miltiple Fields JS -->
{{-- <script src="{{ asset('/js/jquery.multifield.min.js')}}"></script> --}}
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<!-- Data Table Responsive JS -->
<script src="{{ asset('/js/dataTables.responsive.min.js')}}"></script>
<!-- Bootstrap Confirmation JS -->
<script src="{{ asset('/js/bootstrap-confirmation.min.js')}}"></script>
<!-- Custom JS -->
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('/js/custom.js')}}"></script>
<script src="{{ asset('/js/ajaxnotify.js')}}"></script>
<script>
    $(".selector2").datepicker({ dateFormat: 'yy-mm-dd' });
    $(".selector").datepicker({ dateFormat: 'yy-mm-dd' });
</script>

<script >





    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#button-add-more"); //Add button ID

    var x = 1; //initlal text box count



    $(add_button).click(function(e){ //on add input button click
        // alert('ads');
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment



            var body_html = '<div class="panel panel-default"><a href="#" class="pull-right remove_field"><i class="fa fa-times" aria-hidden="true"></i></a> <div class="panel-heading"> Create Milestone for KPI </div> <div class="panel-body">  <div class="form-group">   <label for="text">Milestone name:</label>  <input type="text" class="form-control"  name="name[]"> </div>  <div class="form-group">   <label for="text">Milestone description:</label>  <input type="text" class="form-control"  name="description[]"> </div>   <div class="form-group"> <label for="number">weight:</label>  <input type="number" class="form-control"  name="weight[]"> </div> <div class="form-group"> <label for="text">start date:</label>  <input type="text" class="form-control sdatepick'+x+'"  name="start_date[]">      </div> <div class="form-group"><label for="text">End date:</label>  <input type="text" class="form-control edatepick'+x+'"  name="end_date[]"> </div> </div></div> ';

            $(wrapper).append(body_html); //add input box

              $(".sdatepick"+x).datepicker({ dateFormat: 'yy-mm-dd' });
             $(".edatepick"+x).datepicker({ dateFormat: 'yy-mm-dd' });

        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })



$(document).ready(function(){
    $('#saveandcontinu').attr('disabled',true);
    $('.description, .title').bind('keyup', function(){
        if($('.description').val().length !=0 && $('.title').val().length !=0 && $('.kpi-select').val().length !=0 )
            $('#saveandcontinu').attr('disabled', false);
        else
            $('#saveandcontinu').attr('disabled',true);
    })
    $('.kpi-select').change(function(){
            $('#saveandcontinu').attr('disabled', false);

    })
});

</script>
