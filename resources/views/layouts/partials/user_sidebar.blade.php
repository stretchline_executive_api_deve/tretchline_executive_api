<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar bar-custom">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
       {{--  @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                <input type="hidden" id="home_url_user" name="" value="{{ url('/user/') }}">
                <input type="hidden" id="auth_user" name="" value="{{ Auth::user()->id }}">
                    <img src="{{asset(Auth::user()->profile_image)}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!- Status ->
                    <a href="#"><i class="fa fa-circle text-success"></i> online</a>
                </div>
            </div>
        @endif --}}
 
        <!-- search form (Optional) -->
       
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header" style="height: 50px !important;"></li> 
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Request::path() == 'user/home' ? 'active' : '' }}"><a href="{{ url('/user/home') }}"><img src="{{ asset('img/sidebar/dashboard.png')}}" class="img-responsive sidebar-icon"> <span>DASHBOARD</span></a></li>

            <li class="{{ Request::path() == 'user/my_overall' ? 'active' : '' }}"><a href="{{ url('/user/my_overall') }}"><img src="{{ asset('img/sidebar/my_overall.png')}}" class="img-responsive sidebar-icon"> <span>MY OVERALL</span></a></li>
            
            
            <li class="{{ Request::path() == 'user/sub_users/'.Auth::user()->id ? 'active' : null }}"><a href="{{ url('user/sub_users/'.Auth::user()->id) }}"><img src="{{ asset('img/sidebar/user_milestone.png')}}" class="img-responsive sidebar-icon"> <span>SUBUSERS</span></a></li>

            <li class="{{ Request::path() == 'user/kpi/create/'.Auth::user()->id ? 'active' : null }}"><a href="{{ url('user/kpi/create/'.Auth::user()->id) }}"><img src="{{ asset('img/sidebar/KPI.png')}}" class="img-responsive sidebar-icon"> <span>CREATE KPI</span></a></li>

            <li class="{{ Request::path() == 'user/milestone/create/' ? 'active' : null }}"><a href="{{ url('user/milestone/create/') }}"><img src="{{ asset('img/sidebar/Milestone.png')}}" class="img-responsive sidebar-icon"> <span>CREATE MILESTONE</span></a></li>

              <li class="{{ Request::path() == 'user/super_user/'.Auth::user()->id ? 'active' : null }}"><a href="{{ url('/user/super_user/'.Auth::user()->id) }}"><img src="{{ asset('img/sidebar/superusers.png')}}" class="img-responsive sidebar-icon"> <span>SUPER USER ASSIGNED</span></a></li>

             <li class="{{ Request::path() == 'user/announcement/'.Auth::user()->id ? 'active' : null }}"><a href="{{ url('/user/announcement/'.Auth::user()->id) }}"><img src="{{ asset('img/sidebar/announcement.png')}}" class="img-responsive sidebar-icon"> <span>ANNOUNCEMENTS</span></a></li>

             <li class="{{ Request::path() == 'user/meeting-request/show/'.Auth::user()->id ? 'active' : null }}"><a href="{{ url('/user/meeting-request/show/'.Auth::user()->id) }}"><img src="{{ asset('img/sidebar/request_meeting.png')}}" class="img-responsive sidebar-icon"> <span>MEETING REQUESTS</span></a></li>

{{--             <li class="treeview {{ Request::path() == 'user/sub_users/'.Auth::user()->id ? 'active' : null ||  Request::path() == 'user/milestone/create/' ? 'active' : null || Request::path() == 'user/kpi/create/'.Auth::user()->id  ? 'active' : null}}">
                <a href=""><img src="{{ asset('img/sidebar/subusers.png')}}" class="img-responsive sidebar-icon"> <span>SUB USERS</span> <i class="fa fa-angle-right pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::path() == 'user/sub_users/'.Auth::user()->id ? 'active' : null }}"><a href="{{ url('/user/sub_users/'.Auth::user()->id) }}">Subusers Info</a></li>
                    <li class="{{ Request::path() == 'user/kpi/create/'.Auth::user()->id  ? 'active' : null }}"><a href="{{ url('/user/kpi/create/'.Auth::user()->id ) }}">Create KPI</a></li>
                    <li class="{{ Request::path() == 'user/milestone/create/' ? 'active' : null }}"><a href="{{ url('/user/milestone/create/') }}">Create Milestone</a></li>
                </ul>
            </li> --}}
 
            <!--  <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Hello</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Uses</a></li>
                    <li><a href="#">KTI</a></li>
                     <li><a href="#">Uses</a></li>
                    <li><a href="#">KTI</a></li>
                </ul>
            </li> -->
<!-- 
              <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Hello</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Uses</a></li>
                    <li><a href="#">KTI</a></li>
                </ul>
            </li> -->

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
