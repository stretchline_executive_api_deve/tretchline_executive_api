<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show


<body class="skin-blue sidebar-mini" ng-app="myApp"  >
<div class="wrapper">

    @include('layouts.partials.user_mainheader')

    @include('layouts.partials.user_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 620px!important;">

        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
