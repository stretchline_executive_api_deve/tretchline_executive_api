<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>User Registerd</title>
	<link rel="stylesheet" href="">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Theme style -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.11/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

	{{-- <link href="{{ asset('/css/skins/skin-blue.css') }}" rel="stylesheet" type="text/css" /> --}}
	<!-- iCheck -->
	{{-- <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" /> --}}
</head>
<body>
	<div class="container-fluid">
		<div class="row clearfix" style="margin-top: 50px;">
			<div class="col-md-2"></div>
			<div class="col-md-8">

				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title text-center">User Register Notification</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<p class="lead">Hi {{$username}},</p>
						<p class="lead">
							Deserunt voluptas laudantium dictumst, ipsum asperiores, interdum libero, aliquid aut venenatis temporibus itaque temporibus tempore. Sapiente, excepturi architecto do, aspernatur,
						</p>
						<div class="panel panel-default center-block" style="width: 50%;">
							<div class="panel-body">
								<h5 class="text-center">PASSWORD</h5>
									<p class="text-center">{{$password}}</p>
							</div>
						</div>
						<p class="lead">Regards,<br>
						Stretchline Admin</p>	
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<p class="text-center">© 2017 Stretchline. All rights reserved.</p>
					</div>
					<!-- /.box-footer -->
				</div>
				<!-- /.box -->

			</div>

		</div>
	</div>
	<div class="col-md-2"></div>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>	
</body>
</html>