@component('mail::message')
{{-- ## User Registration Notification<br><br> --}}
#Hello! {{$username}},
First off, I’d like to extend a warm welcome to the Stretchline ADMIN-PANEL. Please use following credentials with your email for login ADMIN-PANEL.


@component('mail::button', ['url' => $baseUrl])
Password: {{$password}}
@endcomponent

If you have any questions or concerns you can contact our ADMIN-PANEL administrator. <br><br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
