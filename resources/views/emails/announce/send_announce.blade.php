@component('mail::message')
#<center>{{$title}}</center>

{{-- The body of your message. --}}
{{$description}}


{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
