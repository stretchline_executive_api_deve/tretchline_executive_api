@component('mail::message')
{{-- # <center>Meeting Status Notification</center> --}}
#Hello! {{$senderName}},
Your Requested Meeting status updated. Please find below details.
@component('mail::table')
<small><b>Receiver Name:</b>  {{$senderName}}</small><br>
<small><b>Milestone ID:</b>  {{$milestoneID}}</small><br>
<small><b>Title:</b>  {{$reqTitle}}</small><br>
<small><b>Message:</b>  {{$reqMessage}}</small><br>
<small><b>Date & Time:</b>  {{$meetingDate}}</small><br>
<small><b>Receiver State:</b>  {{$receiverState}}</small><br>
@endcomponent
@component('mail::button', ['url' => $baseUrl])
View Request
@endcomponent
Curae ullamco, sapiente sollicitudin! Mattis, eros aptent tempora quia torquent autem dui! <br><br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
