@component('mail::message')
{{-- #<center>Meeting Request Notification</center> --}}

#Hello! {{$receiverName}},
You have new meeting request from {{$senderName}}. Please find details below.

{{-- {{$senderName}}||{{$milestoneID}}||{{$reqTitle}}||{{$reqMessage}}
{{$meetingDate}} || {{$receiverName}}|| {{$receiverEmail}} --}}
@component('mail::table')
<small><b>Sender Name</b>:  {{$senderName}}</small><br>
<small><b>Milestone ID</b>:  {{$milestoneID}}</small><br>
<small><b>Title</b>:  {{$reqTitle}}</small><br>
<small><b>Message</b>:  {{$reqMessage}}</small><br>
<small><b>Date & Time</b>:  {{$meetingDate}}</small><br>
@endcomponent
@component('mail::button', ['url' => $baseUrl])
Respond to a Request
@endcomponent
Curae ullamco, sapiente sollicitudin! Mattis, eros aptent tempora quia torquent autem dui! <br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
